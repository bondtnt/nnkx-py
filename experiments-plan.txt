for assessment of various parameters influence on end result (extracted tree precion / complexity)
we need to accomplish set of experiments.

Parameters to be assessed:

ErrorRiseTolerance: [0, 0.05, ???]
Pruning: [max 50% neurons, max 100% neurons?]
Max Tree Depth [5, 10, 20]
m