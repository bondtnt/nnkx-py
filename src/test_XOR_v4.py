import pandas as pd
import torch
from torch.autograd import Variable
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.utils import data

from src.DataFrameDataset import DataFrameDataset

torch.set_num_threads(8)

EPOCHS_TO_TRAIN = 50000

class Net(nn.Module):

    def __init__(self, num_features=2, dropout=0.25, hidden1_units=3, hidden2_units=2):
        super().__init__()
        self.model = nn.Sequential(
            nn.Linear(num_features, hidden1_units),
            nn.ReLU(),
            nn.BatchNorm1d(hidden1_units),
            nn.Linear(hidden1_units, 2),
            nn.LogSoftmax()
        )
        for m in self.model:
            if isinstance(m, nn.Linear):
                nn.init.kaiming_normal_(m.weight)
                nn.init.constant_(m.bias, 0)

    def forward(self, input_tensor):
        x = self.model(input_tensor)
        return x


kwargs = {'batch_size': 4,
          'shuffle': True,
          'num_workers': 2,
          'pin_memory': True}


xor = {
    'col1': [0,1,0,1],
    'col2': [0,0,1,1],
    'target': [0,1,1,0]
    }

df = pd.DataFrame(xor,columns= ['col1', 'col2', 'target'])
full_dataset = DataFrameDataset(df, 'target')
train_loader = data.DataLoader(full_dataset, **kwargs)


model = Net()
model.train()
criterion = nn.CrossEntropyLoss()
# optimizer = torch.optim.Adamax(model.parameters(), betas=(0.9, 0.999), lr=0.1, weight_decay=1e-2)
optimizer = optim.SGD(model.parameters(), lr=0.01)

device = 'cpu'
epochs = 50

print("Training loop:")
for epoch in range(1, epochs + 1):
    for batch_idx, (data, target) in enumerate(train_loader):

        optimizer.zero_grad()   # zero the gradient buffers
        y_pred = model(data)
        target = target.type(torch.LongTensor)
        loss = criterion(y_pred, target)
        loss.backward()
        optimizer.step()    # Does the update
        # if idx % 5000 == 0:
        print("Epoch {: >8} Loss: {}".format(epoch, loss.data.numpy()))



print("")
print("Testing:")

model.eval()
test_loss = 0
correct = 0
with torch.no_grad():
    for data, target in train_loader:
        data, target = data.to(device), target.to(device)
        # target = target.type(torch.LongTensor)
        output = model(data)
        # # test_loss += F.nll_loss(output, target, reduction='sum').item() # sum up batch loss
        # test_loss += F.binary_cross_entropy(output, target, reduction='sum').item()  # sum up batch loss
        if isinstance(criterion, nn.BCELoss) or isinstance(criterion, nn.BCEWithLogitsLoss):
            output = output.flatten()
            test_loss += criterion(output, target).item()
        elif isinstance(criterion, nn.CrossEntropyLoss):
            if device == 'cuda':
                target = target.type(torch.cuda.LongTensor)
            else:
                target = target.type(torch.LongTensor)
            test_loss += criterion(output, target).item()
        else:
            test_loss += criterion(output, target, reduction='sum').item()
        pred = output.argmax(dim=1, keepdim=True)  # get the index of the max log-probability
        # pred = output
        pred = pred.type(torch.LongTensor)
        correct += pred.eq(target.type(torch.LongTensor).view_as(pred)).sum().item()

test_loss /= len(train_loader.dataset)

print('Epoch {}, Test set: Average loss: {:.8f}, Accuracy: {}/{} ({:.4f}%)'.format(
    epoch, test_loss, correct, len(train_loader.dataset),
    100. * correct / len(train_loader.dataset)))
