import pandas as pd

def main():
    # f = open('/home/andrey/works/PythonProjects/nnkx-py/src/2019-05-07_181217/xval10_PrunFallback5_errTol_0.5_retr30.log', "r")
    # f = open('/home/andrey/works/PythonProjects/nnkx-py/src/2019-05-08_073123/xval10_PrunFallback1_errTol_0.5_retr30.log', "r")
    # f = open('/home/andrey/works/PythonProjects/nnkx-py/src/2019-05-08_192644/xval10_PrunFallback1_errTol_0.1_retr30.log', "r")
    # f = open('/home/andrey/works/PythonProjects/nnkx-py/src/2019-05-09_070100/xval10_PrunFallback1_errTol_0.1_retr100.log', "r")
    # f = open('/home/andrey/works/PythonProjects/nnkx-py/src/2019-05-09_070100/xval10_PrunFallback1_errTol_0.1_retr100.log', "r")
    # f = open('/home/andrey/works/PythonProjects/nnkx-py/src/2019-05-09_203319/xval10_train_300_PrunFallback1_errTol_0.1_retr100.log', "r")
    f = open('/home/andrey/works/PythonProjects/nnkx-py/src/2019-05-13_000852/monks2.logs', "r")

    trained_train_acc = []
    trained_test_acc = []

    pruned_train_acc = []
    pruned_test_acc = []

    clusterized_train_acc = []
    clusterized_test_acc = []

    rules_count = []
    rules_tree_depth = []

    rule_train_acc = []
    rule_train_f1 = []
    rule_test_acc = []
    rule_test_f1 = []

    for x in f:
        if 'Trained model TRAIN_ACCURACY: ' in x:
            val = float(x[len('Trained model TRAIN_ACCURACY: '):])
            trained_train_acc.append(val)

        elif 'Trained model TEST_ACCURACY: ' in x:
            val = float(x[len('Trained model TEST_ACCURACY: '):])
            trained_test_acc.append(val)

        elif 'Pruned model TRAIN_ACCURACY: ' in x:
            val = float(x[len('Pruned model TRAIN_ACCURACY: '):])
            pruned_train_acc.append(val)

        elif 'Pruned model TEST_ACCURACY: ' in x:
            val = float(x[len('Pruned model TEST_ACCURACY: '):])
            pruned_test_acc.append(val)

        elif 'Discretized/Clusterized model TRAIN_ACCURACY: ' in x:
            val = float(x[len('Discretized/Clusterized model TRAIN_ACCURACY: '):])
            clusterized_train_acc.append(val)

        elif 'Discretized/Clusterized model TEST_ACCURACY: ' in x:
            val = float(x[len('Discretized/Clusterized model TEST_ACCURACY: '):])
            clusterized_test_acc.append(val)

        elif 'Tree leafs=' in x:
            leafs_depth = x[len('Tree leafs='):]
            leafs_depth_list = leafs_depth.split(' maxDepth=')
            val = float(leafs_depth_list[0])
            rules_count.append(val)
            val = float(leafs_depth_list[1])
            rules_tree_depth.append(val)

        elif 'Rules Train accuracy=' in x:
            acc_and_f1 = x[len('Rules Train accuracy='):]
            acc_and_f1_list = acc_and_f1.split(', f1=')
            val = float(acc_and_f1_list[0])
            rule_train_acc.append(val)
            val = float(acc_and_f1_list[1])
            rule_train_f1.append(val)

        elif 'Rules Test accuracy=' in x:
            acc_and_f1 = x[len('Rules Test accuracy='):]
            acc_and_f1_list = acc_and_f1.split(', f1=')
            val = float(acc_and_f1_list[0])
            rule_test_acc.append(val)
            val = float(acc_and_f1_list[1])
            rule_test_f1.append(val)


    print(trained_train_acc)
    print(trained_test_acc)
    print(pruned_train_acc)
    print(pruned_test_acc)
    print(clusterized_train_acc)
    print(clusterized_test_acc)
    print(rules_count)
    print(rules_tree_depth)
    print(rule_train_acc)
    print(rule_train_f1)
    print(rule_test_acc)
    print(rule_test_f1)

    df = pd.DataFrame({
        'trained_train_acc': trained_train_acc,
        'trained_test_acc': trained_test_acc,
        'pruned_train_acc': pruned_train_acc,
        'pruned_test_acc': pruned_test_acc,
        'clusterized_train_acc': clusterized_train_acc,
        'clusterized_test_acc': clusterized_test_acc,
        'rules_count': rules_count,
        'rules_tree_depth': rules_tree_depth,
        'rule_train_acc': rule_train_acc,
        # 'rule_train_f1': rule_train_f1,
        'rule_test_acc': rule_test_acc,
        # 'rule_test_f1': rule_test_f1
    })

    print(df.mean(axis=0))

if __name__ == '__main__':
    main()