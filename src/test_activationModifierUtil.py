from unittest import TestCase
from nnkx import ActivationModifierUtil
import torch
from sklearn.cluster import DBSCAN
import numpy as np


def almost_eq_cluster_tables(tens_a: torch.Tensor, tens_b: torch.Tensor, e=1e-12):
    # we know that clusters last bound is always +INF - let's replace it with 0.0
    tens_a[-1:, -1] = 0.0
    tens_b[-1:, -1] = 0.0
    return torch.all(torch.lt(torch.abs(torch.add(tens_a, -tens_b)), e)).item()

def isclose(a, b, rel_tol=1e-12, abs_tol=0.0):
    return abs(a-b) <= max(rel_tol * max(abs(a), abs(b)), abs_tol)

class TestActivationModifierUtil(TestCase):
    def test_vector_pairwise_distances(self):
        util = ActivationModifierUtil(None, None)
        initial_clusters = torch.Tensor([[-0.9507, -0.9477],
                                         [-0.9447, -0.9430],
                                         [-0.9413, -0.9409],
                                         [-0.9405, -0.9403],
                                         [-0.9401, -0.9380],
                                         [-0.9358, -0.9339],
                                         [-0.9319, -0.9312],
                                         [-0.9305, -0.9296],
                                         [-0.9288, -0.9278],
                                         [-0.9269, -0.9260],
                                         [-0.9251, -0.9247],
                                         [-0.9244, -0.9225],
                                         [-0.9207, -0.9157],
                                         [-0.9108, -0.9085],
                                         [-0.9062, -0.9051],
                                         [-0.9040, -0.9039],
                                         [-0.9038, -0.9035],
                                         [-0.9033, -0.9031],
                                         [-0.9028, -0.9025],
                                         [-0.9021, -0.9020],
                                         [-0.9019, -0.9018],
                                         [-0.9016, -0.9010],
                                         [-0.9004, -0.8988],
                                         [-0.8971, -0.8969],
                                         [-0.8967, -0.8949],
                                         [-0.8930, -0.8919],
                                         [-0.8908, -0.8897],
                                         [-0.8887, -0.8885],
                                         [-0.8882, -0.8871],
                                         [-0.8861, -0.8857],
                                         [-0.8854, -0.8853],
                                         [-0.8853, -0.8853],
                                         [-0.8853, -0.8844],
                                         [-0.8836, -0.8822],
                                         [-0.8808, -0.8803],
                                         [-0.8799, -0.8797],
                                         [-0.8796, -0.8772],
                                         [-0.8748, -0.8736],
                                         [-0.8724, -0.8723],
                                         [-0.8722, -0.8722],
                                         [-0.8721, -0.8681],
                                         [-0.8641, -0.8638],
                                         [-0.8634, -0.8632],
                                         [-0.8630, -0.8626],
                                         [-0.8622, -0.8615],
                                         [-0.8607, -0.8592],
                                         [-0.8577, -0.8575],
                                         [-0.8573, -0.8571],
                                         [-0.8569, -0.8560],
                                         [-0.8552, -0.8523],
                                         [-0.8493, -0.8486],
                                         [-0.8480, -0.8439],
                                         [-0.8398, -0.8367],
                                         [-0.8337, -0.8325],
                                         [-0.8314, -0.8213],
                                         [-0.8113, -0.8096],
                                         [-0.8079, -0.8072],
                                         [-0.8065, -0.8050],
                                         [-0.8036, -0.8029],
                                         [-0.8021, -0.8010],
                                         [-0.7999, -0.7965],
                                         [-0.7932, -0.7894],
                                         [-0.7855, -0.7845],
                                         [-0.7835, -0.7832],
                                         [-0.7829, -0.7779],
                                         [-0.7728, -0.7714],
                                         [-0.7701, -0.7684],
                                         [-0.7668, -0.7647],
                                         [-0.7626, -0.7606],
                                         [-0.7586, -0.7572],
                                         [-0.7557, -0.7520],
                                         [-0.7484, -0.7427],
                                         [-0.7370, -0.7327],
                                         [-0.7285, -0.7192],
                                         [-0.7100, -0.7096],
                                         [-0.7091, -0.7073],
                                         [-0.7055, -0.7041],
                                         [-0.7028, -0.6995],
                                         [-0.6962, -0.6953],
                                         [-0.6945, -0.6944],
                                         [-0.6944, -0.6888],
                                         [-0.6832, -0.6741],
                                         [-0.6651, -0.6613],
                                         [-0.6575, -0.6565],
                                         [-0.6555, -0.6511],
                                         [-0.6468, -0.6389],
                                         [-0.6309, -0.6287],
                                         [-0.6265, -0.6158],
                                         [-0.6052, -0.6021],
                                         [-0.5990, -0.5975],
                                         [-0.5960, -0.5947],
                                         [-0.5934, -0.5803],
                                         [-0.5672, -0.5592],
                                         [-0.5512, -0.5511],
                                         [-0.5510, -0.5467],
                                         [-0.5424, -0.5327],
                                         [-0.5229, -0.5172],
                                         [-0.5116, -0.5109],
                                         [-0.5101, -0.5031],
                                         [-0.4961, -0.4933],
                                         [-0.4904, -0.4835],
                                         [-0.4766, -0.4716],
                                         [-0.4667, -0.4539],
                                         [-0.4412, -0.4393],
                                         [-0.4374, -0.4216],
                                         [-0.4057, -0.3992],
                                         [-0.3926, -0.3858],
                                         [-0.3789, -0.3337],
                                         [-0.2884, -0.2669],
                                         [-0.2455, -0.2252],
                                         [-0.2050, -0.1681],
                                         [-0.1313, -0.1156],
                                         [-0.0999, -0.0838],
                                         [-0.0677, -0.0656],
                                         [-0.0635, -0.0083],
                                         [0.0469, 0.0621],
                                         [0.0773, 0.0972],
                                         [0.1171, 0.1579],
                                         [0.1988, 0.2158],
                                         [0.2329, 0.2587],
                                         [0.2846, 0.2846],
                                         [0.2846, 0.3216],
                                         [0.3587, 0.3629],
                                         [0.3671, 0.3749],
                                         [0.3827, 0.3834],
                                         [0.3842, 0.3939],
                                         [0.4035, 0.4228],
                                         [0.4420, 0.4968],
                                         [0.5516, 0.6066],
                                         [0.6616, 0.6726],
                                         [0.6836, 0.6977],
                                         [0.7119, 0.7264],
                                         [0.7408, 0.7468],
                                         [0.7527, 0.7528],
                                         [0.7529, 0.7552],
                                         [0.7575, 0.7600],
                                         [0.7626, 0.7641],
                                         [0.7656, 0.7700],
                                         [0.7745, 0.7836],
                                         [0.7926, 0.7986],
                                         [0.8046, 0.8172],
                                         [0.8298, 0.8303],
                                         [0.8307, 0.8327],
                                         [0.8346, 0.8429],
                                         [0.8511, 0.8518],
                                         [0.8526, 0.8539],
                                         [0.8553, 0.8575],
                                         [0.8597, 0.8610],
                                         [0.8623, 0.8646],
                                         [0.8670, 0.8687],
                                         [0.8704, 0.8724],
                                         [0.8743, 0.8757],
                                         [0.8771, 0.8786],
                                         [0.8802, 0.8827],
                                         [0.8852, 0.8853],
                                         [0.8854, 0.8862],
                                         [0.8870, 0.8880],
                                         [0.8890, 0.8900],
                                         [0.8911, 0.8941],
                                         [0.8971, 0.8976],
                                         [0.8982, 0.9000],
                                         [0.9018, 0.9020],
                                         [0.9022, 0.9031],
                                         [0.9039, 0.9047],
                                         [0.9054, 0.9062],
                                         [0.9071, 0.9085],
                                         [0.9100, 0.9104],
                                         [0.9107, 0.9109],
                                         [0.9110, 0.9118],
                                         [0.9125, 0.9126],
                                         [0.9126, 0.9138],
                                         [0.9149, 0.9154],
                                         [0.9158, 0.9175],
                                         [0.9192, 0.9224],
                                         [0.9256, 0.9263],
                                         [0.9270, 0.9281],
                                         [0.9291, 0.9327],
                                         [0.9363, 0.9396],
                                         [0.9428, 0.9437],
                                         [0.9446, 0.9457],
                                         [0.9469, 0.9469],
                                         [0.9469, 0.9481],
                                         [0.9492, 0.9493],
                                         [0.9494, 0.9502],
                                         [0.9510, 0.9511],
                                         [0.9511, 0.9513],
                                         [0.9516, 0.9520],
                                         [0.9524, 0.9526],
                                         [0.9528, 0.9539],
                                         [0.9550, 0.9553],
                                         [0.9556, 0.9562],
                                         [0.9567, 0.9574],
                                         [0.9582, 0.9583],
                                         [0.9584, 0.9584],
                                         [0.9584, 0.9593],
                                         [0.9602, 0.9607],
                                         [0.9612, 0.9612],
                                         [0.9613, float("inf")]])
        epsilon = 0.09
        clusters = util.clusterize(initial_clusters, epsilon)  # clusters are sorted ASC

        x = initial_clusters[:, 0]
        xnumpy = x.detach().numpy()
        xnumpy = xnumpy.reshape(-1, 1)  # this is required by DBSCAN
        db = DBSCAN(eps=epsilon, min_samples=1).fit(xnumpy)
        lb = db.labels_
        unique_clusters = np.unique(lb)

        new_clusters = []
        new_clusters_boundaries = []
        previous_cluster_max = None
        i = 0
        for c in unique_clusters:
            cluster_elements = x[np.where(lb == c)]
            cluster_center = cluster_elements.mean()
            cluster_min = cluster_elements.min()
            cluster_max = cluster_elements.max()
            if i > 0 is not None:
                cluster_boundary = (previous_cluster_max + cluster_min) / 2
                new_clusters_boundaries.append(cluster_boundary)
            new_clusters.append(cluster_center)

            previous_cluster_max = cluster_max
            i = i + 1
        new_clusters_boundaries.append(float("inf"))

        clusters2 = torch.Tensor(np.column_stack((new_clusters, new_clusters_boundaries)))

        print(lb)
        print('done')

    def test_clusterize_inputs(self):

        util = ActivationModifierUtil(None, None)
        initial_clusters = torch.Tensor([[1, 2],
                                         [3, 4],
                                         [7, 7.5],
                                         [8, 8.25],
                                         [8.5, 8.75],
                                         [9.2, 9.6],
                                         [10, float('+inf')]])

        merged_clusters1, merge_bounds1, merge_done1 = \
            util.clusterize_inputs(initial_clusters.clone(),
                                   n=1,
                                   skip_bounds=torch.FloatTensor())

        self.assertTrue(isclose(merge_bounds1[0].item(), 8.25, rel_tol=1e-6))
        self.assertTrue(almost_eq_cluster_tables(merged_clusters1,
                                                 torch.Tensor([[1, 2],
                                                 [3, 4],
                                                 [7, 7.5],
                                                 [8.25, 8.75],
                                                 [9.2, 9.6],
                                                 [10, float('+inf')]])))

        # Now test clusterize_inputs with skip_indexes that are not present in bounds
        merged_clusters2, merge_bounds2, merge_done2 = \
            util.clusterize_inputs(merged_clusters1.clone(),
                                   n=1,
                                   skip_bounds=torch.FloatTensor([merge_bounds1[0]]))

        self.assertTrue(isclose(merge_bounds2[0].item(), 9.6, rel_tol=1e-6))
        self.assertTrue(almost_eq_cluster_tables(merged_clusters2,
                                                 torch.Tensor([[1, 2],
                                                 [3, 4],
                                                 [7, 7.5],
                                                 [8.25, 8.75],
                                                 [9.6, float('+inf')]])))

        # Now test clusterize_inputs with skip_indexes that are not present in bounds
        merged_clusters3, merge_bounds3, merge_done3 = \
            util.clusterize_inputs(merged_clusters2.clone(),
                                   n=1,
                                   skip_bounds=torch.FloatTensor([merge_bounds1[0], merge_bounds2[0]]))

        self.assertTrue(isclose(merge_bounds3[0].item(), 7.5, rel_tol=1e-6))
        self.assertTrue(almost_eq_cluster_tables(merged_clusters3,
                                                 torch.Tensor([[1, 2],
                                                 [3, 4],
                                                 [7.625, 8.75],
                                                 [9.6, float('+inf')]])))

        # now test clusterize_inputs with skip_indexes holding bound that should be used for merging.
        merged_clusters4, merge_bounds4, merge_done4 = \
            util.clusterize_inputs(merged_clusters3.clone(),
                                   n=1,
                                   skip_bounds=torch.FloatTensor([8.75]))

        self.assertTrue(isclose(merge_bounds4[0].item(), 2.0, rel_tol=1e-6))
        self.assertTrue(almost_eq_cluster_tables(merged_clusters4,
                                                 torch.Tensor([[2, 4],
                                                 [7.625, 8.75],
                                                 [9.6, float('+inf')]])))

    def test_clusterize_n_inputs(self):
        util = ActivationModifierUtil(None, None)
        initial_clusters = torch.Tensor([[1, 2],
                                         [3, 4],
                                         [7, 7.5],
                                         [8, 8.25],
                                         [8.5, 8.75],
                                         [9.2, 9.6],
                                         [10, float('+inf')]])

        merged_clusters1, merge_bounds1, merge_done1 = \
            util.clusterize_inputs(initial_clusters.clone(),
                                   n=3,
                                   skip_bounds=torch.FloatTensor())

        self.assertTrue(isclose(merge_bounds1[0].item(), 8.25, rel_tol=1e-6))
        self.assertTrue(almost_eq_cluster_tables(merged_clusters1,
                                                 torch.Tensor([[1, 2],
                                                               [3, 4],
                                                               [7, 7.5],
                                                               [10, float('+inf')]])))