from torch.utils.data import Dataset

class XYDataset(Dataset):
    """XTY dataset wrapper for X and Y tensors"""

    def __init__(self, X, Y):
        """
        Args:
            X (Tensor): Tensor containing X.
            Y (Tensor): Tensor containing Y - labels.
        """
        self.X = X
        self.Y = Y

    def __len__(self):
        return len(self.X)

    def __getitem__(self, idx):
        return self.X[idx], self.Y[idx]