import numpy as np

# Some random data
aa = np.random.uniform(0., 1., 100)
bb = np.array([.1, .2, .4, .55, .97])
#
# # For each element in aa, find the index of the nearest element in bb
# idx = np.searchsorted(bb, aa)
# # For indexes to the right of the rightmost bb element, associate to the last
# # bb element.
# msk = idx > len(bb) - 1
# idx[msk] = len(bb) - 1
#
# # Replace values in aa
# aa2 = np.array([bb[_] for _ in idx])


aa_nearest = bb[abs(aa[None, :] - bb[:, None]).argmin(axis=0)]
print(aa_nearest)