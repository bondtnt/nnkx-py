import json

import pandas as pd

def read_stats(file_path: str):
    trained_train_acc = []
    trained_test_acc = []

    pruned_train_acc = []
    pruned_test_acc = []

    # greedy discretization (no performance decrease)
    greedy_rules_count = []
    greedy_rules_tree_depth = []
    greedy_rule_train_acc = []
    greedy_rule_train_f1 = []
    greedy_rule_test_acc = []
    greedy_rule_test_f1 = []
    greedy_discretized_model_train_acc = []
    greedy_discretized_model_test_acc = []

    # normal discretization ( some loss function worsening allowed )
    normal_rules_count = []
    normal_rules_tree_depth = []
    normal_rule_train_acc = []
    normal_rule_train_f1 = []
    normal_rule_test_acc = []
    normal_rule_test_f1 = []
    normal_discretized_model_train_acc = []
    normal_discretized_model_test_acc = []

    with open(file_path, 'r') as json_file:
        data = json.load(json_file)
        for d in data:
            trained_train_acc.append(d['network_train_acc'])
            trained_test_acc.append(d['network_test_acc'])

            pruned_train_acc.append(d['network_pruned_train_acc'])
            pruned_test_acc.append(d['network_pruned_test_acc'])

            for descr in d['list_discretization_stats']:
                if 'greedy' in descr['discretization_strategy'].lower():
                    greedy_rules_count.append(descr['tree.leafs'])
                    greedy_rules_tree_depth.append(descr['tree.max_depth'])
                    greedy_rule_train_acc.append(descr['tree.accuracy_score_tr'])
                    greedy_rule_test_acc.append(descr['tree.accuracy_score_te'])
                    greedy_rule_train_f1.append(descr['tree.f1_score_tr'])
                    greedy_rule_test_f1.append(descr['tree.f1_score_te'])
                    greedy_discretized_model_train_acc.append(descr['discretized_model_train_acc'])
                    greedy_discretized_model_test_acc.append(descr['discretized_model_test_acc'])
                elif 'Discretize_0.99' in descr['discretization_strategy']:
                    normal_rules_count.append(descr['tree.leafs'])
                    normal_rules_tree_depth.append(descr['tree.max_depth'])
                    normal_rule_train_acc.append(descr['tree.accuracy_score_tr'])
                    normal_rule_test_acc.append(descr['tree.accuracy_score_te'])
                    normal_rule_train_f1.append(descr['tree.f1_score_tr'])
                    normal_rule_test_f1.append(descr['tree.f1_score_te'])
                    normal_discretized_model_train_acc.append(descr['discretized_model_train_acc'])
                    normal_discretized_model_test_acc.append(descr['discretized_model_test_acc'])
                else:
                    raise Exception('Unknown state!' + json.dumps(descr, ident=4))

    df = pd.DataFrame({
        'trained_train_acc': trained_train_acc,
        'trained_test_acc': trained_test_acc,
        'pruned_train_acc': pruned_train_acc,
        'pruned_test_acc': pruned_test_acc,

        'greedy_rules_count': greedy_rules_count,
        'greedy_rules_tree_depth': greedy_rules_tree_depth,
        'greedy_rule_train_acc': greedy_rule_train_acc,
        'greedy_rule_test_acc': greedy_rule_test_acc,
        'greedy_discretized_model_train_acc': greedy_discretized_model_train_acc,
        'greedy_discretized_model_test_acc': greedy_discretized_model_test_acc,

        'normal_rules_count': normal_rules_count,
        'normal_rules_tree_depth': normal_rules_tree_depth,
        'normal_rule_train_acc': normal_rule_train_acc,
        'normal_rule_test_acc': normal_rule_test_acc,
        'normal_discretized_model_train_acc': normal_discretized_model_train_acc,
        'normal_discretized_model_test_acc': normal_discretized_model_test_acc,
    })

    return df

def read_stats2(file_path: str):  # with nested prunings and further nested discretizers
    trained_train_acc = []
    trained_test_acc = []

    # SMALL PRUNING
    small_pruned_train_acc = []
    small_pruned_test_acc = []
    # greedy discretization (no performance decrease)
    small_pruning_greedy_rules_count = []
    small_pruning_greedy_rules_tree_depth = []
    small_pruning_greedy_rule_train_acc = []
    small_pruning_greedy_rule_train_f1 = []
    small_pruning_greedy_rule_test_acc = []
    small_pruning_greedy_rule_test_f1 = []
    small_pruning_greedy_discretized_model_train_acc = []
    small_pruning_greedy_discretized_model_test_acc = []

    # normal discretization ( some loss function worsening allowed )
    small_pruning_normal_rules_count = []
    small_pruning_normal_rules_tree_depth = []
    small_pruning_normal_rule_train_acc = []
    small_pruning_normal_rule_train_f1 = []
    small_pruning_normal_rule_test_acc = []
    small_pruning_normal_rule_test_f1 = []
    small_pruning_normal_discretized_model_train_acc = []
    small_pruning_normal_discretized_model_test_acc = []

    # NORMAL PRUNING
    normal_pruned_train_acc = []
    normal_pruned_test_acc = []
    # greedy discretization (no performance decrease)
    normal_pruning_greedy_rules_count = []
    normal_pruning_greedy_rules_tree_depth = []
    normal_pruning_greedy_rule_train_acc = []
    normal_pruning_greedy_rule_train_f1 = []
    normal_pruning_greedy_rule_test_acc = []
    normal_pruning_greedy_rule_test_f1 = []
    normal_pruning_greedy_discretized_model_train_acc = []
    normal_pruning_greedy_discretized_model_test_acc = []

    # normal discretization ( some loss function worsening allowed )
    normal_pruning_normal_rules_count = []
    normal_pruning_normal_rules_tree_depth = []
    normal_pruning_normal_rule_train_acc = []
    normal_pruning_normal_rule_train_f1 = []
    normal_pruning_normal_rule_test_acc = []
    normal_pruning_normal_rule_test_f1 = []
    normal_pruning_normal_discretized_model_train_acc = []
    normal_pruning_normal_discretized_model_test_acc = []
    
    with open(file_path, 'r') as json_file:
        data = json.load(json_file)
        for d in data:
            trained_train_acc.append(d['network_train_acc'])
            trained_test_acc.append(d['network_test_acc'])
            for p_i in range(2):
                p = d['prunings']
                if p[p_i]['maxFallbacks'] == 1:
                    small_pruned_train_acc.append(p[p_i]['network_pruned_train_acc'])
                    small_pruned_test_acc.append(p[p_i]['network_pruned_test_acc'])
                    for descr in p[p_i]['list_discretization_stats']:
                        print(descr)
                        if 'greedy' in descr['discretization_strategy'].lower():
                            small_pruning_greedy_rules_count.append(descr['tree.leafs'])
                            small_pruning_greedy_rules_tree_depth.append(descr['tree.max_depth'])
                            small_pruning_greedy_rule_train_acc.append(descr['tree.accuracy_score_tr'])
                            small_pruning_greedy_rule_test_acc.append(descr['tree.accuracy_score_te'])
                            small_pruning_greedy_rule_train_f1.append(descr['tree.f1_score_tr'])
                            small_pruning_greedy_rule_test_f1.append(descr['tree.f1_score_te'])
                            small_pruning_greedy_discretized_model_train_acc.append(descr['discretized_model_train_acc'])
                            small_pruning_greedy_discretized_model_test_acc.append(descr['discretized_model_test_acc'])
                        elif 'Discretize_0.99' in descr['discretization_strategy']:
                            small_pruning_normal_rules_count.append(descr['tree.leafs'])
                            small_pruning_normal_rules_tree_depth.append(descr['tree.max_depth'])
                            small_pruning_normal_rule_train_acc.append(descr['tree.accuracy_score_tr'])
                            small_pruning_normal_rule_test_acc.append(descr['tree.accuracy_score_te'])
                            small_pruning_normal_rule_train_f1.append(descr['tree.f1_score_tr'])
                            small_pruning_normal_rule_test_f1.append(descr['tree.f1_score_te'])
                            small_pruning_normal_discretized_model_train_acc.append(descr['discretized_model_train_acc'])
                            small_pruning_normal_discretized_model_test_acc.append(descr['discretized_model_test_acc'])
                        else:
                            raise Exception('Unknown state!' + json.dumps(descr, ident=4))
                else:
                    normal_pruned_train_acc.append(p[p_i]['network_pruned_train_acc'])
                    normal_pruned_test_acc.append(p[p_i]['network_pruned_test_acc'])
                    for descr in p[p_i]['list_discretization_stats']:
                        print(descr)
                        if 'greedy' in descr['discretization_strategy'].lower():
                            normal_pruning_greedy_rules_count.append(descr['tree.leafs'])
                            normal_pruning_greedy_rules_tree_depth.append(descr['tree.max_depth'])
                            normal_pruning_greedy_rule_train_acc.append(descr['tree.accuracy_score_tr'])
                            normal_pruning_greedy_rule_test_acc.append(descr['tree.accuracy_score_te'])
                            normal_pruning_greedy_rule_train_f1.append(descr['tree.f1_score_tr'])
                            normal_pruning_greedy_rule_test_f1.append(descr['tree.f1_score_te'])
                            normal_pruning_greedy_discretized_model_train_acc.append(
                                descr['discretized_model_train_acc'])
                            normal_pruning_greedy_discretized_model_test_acc.append(descr['discretized_model_test_acc'])
                        elif 'Discretize_0.99' in descr['discretization_strategy']:
                            normal_pruning_normal_rules_count.append(descr['tree.leafs'])
                            normal_pruning_normal_rules_tree_depth.append(descr['tree.max_depth'])
                            normal_pruning_normal_rule_train_acc.append(descr['tree.accuracy_score_tr'])
                            normal_pruning_normal_rule_test_acc.append(descr['tree.accuracy_score_te'])
                            normal_pruning_normal_rule_train_f1.append(descr['tree.f1_score_tr'])
                            normal_pruning_normal_rule_test_f1.append(descr['tree.f1_score_te'])
                            normal_pruning_normal_discretized_model_train_acc.append(descr['discretized_model_train_acc'])
                            normal_pruning_normal_discretized_model_test_acc.append(descr['discretized_model_test_acc'])
                        else:
                            raise Exception('Unknown state!' + json.dumps(descr, ident=4))
                    

    df = pd.DataFrame({
        'trained_train_acc': trained_train_acc,
        'trained_test_acc': trained_test_acc,
        
        
        'small_pruned_train_acc': small_pruned_train_acc,
        'small_pruned_test_acc': small_pruned_test_acc,

        'small_pruning_greedy_rules_count': small_pruning_greedy_rules_count,
        'small_pruning_greedy_rules_tree_depth': small_pruning_greedy_rules_tree_depth,
        'small_pruning_greedy_rule_train_acc': small_pruning_greedy_rule_train_acc,
        'small_pruning_greedy_rule_test_acc': small_pruning_greedy_rule_test_acc,
        'small_pruning_greedy_discretized_model_train_acc': small_pruning_greedy_discretized_model_train_acc,
        'small_pruning_greedy_discretized_model_test_acc': small_pruning_greedy_discretized_model_test_acc,

        'small_pruning_normal_rules_count': small_pruning_normal_rules_count,
        'small_pruning_normal_rules_tree_depth': small_pruning_normal_rules_tree_depth,
        'small_pruning_normal_rule_train_acc': small_pruning_normal_rule_train_acc,
        'small_pruning_normal_rule_test_acc': small_pruning_normal_rule_test_acc,
        'small_pruning_normal_discretized_model_train_acc': small_pruning_normal_discretized_model_train_acc,
        'small_pruning_normal_discretized_model_test_acc': small_pruning_normal_discretized_model_test_acc,


        'normal_pruned_train_acc': normal_pruned_train_acc,
        'normal_pruned_test_acc': normal_pruned_test_acc,

        'normal_pruning_greedy_rules_count': normal_pruning_greedy_rules_count,
        'normal_pruning_greedy_rules_tree_depth': normal_pruning_greedy_rules_tree_depth,
        'normal_pruning_greedy_rule_train_acc': normal_pruning_greedy_rule_train_acc,
        'normal_pruning_greedy_rule_test_acc': normal_pruning_greedy_rule_test_acc,
        'normal_pruning_greedy_discretized_model_train_acc': normal_pruning_greedy_discretized_model_train_acc,
        'normal_pruning_greedy_discretized_model_test_acc': normal_pruning_greedy_discretized_model_test_acc,

        'normal_pruning_normal_rules_count': normal_pruning_normal_rules_count,
        'normal_pruning_normal_rules_tree_depth': normal_pruning_normal_rules_tree_depth,
        'normal_pruning_normal_rule_train_acc': normal_pruning_normal_rule_train_acc,
        'normal_pruning_normal_rule_test_acc': normal_pruning_normal_rule_test_acc,
        'normal_pruning_normal_discretized_model_train_acc': normal_pruning_normal_discretized_model_train_acc,
        'normal_pruning_normal_discretized_model_test_acc': normal_pruning_normal_discretized_model_test_acc,
    })

    return df

def main():
    # f = open('/home/andrey/works/PythonProjects/nnkx-py/src/2019-05-07_181217/xval10_PrunFallback5_errTol_0.5_retr30.log', "r")
    # f = open('/home/andrey/works/PythonProjects/nnkx-py/src/2019-05-08_073123/xval10_PrunFallback1_errTol_0.5_retr30.log', "r")
    # f = open('/home/andrey/works/PythonProjects/nnkx-py/src/2019-05-08_192644/xval10_PrunFallback1_errTol_0.1_retr30.log', "r")
    # f = open('/home/andrey/works/PythonProjects/nnkx-py/src/2019-05-09_070100/xval10_PrunFallback1_errTol_0.1_retr100.log', "r")
    # f = open('/home/andrey/works/PythonProjects/nnkx-py/src/2019-05-09_070100/xval10_PrunFallback1_errTol_0.1_retr100.log', "r")
    # f = open('/home/andrey/works/PythonProjects/nnkx-py/src/2019-05-09_203319/xval10_train_300_PrunFallback1_errTol_0.1_retr100.log', "r")

    # df11 = read_stats(file_path='/home/andrey/works/PythonProjects/nnkx-py/src/2019-05-20_215335/AdultCensus_stats.json')
    # print('afterPruningRetrainEpochs: 30,')
    # print('errorWorsenForFallback: 0.01')
    # print('maxFallbacks: 1,')
    # print(df11.mean(axis=0))
    #
    # print('--------------------')
    #
    # df12 = read_stats(file_path='/home/andrey/works/PythonProjects/nnkx-py/src/2019-05-20_070803/AdultCensus_stats.json')
    # print('afterPruningRetrainEpochs: 30,')
    # print('errorWorsenForFallback: 0.025')
    # print('maxFallbacks: 5,')
    # print(df12.mean(axis=0))
    #
    # print('--------------------')
    # print('--------------------')
    # print('--------------------')

    df21 = read_stats2(file_path='/home/andrey/works/PythonProjects/nnkx-py/src/2019-05-22_002102/Ripley_stats.json')
    df22 = read_stats2(file_path='/home/andrey/works/PythonProjects/nnkx-py/src/2019-05-22_004151/AdultCensus_stats.json')
    # print('afterPruningRetrainEpochs: 30,')
    # print('errorWorsenForFallback: 0.001')
    # print('maxFallbacks: 1,')
    print(df21.mean(axis=0))
    print(df22.mean(axis=0))

    # print('--------------------')
    #
    # df22 = read_stats(file_path='/home/andrey/works/PythonProjects/nnkx-py/src/2019-05-21_231909/Ripley_stats.json')
    # print('afterPruningRetrainEpochs: 30,')
    # print('errorWorsenForFallback: 0.015')
    # print('maxFallbacks: 5,')
    # print(df22.mean(axis=0))

if __name__ == '__main__':
    main()