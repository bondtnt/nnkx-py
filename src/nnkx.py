import copy
from abc import ABC, abstractmethod
import itertools
from typing import List

import pandas as pd
from joblib import Parallel, delayed
import multiprocessing
import pprint
import os
import math
import datetime
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from sklearn.model_selection import train_test_split, StratifiedKFold
from torch.autograd import Variable
import sys
import torch
import math
import torch.nn as nn
import torch.optim as optim
import sklearn.metrics
import torch.nn.functional as F
import numpy as np
from matplotlib.colors import ListedColormap
import matplotlib.pyplot as plt
import time
from itertools import combinations
from sklearn.cluster import DBSCAN
from tqdm import tqdm

# try:
#     NUM_CORES = int(os.environ['NUM_CORES'])
# except:
#     NUM_CORES = 1
from src.data_utils import XYDataset

MAX_FLOAT_BOUND = 999999999.0
MIN_FLOAT_BOUND = -999999999.0

pp = pprint.PrettyPrinter(indent=4)

def timeit(method):
    def timed(*args, **kw):
        ts = time.time()
        result = method(*args, **kw)
        te = time.time()

        millis = (te - ts) * 1000
        if 'log_time' in kw:
            name = kw.get('log_name', method.__name__.upper())
            kw['log_time'][name] = int(millis)
        else:
            print('%r  %2.2f ms' % (method.__name__, millis))
        return result
    return timed

def get_X_y_from_dataloader(_data_loader):
    # Collect all input batches into single X tensor
    # this is not memory friendly, but for now we live with that, as next step will require all inputs.
    inputs_list = []
    targets_list = []
    for batch_idx, (input, target) in enumerate(_data_loader):
        inputs_list.append(input)
        targets_list.append(target)
    X = torch.cat(inputs_list)
    y = torch.cat(targets_list)
    return X, y

# # N is batch size;
# # D_in is input dimension;
# # H is hidden dimension;
# # D_out is y_pred dimension.
# N, D_in, H, D_out = 1, 2, 2, 1
#
# model = torch.nn.Sequential(
#     torch.nn.Linear(D_in, H),
#     # torch.nn.ReLU(),
#     torch.nn.Tanh(),
#     torch.nn.Linear(H, D_out),
#     torch.nn.LogSoftmax(),
# )


# def create_XOR_dataset():
#
#     X = torch.Tensor([
#                       # Class 1
#                       [-1, 1],
#                       [-1, 0.8],
#                       [-0.8, 1],
#                       [-0.8, 0.8],
#
#                       [1, -1],
#                       [0.8, -1],
#                       [1, -0.8],
#                       [0.8, -0.8],
#
#                       # Class 2
#                       [-1, -1],
#                       [-0.8, -1],
#                       [-1, -0.8],
#                       [-0.8, -0.8],
#
#                       [1, 1],
#                       [0.8, 1],
#                       [1, 0.8],
#                       [0.8, 0.8]
#
#                       ])
#
#     # y = torch.Tensor([[-1, 1],
#     #                   [-1, 1],
#     #                   [-1, 1],
#     #                   [-1, 1],
#     #                   [-1, 1],
#     #                   [-1, 1],
#     #                   [-1, 1],
#     #                   [-1, 1],
#     #                   [1, -1],
#     #                   [1, -1],
#     #                   [1, -1],
#     #                   [1, -1],
#     #                   [1, -1],
#     #                   [1, -1],
#     #                   [1, -1],
#     #                   [1, -1]
#     #                   ])
#
#     y = torch.Tensor([[-1],
#                       [-1],
#                       [-1],
#                       [-1],
#                       [-1],
#                       [-1],
#                       [-1],
#                       [-1],
#                       [1],
#                       [1],
#                       [1],
#                       [1],
#                       [1],
#                       [1],
#                       [1],
#                       [1]
#                       ])
#
#     # Y = torch.Tensor([0, 1, 1, 0]).view(-1, 1)
#
#     return X, y
#
# X, y = create_XOR_dataset()
#
# print(model)
#
# criterion = nn.MSELoss()
# # criterion = nn.L1Loss()
# # criterion = nn.NLLLoss()
# # criterion = nn.BCELoss()
# optimizer = optim.SGD(model.parameters(), lr=0.001, momentum=0.1)
#
# # NumEpoches = 100
# # for epoch in range(NumEpoches):
# #
# #     running_loss = 0.0
# #     for i, data in enumerate(X, 0):
# #         inputs = data
# #         labels = y[i]
# #         inputs = Variable(torch.FloatTensor(inputs))
# #         labels = Variable(torch.FloatTensor(labels))
# #         optimizer.zero_grad()
# #         outputs = model(inputs)
# #         loss = criterion(outputs, labels)
# #         loss.backward()
# #         optimizer.step()
# #         running_loss += loss.data[0]
# #         if i % 1000 == 0:
# #             print
# #             "loss: ", running_loss
# #             running_loss = 0.0
#
# loss_fn = torch.nn.MSELoss(size_average=False)
#
# learning_rate = 1e-4
# for t in range(500):
#     y_pred = model(X)
#
#     loss = loss_fn(y_pred, y)
#     print(t, loss.data[0])
#
#     model.zero_grad()
#
#     loss.backward()
#
#     for param in model.parameters():
#         param.data -= learning_rate * param.grad.data
#
# print("Finished training...")
#
# net_vals = model.forward(Variable(torch.FloatTensor(X[10])))
# print(net_vals)

def flatten_list(list):
    return [item for sublist in list for item in sublist]


class RunningUtil:

    def __init__(self, tensorUtil, dataUtil, tableUtil):
        self.tableUtil = tableUtil
        self.dataUtil = dataUtil
        self.tensorUtil = tensorUtil

    def calculateMetric(self, _model, data_loader, sklearn_metric_func):

        _model.eval()

        targets_list = []
        outputs_list = []

        for _, (input, target) in enumerate(data_loader):

            if target.shape.__len__() == 2:
                target = flatten_list(target)

            output = _model(input)
            _, predicted_class = torch.max(output.data, 1)

            outputs_list.append(predicted_class)
            targets_list.append(target)

        y_true = flatten_list(targets_list)
        y_pred = flatten_list(outputs_list)
        return sklearn_metric_func(y_true, y_pred)

            # def confusion(self, prediction, truth):
    #     """ Returns the confusion matrix for the values in the `prediction` and `truth`
    #     tensors, i.e. the amount of positions where the values of `prediction`
    #     and `truth` are
    #     - 1 and 1 (True Positive)
    #     - 1 and 0 (False Positive)
    #     - 0 and 0 (True Negative)
    #     - 0 and 1 (False Negative)
    #
    #     Assumption that both vectors are 1D.
    #     """
    #
    #     confusion_vector = prediction / truth
    #     # Element-wise division of the 2 tensors returns a new tensor which holds a
    #     # unique value for each case:
    #     #   1     where prediction and truth are 1 (True Positive)
    #     #   inf   where prediction is 1 and truth is 0 (False Positive)
    #     #   nan   where prediction and truth are 0 (True Negative)
    #     #   0     where prediction is 0 and truth is 1 (False Negative)
    #
    #     true_positives = torch.sum(confusion_vector == 1).item()
    #     false_positives = torch.sum(confusion_vector == MAX_FLOAT_BOUND).item()
    #     true_negatives = torch.sum(torch.isnan(confusion_vector)).item()
    #     false_negatives = torch.sum(confusion_vector == 0).item()
    #
    #     return true_positives, false_positives, true_negatives, false_negatives

    # function for returning key(index) of the element in the table.
    #--------------------------------------------
    # @ param dataset - data matrix - Torch2D.tensor. It must hold in last column labels!
    # @ param labels - just a convenience matter - labels vector(table) is used to split data into kFolds
    # @ param xValRunner - object that have methods.train(),.test(),.collectStatistics() and.clone().
    # @ param foldsN - number of folds - default is 10.
    # @ param foldsRepeats - usually 3 so that 10 folds x 3 repeats give 30 experiments.
    # @ return array of results with cloned trainers each holding trained model and additional fields with statistics.
    def runXVal(self, X, y, xValRunner, foldsN: int, foldsRepeats: int, model_builder_func, small_train_fraction: float):

        results = []

        classNames = xValRunner.classNames
        batch_size = xValRunner.batchSize
        classNames = xValRunner.classNames
        saveModels = xValRunner.saveModels
        verbose = xValRunner.verbose


        totalRuns = foldsN * foldsRepeats
        i = 0
        for fR in range(0, foldsRepeats):
            skf = StratifiedKFold(n_splits=foldsN)
            fN = 0
            for train_index, test_index in skf.split(X, y):
                i = i + 1
                trainX, trainY = torch.FloatTensor(X[train_index]), torch.LongTensor(y[train_index])
                testX, testY = torch.FloatTensor(X[test_index]), torch.LongTensor(y[test_index])

                #we need to take subset of training set - for neurons activations discretization/clusterization speedup
                train_smallX_np, _, train_smallY_np, _ = train_test_split(trainX.numpy(), trainY.numpy(), test_size=1-small_train_fraction, random_state=42)
                train_smallX, train_smallY = torch.FloatTensor(train_smallX_np), torch.LongTensor(train_smallY_np)

                train_dataset = XYDataset(trainX, trainY)
                train_small_dataset = XYDataset(train_smallX, train_smallY)
                test_dataset = XYDataset(testX, testY)

                trainDataLoader = torch.utils.data.DataLoader(dataset=train_dataset, batch_size=batch_size, shuffle=True)
                smallTrainDataLoader = torch.utils.data.DataLoader(dataset=train_small_dataset, batch_size=batch_size, shuffle=True)
                testDataLoader = torch.utils.data.DataLoader(dataset=test_dataset, batch_size=batch_size, shuffle=True)

                model = model_builder_func()
                log_dict = xValRunner.trainTest(model, classNames, trainDataLoader, smallTrainDataLoader, testDataLoader, i)
                # trainTestRunner.trainTest(mlpCopy, classNames, trainDataLoader, smallTrainDataLoader, testDataLoader, i)
                results.append(log_dict)

                completePercent = ((fR - 1) * foldsN + fN) * 100 / totalRuns
                print('Xval '+ str(completePercent) + '%  finished repeat# '+str(fR) +  ' of '+str(foldsRepeats) + ' fold#' + str(fN) + ' of '+str(foldsN))
                fN = fN + 1
        return results

        # function for returning key(index) of the element in the table.
    # ------------------------------------------
    # @ param testTrainRunner - object that have methods.train(),.test()
    # @ param totalRuns - number, usually >= 30(it is  # of experiments)
    # @ return array of results with cloned trainers each holding trained model and additional fields with statistics.
    #function runningUtil.runTestTrain(testTrainRunner, totalRuns)
    def runTestTrain(self, trainTestRunner, totalRuns, model_builder_func, optimizer_builder_func):

        results = []
        mlp = model_builder_func()
        classNames = trainTestRunner.classNames
        trainDataLoader = trainTestRunner.trainDataLoader
        smallTrainDataLoader = trainTestRunner.smallTrainDataLoader
        testDataLoader = trainTestRunner.testDataLoader
        saveModels = trainTestRunner.saveModels
        verbose = trainTestRunner.verbose

        # print('<mlp> using model.')
        # print(mlp)

        for i in range(0, totalRuns):
            mlpCopy, _ = clone_pytorch_model(mlp, None, model_builder_func, optimizer_builder_func)  #mlp.clone()

            print('<mlpCopy> using model.')
            print(mlpCopy)

            log_dict = trainTestRunner.trainTest(mlpCopy, classNames, trainDataLoader, smallTrainDataLoader, testDataLoader, i)
            results.append(log_dict)

            completePercent = i * 100 / totalRuns
            print('TestTrain '+str(completePercent) + '%  finished repeat# ' + str(i)+' of '+str(totalRuns))

        return results

    # # function for copying table.
    # # --------------------------------------------
    # # @ param table - table.
    # # @ return deep copy of given table
    # def deepcopy(self, obj):
    #     return self.deepcopyX(obj, None)
    #
    # # function for copying table.
    # # --------------------------------------------
    # # @ param table - table.
    # # @ param seen - have obj been seen
    # # @ return deep copy of given table
    # def deepcopyX(self, obj, seen):
    #     # Handle non - tables and previously - seen tables.
    #     if type(obj) != 'table':
    #         objType = torch.typename(obj)
    #         if string.find(objType, "Tensor") != nil) then
    #             return obj:clone()
    #     return obj
    #     end
    #     if seen and seen[obj] then
    #         return seen[obj]
    #     end
    #
    #     # New table; mark it as seen an copy recursively.
    #     s = seen or {}
    #     res = setmetatable({}, getmetatable(obj))
    #     s[obj] = res
    #     for k, v in pairs(obj) do
    #         keyCopy = self.deepcopyX(k, s)
    #         res[keyCopy] = self.deepcopyX(v, s)
    #     return res

    # function for applying function to specific mlp layers.
    # --------------------------------------------
    # @ param container - mlp.
    # @ param layerType - string containing full name of layer(like "nn.ActivationsModifier")
    # @ param f - function to be applied to specified layers types
    def applyToFilteredLayers(self, container, layerType, f):
        for m in container.modules():
            if layerType in torch.typename(m):
                f(m)

    # # function for calculation of confusion matrix.
    # --function
    # runningUtil.calculateConfusion(mlp, dataset, classNames)
    # --  local
    # confusion = optim.ConfusionMatrix(classNames)
    # --
    # for i = 1,  # dataset do
    # --    local
    # example = dataset[i]
    # --    local
    # input = example[1]
    # --    local
    # target = example[2]
    # --    local
    # y_pred = mlp:forward(input)
    # --    confusion: add(y_pred, target)
    # --  end
    # --  confusion: updateValids()
    # --
    # return confusion
    # --end

    # function that returns predicted class label.Inputs are predicted target tensor and map of labels 2 target tensors.
    def findClosestLabelTarget(self, t, labels2Targets):
        min = labels2Targets[1].min()
        max = labels2Targets[1].max()

        label = None
        target = None
        minDistance = MAX_FLOAT_BOUND

        for k, v in labels2Targets.items():

            tmpTarget = labels2Targets[k]
            if type(tmpTarget) == 'number':
                tmpTarget = torch.Tensor(tmpTarget)

            if (t.size(1) == 1 and tmpTarget.size(1) > 1):  # we have ClassNLLCriterion or similar - let's transform this 1D 1x1 vector into one hot vector

                indx = torch.LongTensor
                t.squeeze().view(-1, 1)

                oneHot = torch.Tensor(1, tmpTarget.size(1)).fill(min)
                oneHot.scatter(2, indx, max)
                t = oneHot
            if (tmpTarget.size().size() == 1):
                t = t.squeeze()

                tmpDistance = t.dist(tmpTarget)
            if tmpDistance < minDistance:
                minDistance = tmpDistance
                label = k
                target = tmpTarget

        return label, target


    # function calculates confusionMatrix
    # NB!!!! - works with ClassNLLCriterion or CrossEntropyCriterions - due to expected / actual labels calculations
    # @ param optim - optimizer
    # @ param mlp - basically nn.Sequential container - we call it mlp - MultiLayeredPerceptron
    # @ param data - data set table with tables as rows holding input and targetOutput data
    # @ param classNames - just to initialize confusionMatrix
    # @ param labels2Targets - table holding labels(numeric) as keys and target vectors as corresponding targets(used to find labels)
    # @ param confusionMat - in case it is initialized it will be updated and returned. If it is nil - new one created.
    def calculateConfusion2(self, mlp, data, classNames, labels2Targets, confusionMat):
        if confusionMat == None:
            confusionMat = optim.ConfusionMatrix(classNames)

        X, y = self.dataUtil.tensorsFromDataset(data)
        actualOut = mlp.forward(X)
        _, actualLabel = torch.max(actualOut, 2)

        expectedLabel = y
        confusionMat.batchAdd(actualLabel.view(-1), expectedLabel.view(-1))
        confusionMat.updateValids()
        return confusionMat


    # function calculates confusionMatrix
    # NB!!!! - works with ClassNLLCriterion or CrossEntropyCriterions - due to expected / actual labels calculations
    # @ param optim - optimizer
    # @ param - basically nn.Sequential container - we call it mlp - MultiLayeredPerceptron
    # @ param data - data set table with tables as rows holding input and targetOutput data
    # @ param classNames - just to initialize confusionMatrix
    # @ param labels2Targets - table holding labels(numeric) as keys and target vectors as corresponding targets(used to find labels)
    # @ param confusionMat - in case it is initialized it will be updated and returned. If it is nil - new one created.
    def calculateConfusionMSE(self, mlp, data, classNames, labels2Targets, confusionMat):
        if confusionMat == None:
            confusionMat = optim.ConfusionMatrix(classNames)

        X, y = self.dataUtil.tensorsFromDataset(data)
        _, expectedLabel = torch.max(y, 2)
        actualOut = mlp.forward(X)
        _, actualLabel = torch.max(actualOut, 2)
        confusionMat.batchAdd(actualLabel, expectedLabel)
        confusionMat.updateValids()
        return confusionMat


    # function calculates confusionMatrix
    # @ param optim - optimizer
    # @ param mlp - basically nn.Sequential container - we call it mlp - MultiLayeredPerceptron
    # @ param data - data set table with tables as rows holding input and targetOutput data
    # @ param classNames - just to initialize confusionMatrix
    # @ param labels2Targets - table holding labels(numeric) as keys and target vectors as corresponding targets(used to find labels)
    # @ param confusionMat - in case it is initialized it will be updated and returned. If it is nil - new one created.
    def calculateConfusion3(self, rulegen, treeRoot, data_loader):

        y_train = []
        y_pred = []

        for batch_idx, (batch_input, batch_target) in enumerate(data_loader):
            for row_indx in range(0, batch_input.shape[0]):
                input = batch_input[row_indx, :]
                expectedOutput = batch_target[row_indx]

                actualOutput = rulegen.runTree(treeRoot, input)
                actualLabel = actualOutput.item()  #self.findClosestLabelTarget(actualOutput, labels2Targets)
                expectedLabel = expectedOutput.item()  #self.findClosestLabelTarget(expectedOutput, labels2Targets)
                y_train.append(actualLabel)
                y_pred.append(expectedLabel)

        y_pred = np.vstack(y_pred).flatten()
        y_train = np.vstack(y_train).flatten()
        accuracy_score = sklearn.metrics.accuracy_score(y_train, y_pred)
        f1_score = sklearn.metrics.f1_score(y_train, y_pred, average='micro')
        classification_report = sklearn.metrics.classification_report(y_train, y_pred)
        conf_mat = sklearn.metrics.confusion_matrix(y_train, y_pred, labels=None, sample_weight=None)

        return accuracy_score, f1_score, classification_report, conf_mat

    # function calculates confusionMatrix
    # @ param optim - optimizer
    # @ param mlp - basically nn.Sequential container - we call it mlp - MultiLayeredPerceptron
    # @ param data - data set table with tables as rows holding input and targetOutput data
    # @ param classNames - just to initialize confusionMatrix
    # @ param labels2Targets - table holding labels(numeric) as keys and target vectors as corresponding targets(used to find labels)
    # @ param confusionMat - in case it is initialized it will be updated and returned. If it is nil - new one created.
    def calculateConfusion3(self, rulegen, treeRoot, data_loader):

        y_train = []
        y_pred = []

        for batch_idx, (batch_input, batch_target) in enumerate(data_loader):
            for row_indx in range(0, batch_input.shape[0]):
                input = batch_input[row_indx, :]
                expectedOutput = batch_target[row_indx]

                actualOutput = rulegen.runTree(treeRoot, input)
                if isinstance(actualOutput, int) or isinstance(actualOutput, float):
                    actualLabel = actualOutput
                else:
                    actualLabel = actualOutput.item()  # self.findClosestLabelTarget(actualOutput, labels2Targets)
                expectedLabel = expectedOutput.item()  # self.findClosestLabelTarget(expectedOutput, labels2Targets)
                y_train.append(actualLabel)
                y_pred.append(expectedLabel)

        y_pred = np.vstack(y_pred).flatten()
        y_train = np.vstack(y_train).flatten()
        accuracy_score = sklearn.metrics.accuracy_score(y_train, y_pred)
        f1_score = sklearn.metrics.f1_score(y_train, y_pred, average='micro')
        classification_report = sklearn.metrics.classification_report(y_train, y_pred)
        conf_mat = sklearn.metrics.confusion_matrix(y_train, y_pred, labels=None, sample_weight=None)

        return accuracy_score, f1_score, classification_report, conf_mat


    # # create mini batch
    # def createBatch(self, dataset, batchSize, datasetIndex, inputSize, targetSize):
    #     inputs = torch.Tensor(batchSize, inputSize)
    #     targets = torch.Tensor(batchSize, targetSize)
    #     if (targetSize == 1):
    #         targets = targets.squeeze()  # if we have 1 xbatchSize 2D tensor - ClassNNLCriterion heavily complains, thus we convert it 1D tensor
    #
    #     k = 1
    #     for i in range(datasetIndex, math.min(datasetIndex + batchSize-1, dataset.size())):
    #         # load new sample
    #
    #         input = dataset[i][1].clone()
    #
    #         target = dataset[i][2].clone()
    #         if (targetSize == 1):
    #             target = target.squeeze()
    #
    #         inputs[k] = input
    #         targets[k] = target
    #         k = k + 1
    #
    #
    #     if (k - 1 < batchSize):  # trunc input / target tensors
    #         inputs = inputs.narrow(1, 1, k - 1)
    #         targets = targets.narrow(1, 1, k - 1)
    #
    #     return inputs, targets
    #
    #
    #
    # def shuffleTable(self, t):
    #
    #     rand = math.random
    #     assert (t, "shuffleTable() expected a table, got nil")
    #
    #     iterations =  len(t)
    #
    #     j = None
    #     for i in range(iterations, 2, -1):
    #         j = rand(i)
    #         t[i], t[j] = t[j], t[i]

    def  createBatch(self, dataset, batchSize, datasetIndex, inputSize, targetSize):
        # create mini batch
        inputs = torch.Tensor(batchSize, inputSize)
        targets = torch.Tensor(batchSize, targetSize)
        if (targetSize == 1):
            targets = targets.squeeze()  # if we have 1 xbatchSize 2D tensor - ClassNNLCriterion heavily complains, thus we convert it to 1D tensor
        k = 1
        for i in range(datasetIndex, math.min(datasetIndex + batchSize-1, dataset.size())):
            # load new sample
            input = dataset[i][1].clone()
            target = dataset[i][2].clone()
            if (targetSize == 1):
                target = target.squeeze()
            inputs[k] = input
            targets[k] = target
            k = k + 1

        if (k - 1 < batchSize): # trunc input / target tensors
            inputs = inputs.narrow(1, 1, k - 1)
            targets = targets.narrow(1, 1, k - 1)
        return inputs, targets

    def shuffleTable(self, t):
        rand = math.random
        assert (t, "shuffleTable() expected a table, got None")
        iterations = len(t)
        j = None

        for i in range(iterations, 2, -1):
            j = rand(i)
            t[i], t[j] = t[j], t[i]

    # def visualize_classification_boundary(self, _model, _trainDataLoader, folder_prefix=None):
    #     _model.eval()
    #
    #     h = .02  # step size in the visualization mesh
    #     cmap_light = ListedColormap(['#FFAAAA', '#AAFFAA', '#AAAAFF'])
    #     cmap_bold = ListedColormap(['#FF0000', '#00FF00', '#0000FF'])
    #
    #     # Get mesh evaluation
    #     x_min, x_max = _trainDataLoader.dataset.X[:, 0].min() - 1, _trainDataLoader.dataset.X[:, 0].max() + 1
    #     y_min, y_max = _trainDataLoader.dataset.X[:, 1].min() - 1, _trainDataLoader.dataset.X[:, 1].max() + 1
    #     xx, yy = np.meshgrid(np.arange(x_min, x_max, h), np.arange(y_min, y_max, h))
    #     XY_mesh = torch.FloatTensor(np.c_[xx.ravel(), yy.ravel()])
    #     _, Z = torch.max(_model(XY_mesh), 1)
    #
    #     # get train data evaluation
    #     train_predict_list = []
    #     X_train = []
    #     for batch_idx, (input, target) in enumerate(_trainDataLoader):
    #         outputs = _model(input)  # Do the forward pass
    #         _, predicted = torch.max(outputs.data, 1)
    #         train_predict_list.append(predicted)
    #         X_train.append(input.squeeze(0))
    #
    #     X_train = np.vstack(X_train)
    #     train_predict = np.vstack(train_predict_list)
    #
    #     # Put the result into a color plot
    #     Z = Z.numpy()
    #     Z = Z.reshape(xx.shape)
    #     plt.figure()
    #     plt.pcolormesh(xx, yy, Z, cmap=cmap_light)
    #
    #     # Plot also the training points
    #     # XY_mesh = XY_mesh.numpuy()
    #     plt.scatter(X_train[:, 0], X_train[:, 1], c=train_predict.flatten(), cmap=cmap_bold, edgecolor='k', s=20)
    #     plt.xlim(xx.min(), xx.max())
    #     plt.ylim(yy.min(), yy.max())
    #     plt.title("Classification boundary for trained model")
    #
    #     if not folder_prefix is None:
    #         file_name = os.path.join(folder_prefix, "ann_nnkx_" + str(time.time())+'.png')
    #         plt.savefig(file_name, dpi=None, facecolor='w', edgecolor='w', orientation='portrait')
    #     else:
    #         plt.show()

    @timeit
    def train(self, _model, criterion, optimizer_builder_func, data_loader, args, epochs_to_print=100):
        optimizer = optimizer_builder_func(_model)
        _model.train(True)
        # time = sys.clock()
        loss_history = []
        accuracy_history = []
        f1_history = []

        # for epoch in range(1, args.maxEpochs):
        for epoch in range(args.epochs):
            # print("<trainer> online epoch # " + str(epoch) + ' [batchSize = ' + str(args.batchSize) + ']')

            items_total = 0

            epoch_accuracies = []
            epoch_f1_scores = []
            epoch_losses = []

            for batch_idx, (input, target) in enumerate(data_loader):

                # # Convert torch tensor to Variable
                input = Variable(input)
                target = Variable(target)

                _model.train(True)  # Put the network into training mode
                optimizer.zero_grad()  # Clear off the gradients from any past operation

                outputs = _model(input)  # Do the forward pass

                _target = target.data
                if len(target.shape) > 1:  # if batch_size == 1
                    _target = target.data.squeeze(-1)
                loss = criterion(outputs, _target)  # Calculate the loss
                loss.backward()  # Calculate the gradients with help of back propagation

                # if 'LBFGS' in str(type(optimizer)):
                def closure():
                    optimizer.zero_grad()
                    out = _model(input)
                    loss = criterion(out, target)
                    # print('loss:', loss.item())
                    loss.backward()
                    return loss

                optimizer.step(closure=closure)  # Ask the optimizer to adjust the parameters based on the gradients
                # else:
                #     optimizer.step()

                # Record the correct predictions for training data
                items_total += target.size(0)
                _, predicted = torch.max(outputs.data, 1)

                # conf_mat = sklearn.metrics.confusion_matrix(target.data.squeeze(-1), predicted, labels=None, sample_weight=None)

                epoch_losses.append(loss.item())
                accuracy_score = sklearn.metrics.accuracy_score(_target, predicted)
                epoch_accuracies.append(accuracy_score)
                f1_score = sklearn.metrics.f1_score(target.data, predicted)
                epoch_f1_scores.append(f1_score)
                # classification_report = sklearn.metrics.classification_report(target.data.squeeze(-1), predicted)

            # Book keeping
            # Record the loss & metrics
            full_epoch_loss = np.asarray(epoch_losses).mean()
            full_epoch_accuracy = np.asarray(epoch_accuracies).mean()
            full_f1_score = np.asarray(epoch_f1_scores).mean()

            loss_history.append(full_epoch_loss)
            accuracy_history.append(full_epoch_accuracy)
            f1_history.append(full_f1_score)

            loss = loss / data_loader.__len__()
            if (epoch + 1) % epochs_to_print == 0:
                print('Epoch %d/%d, Loss: %.8f, Accuracy: %.8f, F1: %.8f' % (epoch + 1, args.epochs, full_epoch_loss, full_epoch_accuracy, full_f1_score))

                # self.visualize_classification_boundary(_model, data_loader)

        return loss_history, accuracy_history

        # #time taken
        # #time = sys.clock() - time
        # #time = time / dataset:size()
        # #print("<trainer> time to learn 1 sample = "+str(time * 1000))
        # return self.test(self, _model, data_loader, classNames, opt.batchSize)
        # #return confusion

    @timeit
    def test(self, _model, data_loader, criterion, args):
        _model.eval()

        items_correct = 0
        items_total = 0

        # for batch_idx, (input, target) in enumerate(data_loader):
        #     outputs = _model(input)  # Do the forward pass
        #
        #     loss = criterion(outputs, target.squeeze(-1))  # Calculate the loss_train
        #
        #     # Record the correct predictions for training data
        #     items_total += target.size(0)
        #     _, predicted = torch.max(outputs.data, 1)
        #     items_correct += (predicted == target.data.squeeze(-1)).sum().item()
        #
        #     # conf_mat = sklearn.metrics.confusion_matrix(target.data.squeeze(-1), predicted, labels=None, sample_weight=None)
        #     # accuracy_score = sklearn.metrics.accuracy_score(target.data.squeeze(-1), predicted)
        #     # f1_score = sklearn.metrics.f1_score(target.data.squeeze(-1), predicted)
        #     # classification_report = sklearn.metrics.classification_report(target.data.squeeze(-1), predicted)
        #
        # print('Train Loss: %.4f' % (loss.data.item()))
        # accuracy = (100 * items_correct / items_total)

        y_train = []
        y_pred = []
        outputs_list = []
        for batch_idx, (input, target) in enumerate(data_loader):
            outputs = _model(input)  # Do the forward pass
            _, predicted = torch.max(outputs.data, 1)
            y_pred.append(predicted.numpy())
            y_train.append(target.squeeze(0).numpy())
            outputs_list.append(outputs)
            items_total = items_total + target.shape[0]

        outputs = torch.cat(outputs_list, dim=0)
        y_pred = np.concatenate(y_pred, axis=0).flatten()
        y_train = np.concatenate(y_train, axis=0).flatten()
        conf_mat = sklearn.metrics.confusion_matrix(y_train, y_pred, labels=None, sample_weight=None)
        loss = criterion(outputs, torch.Tensor(y_train).long()).item()
        accuracy_score = sklearn.metrics.accuracy_score(y_train, y_pred)
        f1_score = sklearn.metrics.f1_score(y_train, y_pred)
        classification_report = sklearn.metrics.classification_report(y_train, y_pred)

        print('Confusion matrix: ')
        print(str(conf_mat))
        print('Test loss=%.8f, accuracy=%.8f, f1=%.8f' % (loss, accuracy_score, f1_score))
        print('Classification report: ')
        print(str(classification_report))

        return accuracy_score

        # # time = datetime.datetime.now().time()
        # inputSize = testData[1][1].size(1)
        # targetSize = testData[1][2].size(1)
        #
        # # test over given dataset
        # #print('<trainer> on testing Set:')
        # for t in range(1, testData.size(), batchSize):
        #     ##disp progress
        #     #xlua.progress(t, testData:size())
        #
        #     # create mini batch
        #     batchInputsX, batchLabelsX = self.createBatch(testData, batchSize, t, inputSize, targetSize)
        #     batchInputs = torch.Tensor(batchSize, inputSize)
        #     batchLabels = torch.Tensor(batchSize, targetSize)
        #     k = 1
        #     for i in range(t, math.min(t+batchSize-1,testData.size())):
        #         # load new sample
        #         input = testData[i][1].clone()
        #         target = testData[i][2].clone()
        #         if targetSize == 1:
        #             target = target.squeeze()
        #
        #         batchInputs[k] = input
        #         batchLabels[k] = target
        #         k = k + 1
        #
        #     if (k-1 < batchSize):  # trunc input/target tensors
        #         batchInputs = batchInputs.narrow(1, 1, k-1)
        #         batchLabels = batchLabels.narrow(1, 1, k-1)
        #
        #     ##test samples
        #     preds = model.forward(batchInputs)
        #
        #     if (classNames.size() == 2 and preds.size(2) == 1):
        #         # Case when for binary problem single y_pred neuron is used.
        #         # Let's transform targets and preds to 2 column tensors.
        #         # now let's make a shaky assumption and find min/max
        #         # it might be that single batch will not contain all labels
        #         min_ = batchLabels.min()
        #         max_ = batchLabels.max()
        #         batchLabels, _, _ = self.tensorUtil.scaleTensor(batchLabels, 1, 2)
        #
        #         def zero2one(x):
        #             if (x > 1):
        #               return 1
        #             elif (x < 0):
        #               return 0
        #             else:
        #               return x
        #
        #         preds.apply_(zero2one)
        #
        #         preds, _, _ = self.tensorUtil.scaleTensor(preds.round(), 1, 2)
        #
        #         batchLabels, _, _ = self.dataUtil.labelsColumnToMatrix(batchLabels, min_, max_)
        #         preds, _, _ = self.dataUtil.labelsColumnToMatrix(preds, min_, max_)
        #
        #       # confusion:
        #     for i in range(1, preds.size(1)):
        #          confusion.add(preds[i], batchLabels[i].squeeze())
        #
        # #   # timing
        # #   time = sys.clock() - time
        # #   time = time / testData:size()
        # #   print("<trainer> time to test 1 sample = " .. (time*1000) .. 'ms')
        #
        # confusion.updateValids()
        # return confusion
        pass


class DataUtil:

    def __init__(self, tableUtil):
        self.tableUtil = tableUtil

    #function for returning indices of folds
    #------------------------------------------
    # @ param dataset - standard dataset table.
    #                   Must contain size function and 2 columns data, labels
    # @ param k -       parameter for splitting dataset.Double, usually = 10 (folds count)
    def cvpartitionKFold(self, labels, k):

        n = len(labels)
        uniqueset, j = self.tableUtil.uniqueTableValues(labels) # todo bug??? seems uniqueTableValues bad, should be
        n_per_class = self.tableUtil.frequencies(labels)
        n_classes =  len(uniqueset)

        jTensor = torch.Tensor(j)


        #  C = { "classes" =, "inds" = {}, "n_classes" = {}, "NumObservations" = {}, "NumTestSets" = {}, "TestSize" = {}, "TrainSize" = {}, "Type" = {}}
        C = {}
        # The non - Matlab fields classes, inds, n_classes are only useful for some methods
        indsTensor = torch.Tensor()
        if (n_classes == 1):
            indsTensor = torch.range(0, (n - 1)).mul(k / n).floor().add(1)
            # inds = floor((0:(n-1))' * (k / n)) + 1;
        else:
            #inds = nan(n, 1);
            indsTensor = torch.Tensor(n).fill(0)
            for i in range(1, n_classes):
                if (i % 2) == 0: # alternate ordering over classes so that the subsets are more nearly the same size
                    #inds(j == i) = floor((0:(n_per_class(i) - 1))' * (k / n_per_class(i))) + 1;
                    range_ = torch.range(0, n_per_class[uniqueset[i]] - 1)
                    value = range_.mul(k / n_per_class[uniqueset[i]]).floor().add(1)
                    indsTensor[jTensor.eq(i)] = value
                else:
                    #inds(j == i) = floor(((n_per_class(i) - 1):-1:0)' * (k / n_per_class(i))) + 1;
                    range_ = torch.range(0, n_per_class[uniqueset[i]] - 1)
                    reversedRange, _ = torch.sort(range_, 1, True)
                    value = reversedRange.mul(k / n_per_class[uniqueset[i]]). floor().add(1)
                    indsTensor[jTensor.eq(i)] = value
                    # r = torch.range(0, n_per_class[i] - 1)
                    # y, _ = torch.sort(r, 1, True)
                    # indx = math.floor(y * (k / n_per_class[i])) + 1
                    # inds[indx:eq(i)]
        C.inds = indsTensor
        C.NumTestSets = k
        indsTable = indsTensor.totable()
        _, jj = self.tableUtil.uniqueTableValues(indsTable)
        # [~, ~, jj] = unique(inds);
        n_per_subset = self.tableUtil.frequencies(jj)
        # n_per_subset = accumarray(jj, 1);
        C.TrainSize = torch.Tensor(n_per_subset).mul(-1). add(n).totable()
        C.TestSize = n_per_subset
        C.NumObservations = n
        C.Type = 'KFold'.lower() #'#string.lower('KFold')

        C.getTrainFold = lambda foldNumber: self.neq(C.inds, foldNumber)
        C.getTestFold = lambda foldNumber: C.inds.eq(foldNumber)

        return C

    def neq(self, table_, value):
        tableCopy = torch.Tensor(table_).clone()  #.totable()

        def notEqual_OneFunc(x):
            if (x != value):
                return 1
            else:
                return 0

        indexes = tableCopy.apply_(notEqual_OneFunc)

        return indexes.type('torch.ByteTensor')

    # LABELSCOLUMNTOMATRIX Transforms labels column into Neural Network
    #                      y_pred layer targets.
    #    [1; 2; 3] is transformed into[1, 0, 0; 0, 1, 0; 0, 0, 1]
    #    we are talking about classfication labels, not regression
    # ------------------------------------------
    # @ param labels - Tensor having labels column - of length 1.
    # @ return table holding target vectors for neural network
    def labelsColumnToMatrix(self, labels, minVal, maxVal):
        if (labels.size().size() > 1):
            assert (labels.size()[2] == 1, 'labels should contain single column')

        labels2Targets = {}
        classN = 0
        freq = {}
        flatLabels = labels.clone(). resize(labels.numel())
        for k, v in enumerate(flatLabels.totable()):
            if (freq[v] == None):
                freq[v] = 1
                classN = classN + 1
            else:
                freq[v] = freq[v] + 1

        classIndexes = {}
        i = 1
        for k, _ in enumerate(freq):
            classIndexes[k] = i
            i = i + 1

        targetLabels = torch.Tensor(flatLabels.size()[1], classN).fill(minVal)
        for i in range(1, flatLabels.size()[1]):
            y = labels[i]
            col = classIndexes[y[1]]
            targetLabels[i][col] = maxVal
            if (labels2Targets[y[1]] == None):
                labels2Targets[y[1]] = targetLabels[i]

        return targetLabels, classN, labels2Targets


    def convertToDataset(self, data, labels):
        dataset = {}
        s = data.size(1)
        for i in range(1, s):
            input = data[i]
            output = labels[i]
            dataset[i] = {input, output}

        dataset.size = lambda data: data.size(1)

        return dataset


    def tensorsFromDataset(self, dataset):
        rowsCount = dataset.size()
        rowLength = dataset[1][1].size(1)
        labelLength = dataset[1][2].size(1)
        X = torch.Tensor(rowsCount, rowLength).fill(0)
        y = torch.Tensor(rowsCount, labelLength).fill(0)
        i = 1
        for _, v in enumerate(dataset):
            if ('function' != type(v)):
                input = v[1]
                output = v[2]
                X[i] = input.view(-1)
                y[i] = output.view(-1)
                i = i + 1

        return X, y


class TableUtil():

    def __init__(self):
        pass

    # function for returning key(index) of the element in the table.
    # --------------------------------------------
    # @ param table - table.
    # @
    # return first key for value
    def keyForValue(self, t, value):
        if 'torch' not in torch.typename(t) or 'Tensor' not in torch.typename(t):
            raise Exception('t should be torch Tensor!')

        for k, v in enumerate(t):
            if (v == value):
                return k

        return None

    # function for returning frequencies of the elements in the table.
    # Used by uniqueTableValues(table)
    # --------------------------------------------
    # @ param table - table.
    # @ return table wich will assign values - indices from uniqueTable mapping to originalTable
    def icForUnique(self, uniqueTable, originalTable):
        # typeUtil.assert (uniqueTable, "table")
        # typeUtil.assert (originalTable, "table")
        if 'torch' not in torch.typename(uniqueTable) or 'Tensor' not in torch.typename(uniqueTable):
            raise Exception('First param should be torch Tensor!')
        if 'torch' not in torch.typename(originalTable) or 'Tensor' not in torch.typename(originalTable):
            raise Exception('Second param should be torch Tensor!')

        ic = {}
        for k, v in enumerate(originalTable):
            ic[k] = self.tableUtil.keyForValue(uniqueTable, v)
        return ic

    # function for returning indices of folds
    # --------------------------------------------
    # @ param table - table.
    def uniqueTableValues(self, table):
        if 'torch' not in torch.typename(table) or 'Tensor' not in torch.typename(table):
            raise Exception('t should be torch Tensor!')

        valset = self.tableUtil.frequencies(table)
        # now convert that valset table keys into normal table with values
        uniqueset = {}
        n = 0
        for k, _ in enumerate(valset):
            n = n + 1
            uniqueset[n] = k

        ic = self.tableUtil.icForUnique(uniqueset, table)

        return uniqueset, ic


    # function for checking table contains element
    #--------------------------------------------
    # @ param X - Torch Tensor.
    # @ param element - value to check.
    # @ return True / False
    def contains(self, X, element):
        if 'torch' not in torch.typename(X) or 'Tensor' not in torch.typename(X):
            raise Exception('t should be torch Tensor!')

        _ones = torch.ones(X.size())
        _zeros = torch.zeros(X.size())
        x_eq = torch.where(X == element, _zeros, _ones)
        return x_eq.sum() > 0


    def unique_keys(self, input):
        if 'torch' not in torch.typename(input) or 'Tensor' not in torch.typename(input):
            raise Exception('t should be torch Tensor!')

        keyset = {}
        n = 0
        for k, v in enumerate(input):
            n = n + 1
            keyset[n] = k
        return keyset

    # function for returning frequencies of the elements in the table.
    # --------------------------------------------
    # @ param table - table.
    # @ return table where keys are values of original table and values are encountering frequencies
    def frequencies(self, table):
        # typeUtil.assert (table, "table")
        if 'torch' not in torch.typename(table) or 'Tensor' not in torch.typename(table):
            raise Exception('t should be torch Tensor!')

        valset = {}
        for _, v in enumerate(table):
            if (valset[v] == None):
                valset[v] = 1
            else:
                valset[v] = valset[v] + 1
        return valset


    # function for returning frequencies of the elements in the table.
    # --------------------------------------------
    # @ param table - table.
    # @ param indexes - torch.ByteTensor({1, 1, 0, 0, 1, 0, 0, 0, 0, 0}) - means result table will hold only rows 1, 2 and 5
    # @ return table where keys are values of original table and values are encountering frequencies
    def rows(self, table, indexes):
        # typeUtil.assert (table, "table")
        # typeUtil.assert (indexes, "ByteTensor")
        if 'torch' not in torch.typename(table) or 'Tensor' not in torch.typename(table):
            raise Exception('first param should be torch Tensor!')
        if 'torch.ByteTensor' != torch.typename(indexes):
            raise Exception('second param should be torch ByteTensor!')

        filteredTable = {}
        i = 1
        for k, v in enumerate(table):
            if (indexes[k] != 0):
                filteredTable[i] = v
                i = i + 1

        # dataset case - update size function
        if (i - 1 > 0 and torch.typename(filteredTable[i - 1]) == "function"):
            filteredTable[i - 1] = None
            filteredTable.size = lambda i: i - 2

        return filteredTable


    # function for flatteninng table.
    # for example torch.Tensor({1, 2, 3, 45}): totable() will give {{1}, {2}, {3}, {45}} and
    # tableUtil.flatten({{1}, {2}, {3}, {45}}) will return {1, 2, 3, 45}
    # --------------------------------------------
    # @ param arr - tensor  #table - table to be flattened.
    # @ return flattened table
    def flatten(self, arr):

        cells_count = arr.numel()
        return torch.reshape(arr, (cells_count))

        # result = {}
        # def _flatten(arr):
        #     # for _, v in ipairs(arr):
        #     for _, v in enumerate(arr):
        #         if type(v) == "table":
        #             flatten(v)
        #         else:
        #             table.insert(result, v)
        #
        # _flatten(arr)
        # return result

    # # function for getting table size (count of keys)
    # #--------------------------------------------
    # # @ param table - for which we will check size
    # # @ return count of table keys
    # def getSize(self, table):
    #     count = 0
    #     for _ in enumerate(table):
    #         count = count + 1
    #     return count

    # # function for shallow copy of tables
    # #----------------------------------------------
    # # @ param table - which will be copied
    # # @ param keyToExclude - key which should not be copied
    # # @ return table shallow copy
    # def copyTableWithoutKey(self, table, keyToExclude):
    #     #copy = {}
    #     for k, v in enumerate(table):
    #     if (k != keyToExclude):
    #         copy[k] = v
    #     return copy


    # # function for tables shallow copying
    # # --------------------------------------------
    # # @ param orig - which will be copied
    # # @ return table shallow copy
    # def shallowcopy(self, orig):
    #     orig_type = type(orig)
    #     copy = None
    #     if orig_type == 'table':
    #         copy = {}
    #         for orig_key, orig_value in enumerate(orig):
    #             copy[orig_key] = orig_value
    #     else: # number, string, boolean, etc
    #         copy = orig
    #
    #     return copy


    # def clone(self, t):  # deep - copy a table
    #     if type(t) != "table":
    #         return t
    #
    #     meta = getmetatable(t)
    #     target = {}
    #     for k, v in enumerate(t):
    #         if type(v) == "table":
    #             target[k] = self.tableUtil.clone(v)
    #         else:
    #             target[k] = v
    #     setmetatable(target, meta)
    #     return target


    # def copy3(self, obj, seen):
    #     # Handle non - tables and previously - seen tables.
    #     if type(obj) != 'table':
    #         return obj
    #     if seen and seen[obj]:
    #         return seen[obj]
    #
    #     # New table; mark it as seen an copy recursively.
    #     s = seen or {}
    #     res = setmetatable({}, getmetatable(obj))
    #     s[obj] = res
    #     for k, v in enumerate(obj):
    #         res[self.copy3(k, s)] = self.copy3(v, s)
    #
    #     return res

    # # function for deep tables print out
    # # --------------------------------------------
    # # @ param orig - which will be copied
    # # @ return table shallow copy
    # def printObj(self, obj, hierarchyLevel):
    #     if (hierarchyLevel == None):
    #         hierarchyLevel = 0
    #     elif (hierarchyLevel == 4):
    #         return 0
    #
    #     whitespace = ""
    #     for i in range(0, hierarchyLevel, 1):2
    #         whitespace = whitespace + "-"
    #
    #     io.write(whitespace)
    #
    #     print(obj)
    #     if (type(obj) == "table"):
    #         for k, v in enumerate(obj):
    #             io.write(whitespace + "-")
    #             if (type(v) == "table"):
    #                 self.tableUtil.printObj(v, hierarchyLevel + 1)
    #             else:
    #                 print(v)
    #     else:
    #         print(obj)



class QuantizationFunction(torch.autograd.Function):
    """
    Function alters input tensor according to passed in quantization table.
    This table holds two columns per neuron. First column is quanized values,
    second column is quantization boundaries (you can treat them as a clusters boundaries
    and quantized values - as clusters centers). All input values
    belonging to quantized region (cluster), i.e. it is between quantized_valued_table
    """

    @staticmethod
    def forward(ctx, input_tensor, quantization_table):
        """
        Function applies pruning mask to input tensor.
        Where mask is 1 - input tensor will be replaced with pruning_empty_value (this
        value depends on activation function used, i.e. sigm - 0.5, tanh - 0.0, relu - 0.0)

        :param ctx: Function context
        :param input_tensor: torch.Tensor
        :param quantization_table: torch.FloatTensor, size is "Neurons count X quantized tables", where
            quantized table size is "quantized values count X 2"
        :return: returns quantized input_tensor.
        """
        # ctx is a context object that can be used to stash information
        # for backward computation
        ctx.quantization_table = quantization_table
        return QuantizationFunction.__quantize(input_tensor, quantization_table)

    @staticmethod
    def backward(ctx, grad_output):
        """
        In contrast to forward here we always replace gradients with zeros where
        pruning_mask is 1.

        :param ctx: Function context
        :param grad_output:
        :return:
        """
        # We return as many input gradients as there were arguments.
        # Gradients of non-Tensor arguments to forward must be None.
        return grad_output, None, None

    @staticmethod
    def __quantize(input_tensor, quantization_table):

        is_empty_table = len(quantization_table.shape) == 1 and quantization_table.shape[0] == 0
        if is_empty_table:
            return input_tensor


        dim = input_tensor.shape[1]

        if dim == 1:
            input_tensor.resize(1, input_tensor.size(0))  # make this 1D tensor a 2D tensor with single row
        # elif dim < 1 or dim > 2:
        #     raise Exception('Input tensor should be either 1D vector or 2D tensor! Instead it have dimensionality = ' + str(dim))

        for neuronNum in range(0, input_tensor.shape[1]):
            neuronClusters = quantization_table[neuronNum]
            # modifiedInputSlow = modifiedInput.clone()

            # # slow cluster centers acquisition
            # for rowNum in range(0, modifiedInput.shape[0]):
            #     value = modifiedInput[rowNum, neuronNum].item()
            #     modifiedInputSlow[rowNum, neuronNum] = __getClusterCenter(value, neuronClusters)
            # # END of slow cluster centers acquisition

            # fast cluster centers acquisition
            centers = neuronClusters[:, 0]  # take first column - centers of clusters to replace neuron activations to
            bounds = neuronClusters[:, 1]  # take second column - boundaries themselves
            if bounds.shape[0] == 1:
                # there is only one cluster center - all neurons inputs should be replaced with that
                mod_input = centers.repeat(input_tensor.size(0), 1)
                input_tensor[:, neuronNum] = mod_input.view(1, -1)
            else:
                bounds_extended = torch.cat((torch.Tensor([MIN_FLOAT_BOUND]), bounds), 0)
                bounds_extended[bounds_extended.shape[0] - 1] = MAX_FLOAT_BOUND  # dunno why but otherwise torch.ge fails
                ge_mat = input_tensor[:, neuronNum].view(-1, 1).ge(bounds_extended)
                indices_of_max_vals = ge_mat.argmax(1)  # across columns
                # indices_of_max_vals = ge_mat.argmax(1) + 1  # across columns
                # # because of +1 (this is needed), we can be out of bounds. Thus max values should be lowered
                # _, max_indx = indices_of_max_vals.max(0)
                # indices_of_max_vals[max_indx] = indices_of_max_vals[max_indx] - 1  # it can be larger only by 1
                mat_to_gather_from = centers.repeat(input_tensor.size(0), 1)
                try:
                    mod_input = mat_to_gather_from.gather(1, indices_of_max_vals.view(-1, 1))
                    input_tensor[:, neuronNum] = mod_input.view(1, -1)  # mod_input.view(1, -1) - makes it a vector
                except:
                    print('Holala!')
                    # for rowNum in range(0, modifiedInput.shape[0]):
                    #     value = modifiedInput[rowNum, neuronNum].item()
                    #     modifiedInput[rowNum, neuronNum] = __getClusterCenter(value, neuronClusters)
                    print('input_tensor')
                    print(input_tensor)
                    raise Exception('Something went wrong!!!!')

            # END OF fast cluster centers acquisition
        if dim == 1:
            input_tensor.resize_as_(input)
        return input_tensor

    @staticmethod
    def __getClusterCenter(value, clusters):
        # if clusters.shape[0] == 1:
        #     matching_value = clusters[0, 0]
        # else:
        #     # print(value)
        #     # print(clusters)
        #     # print('----------------------')
        #     matching_cluster_index = clusters[:, 1].ge(value)
        #     if matching_cluster_index.sum() > 0:
        #         matching_value = clusters[matching_cluster_index][0, 0]
        #     else:
        #         matching_value = clusters[clusters.shape[0]-1][1]
        #     # print(matching_value)
        #     # print('----------------------')
        # return matching_value

        lowerBound = MIN_FLOAT_BOUND
        for i in range(0, clusters.shape[0]):
            upperBound = clusters[i][1]
            if lowerBound < value <= upperBound:
                return clusters[i][0]
            lowerBound = upperBound
        return clusters[clusters.shape[0]-1][1]  # this is actually a '-inf', as second column contains lower bound of clusters and last row will be unbounded to negative side.


class QuantizationLayer(nn.Module):

    NAME = 'QuantizationLayer'

    def __init__(self, clusters_table=torch.FloatTensor()):
        nn.Module.__init__(self)
        # requires_grad=False --> we don't want to optimize our parameters
        clusters_table.requires_grad = False
        self.clusters_table = nn.Parameter(clusters_table, requires_grad=False)
        self.clusters_table_backup = nn.Parameter(torch.FloatTensor(), requires_grad=False)

    def forward(self, input):
        return QuantizationFunction.apply(input, self.clusters_table)

    def extra_repr(self):
        # (Optional)Set the extra information about this module. You can test
        # it by printing an object of this class.
        return 'clusters_table={}'.format(
            self.clusters_table
        )

    def build_and_set_quantization_table(self, X, eps=1000000): # everything will be rounded to 6 digits after comma
        """
        Function acccepts all inputs and builds (initial) quantization table from these inputs.
        This table is set in this module.
        :param X: torch.FloatTensor()
        :return: None
        """
        neuron_count = X.shape[1]
        clusters_tables = {}
        for neuronIndx in range(0, neuron_count):
            singleNeuronActivations = X[:, neuronIndx]
            singleNeuronActivations, _ = singleNeuronActivations.sort(0, descending=False)

            singleNeuronUniqueActivationsRoundedLargeUnique = torch.unique(torch.round(singleNeuronActivations * eps).long(), sorted=False, return_inverse=False)
            singleNeuronUniqueActivationsRoundedUnique = singleNeuronUniqueActivationsRoundedLargeUnique.float() / eps
            print('Neuron indx:' +str(neuronIndx)+'; unique outputs:' + str(singleNeuronUniqueActivationsRoundedUnique.shape[0]))
            singleNeuronUniqueActivationsUniqueRoundedSorted, _ = singleNeuronUniqueActivationsRoundedUnique.sort(0, descending=False)
            clusterBoundaries = torch.add(singleNeuronUniqueActivationsUniqueRoundedSorted[1:], singleNeuronUniqueActivationsUniqueRoundedSorted.clone()[:-1]) / 2
            clusterBoundaries = torch.cat((clusterBoundaries, torch.FloatTensor([MAX_FLOAT_BOUND])), dim=0)
            clusters = torch.stack([singleNeuronUniqueActivationsUniqueRoundedSorted, clusterBoundaries], dim=1)
            clusters_tables[neuronIndx] = clusters
            # initialClusters = torch.stack([singleNeuronActivations.clone(), singleNeuronActivations.clone()], dim=1)
            # for i in range(0, (initialClusters.shape[0]-1)):
            #     initialClusters[i][1] = initialClusters[i][1] + torch.abs(initialClusters[i][1] - initialClusters[i+1][1]) / 2
            # initialClusters[initialClusters.shape[0]-1][1] = MAX_FLOAT_BOUND  # last cluster is not bounded.(first one is not bounded either)
            # clusters_table_from_X[neuronIndx] = initialClusters

        longest_table_size = 0
        for key, c_table in clusters_tables.items():
            if longest_table_size < c_table.shape[0]:
                longest_table_size = c_table.shape[0]

        # as we are storing everything in one table - we need to
        # fill in short clusters_tables (of various length for various neurons) to make them of equal length
        # let's fill rest of all short tables with float['inf'] values
        clusters_table_from_X = torch.zeros(neuron_count, longest_table_size, 2, requires_grad=False)
        for key, c_table in clusters_tables.items():
            c_table_rows_count = c_table.shape[0]
            if c_table_rows_count < longest_table_size:
                rows_to_add_count = longest_table_size - c_table_rows_count
                rows_to_add = torch.FloatTensor([MAX_FLOAT_BOUND]).repeat(rows_to_add_count, 2)
                clusters_table_from_X[key, :, :] = torch.cat([c_table, rows_to_add], dim=0)
            else:
                clusters_table_from_X[key, :, :] = c_table

        clusters_table_from_X.requires_grad = False
        self.clusters_table.data = clusters_table_from_X

    def set_merged_clusters_table(self, neuron_indx, clusters_table):
        """
        This method accepts clusters table and sets it to specified neuron.
        The trick is that one neuron can have table of length 100 and new table can have length 90 (for example).
        We will have to grow clusters_table (by adding new rows).

        :param neuron_indx:
        :param clusters_table:
        :return:
        """

        clusters_table_len = self.clusters_table.data[neuron_indx, :, :].shape[0]
        clusters_table_rows_added = self.__add_rows_to_merged_clusters(clusters_table_len, clusters_table)
        self.clusters_table.data[neuron_indx, :, :] = clusters_table_rows_added

    def __add_rows_to_merged_clusters(self, clusters_table_len, clusters_table):
        if not clusters_table is None and clusters_table.shape[0] < clusters_table_len:
            rows_to_add_count = clusters_table_len - clusters_table.shape[0]
            rows_to_add = torch.FloatTensor([MAX_FLOAT_BOUND]).repeat(rows_to_add_count, 2)
            clusters_table = torch.cat([clusters_table, rows_to_add], dim=0)
        return clusters_table

class ActivationModifier(nn.Module):
    '''
    Class extends nn module - this is essentially a layer which allows to modify outputs
    on forward pass. Backward pass is not modified.
    This layer is used to modify activations for discretization and clusterization of activation values.
    These are essential steps for rules extraction.
    Activations modifications is performed by injected modifier functions.
    Thus activations modification strategy can be easily changed.
    '''

    def __init__(self, layer_size):
        nn.Module.__init__(self)
        self.layer_size = layer_size
        self.modifierFunction = self.emptyModifierFunction
        self.clustersTableBackup = None
        self.fast_mode = False

    def forward(self, X):
        return self.modifierFunction(X)

    # -------------------------------------------
    def reset(self):
        self.modifierFunction = self.emptyModifierFunction

    # def updateOutput(self, input):
    #     modifiedInput = self.modifierFunction(input)
    #     self.y_pred.resizeAs(modifiedInput).copy(modifiedInput)
    #     return self.y_pred
    #
    # def updateGradInput(self, input, gradOutput):
    #     self.gradInput.resizeAs(gradOutput).copy(gradOutput)
    #     return self.gradInput

    def emptyModifierFunction(self, input):
        return input

    def afterComaDigitsDiscretizerFunc(self, afterComaDigits):
        '''
        function just rounds input matrix - doing discretization of continuous inputs
        '''

        def f(input):
            inputCopy = input.clone()
            if (afterComaDigits == 0):
                return inputCopy.round()
            else:
                #inputCopy.mul(10 ^ afterComaDigits).round().div(10 ^ afterComaDigits)
                return torch.round(inputCopy * 10 ** afterComaDigits) / (10 ** afterComaDigits)
            return inputCopy

        return f

    # function which modifies input vector according to given tableOfIntervals.
    # For each neuron( for each input value in input vector) it uses appropriate intervals table and finds
    # matching interval (which holds given input(activation) or to which given input is located closer), then function
    # replaces activation value with cluster center (cluster formed by interval - cluster center is interval mean)
    # @ param tableOfIntervals - table where for each neuron(indexed from 1 to totalNumberOfNeuronsInLayer
    # ( in case you have 2D or 3D input tensor - it is flattened))
    # @ result tableOfClusters. Where each cluster is 2D tensor where first column is cluster center and second column
    # is cluster upper boundary.
    def clusterizerFunc(self, tableOfIntervals, min, max):
        tableOfClusters = self.buildClustersTable(tableOfIntervals, min, max)

        def f(input):
            flatInput = input.view(input.numel())
            modifiedFlatInput = flatInput.clone()
            dim = modifiedFlatInput.size().size()
            for i in range(1, input.numel()):
                clusters = tableOfClusters[i]
                if dim == 1:
                    modifiedFlatInput[i] = self.getClusterCenter(flatInput[i], clusters)
                elif dim == 2:
                    modifiedFlatInput[i][1] = self.getClusterCenter(flatInput[i][1], clusters)
                else:
                    raise Exception('Flattened tensor should be either vector or 2D tensor having single column! Instead it have dim = ' + str(dim))
                return modifiedFlatInput.resizeAs(input)

        return f(input)

    # function which modifies input vector according to given tableOfClusters.
    # For each neuron( for each input value in input vector) it uses appropriate clusters table and finds
    # matching cluster, then function replaces activation value with cluster center
    # (1st column in cluster 2 column tensor, 2nd column is cluster upper bound)
    # @ param tableOfClusters - table where for each neuron(indexed from 1 to totalNumberOfNeuronsInLayer
    # ( in case you have 2D or 3D input tensor - it is flattened))
    # @ result function which will modify input tensor values according to tableOfClusters.
    def clusterizerFuncFromClusters(self, tableOfClusters, min, max):
        def f(input):
            modifiedInput = input.clone()
            dim = modifiedInput.shape[1]

            if dim == 1:
                modifiedInput.resize(1, modifiedInput.size(0))  # make this 1D tensor a 2D tensor with single row
            # elif dim < 1 or dim > 2:
            #     raise Exception('Input tensor should be either 1D vector or 2D tensor! Instead it have dimensionality = ' + str(dim))

            # def _get_modified_output(neuronNum, input_size_0):
            #     neuronClusters = tableOfClusters[neuronNum]
            #     # slow cluster centers acquisition
            #     modified_input = torch.Tensor(input_size_0)
            #     for rowNum in range(0, modifiedInput.shape[0]):
            #         value = modifiedInput[rowNum, neuronNum].item()
            #         modified_input[neuronNum] = self.getClusterCenter(value, neuronClusters)
            #     # END of slow cluster centers acquisition
            #     return modified_input.view(-1, 1), neuronNum  # let's make our vector a matrix with single column
            #
            # neuron_count = modifiedInput.shape[1]
            # input_rows_count = input.size(0)
            # # NUM_CORES * 2
            # modified_inputs = Parallel(n_jobs=8)(delayed(_get_modified_output)(neuronNum, input_rows_count) for neuronNum in range(0, neuron_count))
            # modified_inputs_mapped = [0] * len(modified_inputs)
            # for mod_i in modified_inputs:
            #     _mod_input_tensor = mod_i[0]
            #     _node_num = mod_i[1]
            #     modified_inputs_mapped[_node_num] = _mod_input_tensor
            #
            # modifiedInput = torch.cat(modified_inputs_mapped, 1)

            for neuronNum in range(0, modifiedInput.shape[1]):
                neuronClusters = tableOfClusters[neuronNum]
                modifiedInputSlow = modifiedInput.clone()

                # slow cluster centers acquisition
                for rowNum in range(0, modifiedInput.shape[0]):
                    value = modifiedInput[rowNum, neuronNum].item()
                    modifiedInputSlow[rowNum, neuronNum] = self.getClusterCenter(value, neuronClusters)
                # END of slow cluster centers acquisition

                # # fast cluster centers acquisition
                # centers = neuronClusters[:, 0]  # take first column - centers of clusters to replace neuron activations to
                # bounds = neuronClusters[:, 1]  # take second column - boundaries themselves
                # if bounds.shape[0] == 1:
                #     # there is only one cluster center - all neurons inputs should be replaced with that
                #     mod_input = centers.repeat(input.size(0), 1)
                #     modifiedInput[:, neuronNum] = mod_input.view(1, -1)
                # else:
                #     bounds_extended = torch.cat((torch.Tensor([MIN_FLOAT_BOUND]), bounds), 0)
                #     bounds_extended[bounds_extended.shape[0] - 1] = MAX_FLOAT_BOUND  # dunno why but otherwise torch.ge fails
                #     ge_mat = input[:, neuronNum].view(-1, 1).ge(bounds_extended)
                #     indices_of_max_vals = ge_mat.argmax(1)  # across columns
                #     # indices_of_max_vals = ge_mat.argmax(1) + 1  # across columns
                #     # # because of +1 (this is needed), we can be out of bounds. Thus max values should be lowered
                #     # _, max_indx = indices_of_max_vals.max(0)
                #     # indices_of_max_vals[max_indx] = indices_of_max_vals[max_indx] - 1  # it can be larger only by 1
                #     mat_to_gather_from = centers.repeat(input.size(0), 1)
                #     try:
                #         mod_input = mat_to_gather_from.gather(1, indices_of_max_vals.view(-1, 1))
                #         modifiedInput[:, neuronNum] = mod_input.view(1, -1)  # mod_input.view(1, -1) - makes it a vector
                #     except:
                #         print('Holala!')
                #         for rowNum in range(0, modifiedInput.shape[0]):
                #             value = modifiedInput[rowNum, neuronNum].item()
                #             modifiedInput[rowNum, neuronNum] = self.getClusterCenter(value, neuronClusters)
                #
                # # END OF fast cluster centers acquisition
            if dim == 1:
                modifiedInput.resize_as_(input)
            return modifiedInput

        return f

    def getClusterCenter(self, value, clusters):
        # if clusters.shape[0] == 1:
        #     matching_value = clusters[0, 0]
        # else:
        #     # print(value)
        #     # print(clusters)
        #     # print('----------------------')
        #     matching_cluster_index = clusters[:, 1].ge(value)
        #     if matching_cluster_index.sum() > 0:
        #         matching_value = clusters[matching_cluster_index][0, 0]
        #     else:
        #         matching_value = clusters[clusters.shape[0]-1][1]
        #     # print(matching_value)
        #     # print('----------------------')
        # return matching_value

        lowerBound = MIN_FLOAT_BOUND
        for i in range(0, clusters.shape[0]):
            upperBound = clusters[i][1]
            if lowerBound < value <= upperBound:
                return clusters[i][0]
            lowerBound = upperBound
        return clusters[clusters.shape[0]-1][1]  # this is actually a '-inf', as second column contains lower bound of clusters and last row will be unbounded to negative side.

    # #  
    # lowerBound = MAX_FLOAT_BOUND * -1
    # #
    # for i = 1, clusters:size()[1]
    # do
    # --    
    # upperBound = clusters[i][1]
    # -- if (value > lowerBound and value <= upperBound)
    # then
    # --
    # return clusters[i][2]
    # --    
    # --    lowerBound = upperBound
    # --  
    # --
    # return clusters[clusters:size(1)][2]
    #
    # --  
    # clustersBoundaries = clusters[{{}, {1}}]
    # -- if (value <= clusters[1][1])
    # then
    # --
    # return clusters[1][2]
    # --  elif(value > clusters[clusters:size(1)][1])
    # then
    # --
    # return clusters[clusters:size(1)][2]
    # -- else
    # --  print('value = '..value)
    # --  print('clusters = '..clusters)
    # --    
    # r = torch.range(1, clusters:size(1))
    # --    
    # indexesOfLargerBounds = clustersBoundaries:gt(value)
    # --    
    # upperBoundIndex = r[indexesOfLargerBounds][1]
    # --
    # return clusters[upperBoundIndex - 1][2]
    # --  
    # --  error('Was unable to find clusters center using value='..value..
    # ' and clusters='..clusters)
    # 

    # function transforms intervals table(containing intervals tensors for each neuron)
    # @ param tableOfIntervals:
    # can be seen as:
    #  intervalsForNeuron1 = torch.Tensor({{0.0, 0.1}, {0.4, 0.6}, {0.9, 1.0}})  -- here we see 3 intervals found during known activations merging
    #  intervalsForNeuron2 = ...
    #  tableOfIntervals = {}
    # tableOfIntervals[1] = intervalsForNeuron1
    # tableOfIntervals[2] = intervalsForNeuron2
    # this is structure of input - tableOfIntervals;
    # --------------------------------------------------
    # @ result - same as above, but tensors are a bit different
    # column 1 - denotes center of cluster, and column 2 denotes upper bound of cluster.
    # Such data strcuture allows to find cluster to which belongs input(using bounds of clusters)
    # and replace it with cluster center value (corresponding value in first column)
    def buildClustersTable(self, tableOfIntervals, min, max):
        clustersTable = {}
        for neuronIndex, intervals in enumerate(tableOfIntervals):
            clusters = ActivationModifier.buildClustersFromIntervals(intervals, min, max)
            clustersTable[neuronIndex] = clusters
        return clustersTable;

    # @ param intervals - 2D tensor with 2 columns denoting beginnning and  of interval
    # @ result - 2D tensor, where 1st column is cluster center, and 2nd column is cluster upper boundary
    def buildClustersFromIntervals(self, intervals, min, max):
        clusters = torch.DoubleTensor(intervals.size()).fill(0)
        size = intervals.size()[1]
        lowerClusterBoundary = min
        for i in range(1, size - 1):
            upperClusterBoundary = intervals[i][2] + torch.abs((intervals[i][2] - intervals[i + 1][1]) / 2)
            clusterCenter = lowerClusterBoundary + torch.abs((lowerClusterBoundary - upperClusterBoundary) / 2)
            clusters[i][1] = clusterCenter
            clusters[i][2] = upperClusterBoundary
            lowerClusterBoundary = upperClusterBoundary
        clusterCenter = lowerClusterBoundary + torch.abs((lowerClusterBoundary - max) / 2)
        clusters[size][1] = clusterCenter
        clusters[size][2] = max
        return clusters

    def buildAndSetClusterizerModifierFunction(self, activations):
        if list == type(activations):
            activations = torch.stack(activations)

        min = activations.min()
        max = activations.max()
        clustersTable = {}
        eps = 1000000
        for neuronIndx in range(0, activations.shape[1]):
            singleNeuronActivations = activations[:, neuronIndx]
            singleNeuronActivations, _ = singleNeuronActivations.sort(0, descending=False)

            singleNeuronUniqueActivationsRoundedLargeUnique = torch.unique(torch.round(singleNeuronActivations * eps).long(), sorted=False, return_inverse=False)
            singleNeuronUniqueActivationsRoundedUnique = singleNeuronUniqueActivationsRoundedLargeUnique.float() / eps
            singleNeuronUniqueActivationsUniqueRoundedSorted, _ = singleNeuronUniqueActivationsRoundedUnique.sort(0, descending=False)
            clusterBoundaries = torch.add(singleNeuronUniqueActivationsUniqueRoundedSorted[1:], singleNeuronUniqueActivationsUniqueRoundedSorted.clone()[:-1]) / 2
            clusterBoundaries = torch.cat((clusterBoundaries, torch.FloatTensor([MAX_FLOAT_BOUND])), dim=0)
            clusters = torch.stack([singleNeuronUniqueActivationsUniqueRoundedSorted, clusterBoundaries], dim=1)
            clustersTable[neuronIndx] = clusters
            # initialClusters = torch.stack([singleNeuronActivations.clone(), singleNeuronActivations.clone()], dim=1)
            # for i in range(0, (initialClusters.shape[0]-1)):
            #     initialClusters[i][1] = initialClusters[i][1] + torch.abs(initialClusters[i][1] - initialClusters[i+1][1]) / 2
            # initialClusters[initialClusters.shape[0]-1][1] = MAX_FLOAT_BOUND  # last cluster is not bounded.(first one is not bounded either)
            # clustersTable[neuronIndx] = initialClusters
        self.clustersTable = clustersTable
        self.modifierFunction = self.clusterizerFuncFromClusters(clustersTable, min, max)


class ActivationRecorder(nn.Module):

    # Class extends nn module - this is essentially a layer which saves all incoming inputs (if recordFlag == True).
    # It should be placed behind pruning or linear layers and afterwards network should be
    # fed with full dataset. Afterwards this layer will have all activations of all neurons
    # from previous layer. These activations are input to next rules extraction step - discretization.
    # --------------------------------------------
    def __init__(self, recordFlag):
        nn.Module.__init__(self)
        self.recordFlag = recordFlag
        self.inputs_list = []
        self.reset()

    def forward(self, X):
        if self.recordFlag == True:
            self.inputs_list.append(X)
        return X

    # Resetting activations table.
    # --------------------------------------------
    def reset(self):
        self.activations = []
        self.inputs_list = []

    def get_activations(self):
        return flatten_list(self.inputs_list)

    # def updateOutput(self, input):
    #     self.y_pred.resizeAs(input).copy(input)
    #     if self.recordFlag == True:
    #         y_pred = self.y_pred.clone()
    #         outputTensor = y_pred.resize(y_pred.size()[1], 1).t()
    #
    #         if self.activations.nDimension() == 0:
    #             self.activations = outputTensor
    #         else:
    #             self.activations = torch.cat(self.activations, outputTensor, 1)
    #
    #     return self.y_pred
    #
    # def updateGradInput(self, input, gradOutput):
    #     self.gradInput.resizeAs(gradOutput).copy(gradOutput)
    #     return self.gradInput

class ADiscretizeMetricStrategy(ABC):
    """
    Used in `clusterize_discretize` method.
    During discretization step merging of neurons outputs is performed.
    If some metric (loss, accuracy, f1) is worsened last merge is reverted and new candidates for merge are explored.

    This class formulates strategy that allows to
        a) lower initial metric value so that we will allow small decrease in metric during merge
            This will result in less crisp decision boundary and in theory will lower extracted rules accuracy
            but at the same time rules will be simpler and more comprehensible.
        b) allows to update metric during merge to higher values. In cases when merge of clusters
            increases metric value. This is greedy strategy which will result in crispest possible rules of high
            complexity. Basically it can mean that almost no discretization will be done.
    """
    @abstractmethod
    def init_metric_orig(self, original_metric):
        pass

    @abstractmethod
    def update_metric_orig(self, metric_orig, metric_new):
        pass

class DiscretizeMetricStrategyCrispRules(ADiscretizeMetricStrategy):
    def init_metric_orig(self, original_metric):
        return original_metric

    def update_metric_orig(self, metric_orig, metric_new):
        return metric_new

    def __repr__(self):
        return 'Discretize_Greedy'

class DiscretizeMetricStrategyNormalRules(ADiscretizeMetricStrategy):
    def init_metric_orig(self, original_metric):
        return original_metric

    def update_metric_orig(self, metric_orig, metric_new):
        return metric_orig

    def __repr__(self):
        return 'Discretize_1.0'

class DiscretizeMetricStrategyRelaxedRules(ADiscretizeMetricStrategy):
    def __init__(self, worsen_coef):
        self.worsen_coef = worsen_coef

    def init_metric_orig(self, original_metric):
        return original_metric * self.worsen_coef

    def update_metric_orig(self, metric_orig, metric_new):
        return metric_orig

    def __repr__(self):
        return 'Discretize_'+str(self.worsen_coef)

class ActivationsDiscretizer(nn.Module):
    '''
    Class implementing ActivationsDiscretizer module.
    It holds logic for dicretizing and clusterizing activation values of
    'nn.ActivationModifier' layers in layered neural network
    --------------------------------------------
    '''

    def __init__(self, runningUtil, tableUtil, modelPingUtil, activationModifierUtil):
        nn.Module.__init__(self)
        self.activationModifierUtil = activationModifierUtil
        self.modelPingUtil = modelPingUtil
        self.tableUtil = tableUtil
        self.runningUtil = runningUtil

    # This function travels through passed mlp, looks for ActivationModifier layers and inits them
    # with afterComaDigitsDiscretizerFunc(d) - where d is number of digits to be left after comma.
    # this basically discretizes all activations across whole network.
    # Afterwards confusion is calculated(via passed in function - confusionCalculatorFunc)
    # and if it does not drop d is decreased and procedure repeated.
    # Operation continues until minimal d that does not increase error is found
    def discretize(self, mlp, initialD, lossCalcFunc):
        # confusionD = self.calculateConfusion(mlp, dataset, classNames)
        d = initialD  # let's round activations to d digits after coma. PLease note that 0 digits is Ok with us here.
        originalLoss = lossCalcFunc()
        continuationFlag = True
        while continuationFlag == True:

            def setModifierFunc(layer):
                layer.modifierFunction = ActivationModifier().afterComaDigitsDiscretizerFunc(d)

            self.runningUtil.applyToFilteredLayers(mlp, 'ActivationModifier', setModifierFunc)

            newLoss = lossCalcFunc()  # calculateConfusion(mlp, dataset, classNames)
            if newLoss > originalLoss or d < 0:
                d = d + 1  # rollback
                continuationFlag = False
                # Revert back appropriate discretization level
                self.runningUtil.applyToFilteredLayers(mlp, 'ActivationModifier', setModifierFunc)
            else:
                d = d - 1

        return d

    # # This function sets discretization to each neuron (in random order) starting from last layer.
    # def discretize_per_neuron(self, mlp, maxD, loss_calc_func):
    #     for modifier_layer in reversed(mlp.modules()):
    #         if 'ActivationModifier' in torch.typename(modifier_layer):
    #             layer_size = modifier_layer.layer_size
    #             neurons_
    #
    #             rand_neurons_indexes = torch.randperm(layer_size)
    #
    #
    #             def setModifierFunc(layer):
    #                 layer.modifierFunction = ActivationModifier().afterComaDigitsDiscretizerFunc(d)



    def clone_dict_of_tensors(self, dict):
        dict_clone = dict.copy()
        for key, value in dict_clone.items():
            dict_clone[key] = value.clone()  # value is tensor, let's save it's clone

        return dict_clone


    # Function creates clusters from activations.
    # It assumes that activations recorder layers are located after modifier layers.
    # Modifier layers are those who will be fed with found clusters.They will modify continuous activations
    # into discreete values (clusters centers).
    # These clustered values are required to extract rules (or build decision tree)
    # @ param mlp - seqential container - 'multilayered perceptron'
    # @ param - metric_calc_func - function to estimate metric - lower is better.
    # @ param initialEpsilon - initial minimal distance between activation values of single neuron to be merged
    #                          It should be more than 2 in case tanh is used and we have possible distances between activation values of about 2
    #                          We will be decreasing this value until find appropriate which preserves accuracy.
    @timeit
    def clusterize(self, mlp, metric_calc_func, initialEpsilon, width_p, height_p, minX_p, minY_p, maxX_p, maxY_p, folder_prefix=None):
        print('Clusterize')

        epsilon = initialEpsilon
        epsilonDecreaseStep = 0.8  # in case supplied initialEpsilon is too large it will be decreased using epsilonDecreaseStep. Until accuracy will stay the same.

        # Clusterize
        # Initialize cluster - modifier - functions for ActivationModifier layers
        metric_orig = metric_calc_func(mlp)  # calc metric (should be calculated on training data)

        # NB! We always assume ActivationsRecorder layer is located right after ActivationsModifier layer
        act_mod_layer = None
        for layer in mlp.modules():
            if 'ActivationModifier' in torch.typename(layer):
                act_mod_layer = layer

            if 'ActivationRecorder' in torch.typename(layer):
                recorded_activations = layer.get_activations()
                act_mod_layer.buildAndSetClusterizerModifierFunction(recorded_activations)
                act_mod_layer = None

        iter = 1
        # for all layers starting from last one
        # for i in range(mlp.size(), 1, -1):
        modules_list = []
        for l in mlp.modules():
            modules_list.append(l)

        layer_indx = 1
        for modifierLayer in reversed(modules_list):
            # if (torch.typename(mlp.get(i)) == 'nn.ActivationModifier'):
            if 'ActivationModifier' in torch.typename(modifierLayer):
                # modifierLayer = mlp.get(i)
                if modifierLayer.clustersTableBackup == None:
                    # initially clusters_table_dict contains continuous activation values, they will be merged into clusters(wtih boundaries)
                    # modifierLayer.clustersTableBackup = self.tableUtil.clone(modifierLayer.clusters_table_dict)
                    modifierLayer.clustersTableBackup = self.clone_dict_of_tensors(modifierLayer.clustersTable)  # clustersTable is dict of tensors
                else:
                    modifierLayer.clustersTable = self.tableUtil.clone(modifierLayer.clustersTableBackup)

                clusters_table_dict = modifierLayer.clustersTable
                for neuronIndx, neuronClusters in clusters_table_dict.items():
                    # print('====== neuronIndx=' + str(neuronIndx))
                    neuronClustersBackup = neuronClusters.clone()

                    eps = epsilon
                    # print('1. eps='+ str(eps))
                    continuationFlag = True
                    # 
                    decreasingEpsilon = True
                    while continuationFlag:
                        # print('neuronClusters=>')
                        # print(neuronClusters)
                        mergedClusters = self.activationModifierUtil.clusterize(neuronClusters, eps)
                        # print('mergedClusters=>')
                        # print(mergedClusters)
                        clusters_table_dict[neuronIndx] = mergedClusters

                        metric_new = metric_calc_func(mlp)
                        # if (decreasingEpsilon) then
                        continuationFlag = (metric_new < metric_orig) and (mergedClusters.shape[0] < neuronClustersBackup.shape[0])  # if epsilon is so small that nothing is merged - exit

                        iter = iter + 1
                        # if (decreasingEpsilon and continuationFlag == False):
                        # decreasingEpsilon = False
                        # print('continuationFlag='+str(continuationFlag)+ '; new totalValid='+str(metric_new.totalValid)+'; orig totalValid='+str(metric_orig.totalValid)+'; mergedClusters:size(1)='+str(mergedClusters.size(1))+ '; neuronClustersBackup.size(1)='+str(neuronClustersBackup.size(1))
                        if continuationFlag:  # decrease
                            # if width_p is not None and height_p is not None and minX_p is not None and minY_p is not None:
                            #     log_file_name = "trainedNetwork_discretize_iter" + str(iter) + "_L" + str(layer_indx) + "_n" + str(neuronIndx) + ".txt"
                            #     self.modelPingUtil.pingModelWriteFile(mlp, width_p, height_p, minX_p, minY_p, maxX_p, maxY_p, log_file_name)
                            eps = eps * epsilonDecreaseStep
                            # print('2. decreased eps='..eps)
                            clusters_table_dict[neuronIndx] = neuronClustersBackup

                if width_p is not None and height_p is not None and minX_p is not None and minY_p is not None:
                    log_file_name = "trainedNetwork_discretize_iter" + str(iter) + "_FinishedNeuron_L" + str(layer_indx) + "_n" + str(neuronIndx) + ".txt"
                    if not folder_prefix is None:
                        log_file_name = os.path.join(folder_prefix, log_file_name)
                    # self.modelPingUtil.pingModelWriteFile(mlp, width_p, height_p, minX_p, minY_p, maxX_p, maxY_p, log_file_name)

            layer_indx = layer_indx + 1

        print('Clusterize finished')

    # Function creates clusters from activations.
    # It collects inputs of DynamicOutputQuantizationModule (they will be quantized)
    # These clustered values are required to extract rules (or build decision tree)
    # @ param mlp - seqential container - 'multilayered perceptron'
    # @ param - metric_calc_func - function to estimate metric - lower is better.
    # @ param initialEpsilon - initial minimal distance between activation values of single neuron to be merged
    #                          It should be more than 2 in case tanh is used and we have possible distances between activation values of about 2
    #                          We will be decreasing this value until find appropriate which preserves accuracy.
    @timeit
    def clusterize_new(self, mlp, metric_calc_func, initialEpsilon, data_loader, epsilonDecreaseStep=0.98):
        print('Clusterize (new)')

        # Clusterize
        # Initialize cluster - modifier - functions for ActivationModifier layers
        metric_orig = metric_calc_func(mlp)  # calc metric (should be calculated on training data)

        # Collect all input batches into single X tensor
        # this is not memory friendly, but for now we live with that, as next step will require all inputs.
        X, _ = get_X_y_from_dataloader(_data_loader=data_loader)

        # Initialize quantization layers with inputs (initial clusters tables will be formed from them.
        modules_list = []
        for key, l in mlp.model._modules.items():
            layer_type = torch.typename(l)
            if QuantizationLayer.NAME in layer_type:
                l.build_and_set_quantization_table(X)
            X = l(X)  # Do the forward pass
            modules_list.append(l)

        # Clusterize
        # Initialize cluster - modifier - functions for ActivationModifier layers
        quantized_metric_orig = metric_calc_func(mlp)  # calc metric (should be calculated on training data)

        iter = 1
        layer_indx = 1
        for modifierLayer in reversed(modules_list):
            # if (torch.typename(mlp.get(i)) == 'nn.ActivationModifier'):
            if QuantizationLayer.NAME in torch.typename(modifierLayer):
                # modifierLayer = mlp.get(i)
                if modifierLayer.clusters_table_backup.data.nelement() == 0:
                    # initially clusters_table_dict contains continuous activation values, they will be merged into clusters(wtih boundaries)
                    modifierLayer.clusters_table_backup.data = modifierLayer.clusters_table.data.clone()
                else:
                    modifierLayer.clusters_table.data = modifierLayer.clusters_table_backup.data.clone()

                clusters_table = modifierLayer.clusters_table.data
                neurons_count = clusters_table.shape[0]
                table_len = clusters_table.shape[1]

                longest_clusters_table = 0
                for neuronIndx in range(neurons_count):
                    neuronClusters = clusters_table[neuronIndx]
                # for neuronIndx, neuronClusters in clusters_table.items():
                    # print('====== neuronIndx=' + str(neuronIndx))
                    neuronClustersBackup = neuronClusters.clone()

                    eps = initialEpsilon
                    # print('1. eps='+ str(eps))
                    continuationFlag = True
                    #
                    decreasingEpsilon = True
                    previousMergedClusters = None
                    while continuationFlag:
                        # print('neuronClusters=>')
                        # print(neuronClusters)
                        while True:  # this while allow to progressively lower Eps, skipping metric calculation if clusters are the same as in previous iteration (when metric was already calculated)
                            mergedClusters = self.activationModifierUtil.clusterize(neuronClusters, eps)
                            eps = eps * epsilonDecreaseStep
                            if not (not previousMergedClusters is None and \
                                    previousMergedClusters.shape == mergedClusters.shape and \
                                    torch.all(torch.eq(previousMergedClusters, mergedClusters)).item() == 1):
                                break

                        previousMergedClusters = mergedClusters
                        mergedClusters_orig_len = mergedClusters.shape[0]
                        if mergedClusters_orig_len > longest_clusters_table:
                            longest_clusters_table = mergedClusters_orig_len
                        # print('mergedClusters=>')
                        # print(mergedClusters)
                        null = self.add_rows_to_merged_clusters(table_len, mergedClusters)
                        modifierLayer.clusters_table.data[neuronIndx, :, :] = mergedClusters

                        metric_new = metric_calc_func(mlp)
                        # if (decreasingEpsilon) then
                        continuationFlag = (metric_new < metric_orig) and (mergedClusters_orig_len < neuronClustersBackup.shape[0])  # if epsilon is so small that nothing is merged - exit

                        iter = iter + 1
                        # if (decreasingEpsilon and continuationFlag == False):
                        # decreasingEpsilon = False
                        # print('continuationFlag='+str(continuationFlag)+ '; new totalValid='+str(metric_new.totalValid)+'; orig totalValid='+str(metric_orig.totalValid)+'; mergedClusters:size(1)='+str(mergedClusters.size(1))+ '; neuronClustersBackup.size(1)='+str(neuronClustersBackup.size(1))
                        if continuationFlag:  # decrease
                            # # if width_p is not None and height_p is not None and minX_p is not None and minY_p is not None:
                            # #     log_file_name = "trainedNetwork_discretize_iter" + str(iter) + "_L" + str(layer_indx) + "_n" + str(neuronIndx) + ".txt"
                            # #     self.modelPingUtil.pingModelWriteFile(mlp, width_p, height_p, minX_p, minY_p, maxX_p, maxY_p, log_file_name)
                            # eps = eps * epsilonDecreaseStep
                            # # print('2. decreased eps='..eps)
                            modifierLayer.clusters_table.data[neuronIndx, :, :] = neuronClustersBackup

                    metric_orig = metric_new
                # Now when all current layer neurons are processed - let's see, maybe we can cut out some unneeded rows
                # in clusters tables.
                modifierLayer.clusters_table.data = modifierLayer.clusters_table.data[:, :longest_clusters_table, :]


            layer_indx = layer_indx + 1

        print('Clusterize (new) finished')

    # Function creates clusters from neurons outputs.
    # Over given model it searches for DynamicOutputQuantizationModule layers, then records their's inputs
    # and constructs inputs quantization tables, so that any continuous signal will be transformed (according
    # to quantization tables) into quantized output (picked up from finite set of outpus defined in table).
    #
    # The process is:
    # For each DynamicOutputQuantizationModule layers starting from last one (closest to output):
    #    For all neurons in such layer in random order:
    #       record inputs
    #       round these inputs by using some number of digits after comma so that overall model metric is not worsened.
    #       perform inputs merging into clusters (to update qunatization (clusterization) tables) so that overall model metric is not worsened.
    #
    # @ param mlp - sequential container - there should be at least one DynamicOutputQuantizationModule layer.
    #               otherwise nothing will be done.
    # @ param - metric_calc_func - function to estimate metric - during our process metric should not get worse (should not rise).
    # @ param data_loader - train data loader to record all inputs for qunatization (if dataset does not fit in memory
    #                       this data_loader can load only a subset of the dataset. It is up to a user.
    @timeit
    def clusterize_discretize(self,
                              mlp, metric_calc_func,
                              data_loader,
                              visualizer=None,
                              visualizer_args=None,
                              metric_strategy_f=None,
                              quantization_epsilon=1000000):
        """

        :param mlp: model which should have at least single nnkx.QuantizationLayer, otherwise nothing will be done.
        :param metric_calc_func: function which allow to understand when discretization worsens performance. Higher is better.
        :param data_loader: to load data for discretization (can be subset of train data if data amount is large)
        :param visualizer: function to be called to visualize acquired decision boundary (to visualize discretization process)
        :param visualizer_args: arguments with wich visualizer function will be called
        :param metric_strategy_f: ADiscretizeMetricStrategy
        :return:
        """

        if metric_strategy_f is None:
            metric_strategy_f = DiscretizeMetricStrategyNormalRules()

        print('Clusterize+discretize')

        # record metric initial value
        metric_orig_before_quantization = metric_calc_func(mlp)  # calc metric (should be calculated on training data)
        print('Metric original - before quantization: '+str(metric_orig_before_quantization))

        # record all inputs
        # Collect all input batches into single X tensor
        # this is not memory friendly, but for now we live with that, as next step will require all inputs.
        inputs_list = []
        for batch_idx, (input, target) in enumerate(data_loader):
            inputs_list.append(input)
        X = torch.cat(inputs_list)

        # Initialize quantization layers with inputs (initial clusters tables will be formed from them.


        # Collect quantization layers
        quantization_layers = []
        for key, l in mlp.model._modules.items():
            layer_type = torch.typename(l)
            if QuantizationLayer.NAME in layer_type:
                # this process can degrade metric a bit as rounding is used when quantization table is created.
                l.build_and_set_quantization_table(X, eps=quantization_epsilon)
                quantization_layers.append(l)
            X = l(X)  # Do the forward pass

        if X.shape[1] == 2 and visualizer is not None:
            visualizer(*visualizer_args)

        metric_orig = metric_calc_func(mlp)
        print('Metric original - after quantization: ' + str(metric_orig))

        metric_orig = metric_strategy_f.init_metric_orig(metric_orig)
        # metric_orig = metric_orig * 0.9999  # worsen it a bit

        layer_indx = len(quantization_layers)
        for modifierLayer in reversed(quantization_layers):
            if QuantizationLayer.NAME in torch.typename(modifierLayer):
                # modifierLayer = mlp.get(i)
                if modifierLayer.clusters_table_backup.data.nelement() == 0:
                    # initially clusters_table_dict contains continuous activation values, they will be merged into clusters(wtih boundaries)
                    modifierLayer.clusters_table_backup.data = modifierLayer.clusters_table.data.clone()
                else:
                    modifierLayer.clusters_table.data = modifierLayer.clusters_table_backup.data.clone()

                clusters_table = modifierLayer.clusters_table.data
                neurons_count = clusters_table.shape[0]

                for neuronIndx in range(neurons_count):

                    if X.shape[1] == 2 and visualizer is not None:
                        def visualizer_f(iteration: int):
                            visualizer_args[len(visualizer_args) - 1] = 'ann_quantized_L'+str(layer_indx)+'_Nrn' + str(neuronIndx) + '_Iter'+str(iteration)+"_"
                            visualizer(*visualizer_args)

                        visualizer_2 = visualizer_f
                    else:
                        visualizer_2 = None


                    self.__merge_neuron_clusters(neuronIndx,
                                                 modifierLayer,
                                                 metric_calc_func,
                                                 mlp,
                                                 metric_orig,
                                                 visualizer_2,
                                                 metric_strategy_f)

                    # metric_orig = metric_new

                # Now when all current layer neurons are processed - let's see, maybe we can cut out some unneeded rows
                # in clusters tables.
                # modifierLayer.clusters_table.data = modifierLayer.clusters_table.data[:, :longest_merged_clusters_orig_len, :]

            layer_indx = layer_indx - 1

        print('Clusterize (new) finished')

    def __get_percent(self, clusters_table_length, iter):
        return iter * 100 / clusters_table_length


    def __merge_neuron_clusters(self, neuronIndx, modifierLayer, metric_calc_func, mlp,
                                metric_orig, visualizer=None, metric_strategy_f=None):
        """

        :param neuronIndx:
        :param modifierLayer:
        :param metric_calc_func:
        :param mlp:
        :param metric_orig:
        :param visualizer:
        :param metric_strategy_f: ADiscretizeMetricStrategy subclass
        :return:
        """

        if metric_strategy_f is None:
            metric_strategy_f = DiscretizeMetricStrategyNormalRules()

        clusters_table_length = modifierLayer.clusters_table.data[neuronIndx, :, :].shape[0]
        print('__merge_neuron_clusters(neuronIndx='+str(neuronIndx)+'), clusters_table_length = '+str(clusters_table_length))
        # clusters_table_len = modifierLayer.clusters_table.data.shape[1]
        iter = 0
        continuation_flag = True
        N = 10  # how many close points to merge during one iteration
        skip_bounds = torch.FloatTensor([MAX_FLOAT_BOUND])  # this is a requirement!
        with tqdm(total=100) as pbar:
            while continuation_flag:
                neuron_clusters = modifierLayer.clusters_table.data[neuronIndx, :, :].clone()
                merged_clusters, merged_bound, merged_clusters_count = \
                    self.activationModifierUtil.clusterize_inputs(neuron_clusters,
                                                                  n=N,
                                                                  skip_bounds=skip_bounds)
                if merged_clusters_count == 0:
                    pbar.update(100)
                    break

                modifierLayer.set_merged_clusters_table(neuronIndx, merged_clusters)

                metric_new = metric_calc_func(mlp)

                is_nothing_to_merge = torch.lt(torch.abs(torch.add(merged_clusters[:, 1].min(), -MAX_FLOAT_BOUND)),
                                               1e-12).item() == 1

                if metric_new < metric_orig:  # rollback last merge
                    # save bound which should not be crossed (merged)
                    # skip_bounds = torch.cat((skip_bounds, torch.FloatTensor([merged_bound])))  # we can't save any bad bounds to avoid merging as there were too many of them (N)
                    # restore previous state - before metric got worse
                    modifierLayer.set_merged_clusters_table(neuronIndx, neuron_clusters)

                    print('Metric worsened running 1 cluster merge procedure')

                    # now try to run clusterization N times
                    inner_continuation_flag = True
                    inner_iter = 0
                    while inner_continuation_flag and inner_iter < N:
                        inner_iter = inner_iter + 1

                        neuron_clusters = modifierLayer.clusters_table.data[neuronIndx, :, :].clone()
                        merged_clusters, merged_bounds, merged_clusters_inner = \
                            self.activationModifierUtil.clusterize_inputs(neuron_clusters,
                                                                          n=1,
                                                                          skip_bounds=skip_bounds)
                        if merged_clusters_inner == 0:
                            break

                        modifierLayer.set_merged_clusters_table(neuronIndx, merged_clusters)

                        metric_new = metric_calc_func(mlp)

                        iter = iter + 1

                        is_nothing_to_merge = torch.lt(
                            torch.abs(torch.add(merged_clusters[:, 1].min(), -MAX_FLOAT_BOUND)),
                            1e-12).item() == 1

                        if metric_new < metric_orig:  # rollback last merge
                            # save bound which should not be crossed (merged)
                            skip_bounds = torch.cat((skip_bounds, torch.FloatTensor([merged_bounds[0].item()])))
                            # restore previous state - before metric got worse
                            modifierLayer.set_merged_clusters_table(neuronIndx, neuron_clusters)
                            inner_continuation_flag = False
                        else:
                            if visualizer is not None:
                                visualizer(iter)
                            print('Metric changed (merged 1 cluster) from ' + str(metric_orig) + ' to ' + str(metric_new))
                            metric_orig = metric_strategy_f.update_metric_orig(metric_orig, metric_new)
                            iter = iter + 1
                            pct = self.__get_percent(clusters_table_length, 1)
                            pbar.update(pct)

                        if is_nothing_to_merge:
                            inner_continuation_flag = False

                else:
                    if visualizer is not None:
                        visualizer(iter)
                    print('Metric changed (merged 10? clusters) from '+str(metric_orig) + ' to ' +str(metric_new))
                    metric_orig = metric_strategy_f.update_metric_orig(metric_orig, metric_new)
                    iter = iter + merged_clusters_count
                    pct = self.__get_percent(clusters_table_length, merged_clusters_count)
                    pbar.update(pct)

                if is_nothing_to_merge:
                    pbar.update(100)
                    continuation_flag = False

    # def __add_rows_to_merged_clusters(self, clusters_table_len, merged_clusters):
    #     if merged_clusters.shape[0] < clusters_table_len:
    #         rows_to_add_count = clusters_table_len - merged_clusters.shape[0]
    #         rows_to_add = torch.FloatTensor([MAX_FLOAT_BOUND]).repeat(rows_to_add_count, 2)
    #         merged_clusters = torch.cat([merged_clusters, rows_to_add], dim=0)
    #     return merged_clusters
    #
    # def __update_longest_merged_clusters_orig_len(self, merged_clusters_orig_len, longest_merged_clusters_orig_len):
    #     if merged_clusters_orig_len > longest_merged_clusters_orig_len:
    #         longest_merged_clusters_orig_len = merged_clusters_orig_len
    #     return longest_merged_clusters_orig_len


class NeuronPruningFunction(torch.autograd.Function):

    @staticmethod
    def forward(ctx, input_tensor, pruning_mask, pruning_empty_value):
        """
        Function applies pruning mask to input tensor.
        Where mask is 1 - input tensor will be replaced with pruning_empty_value (this
        value depends on activation function used, i.e. sigm - 0.5, tanh - 0.0, relu - 0.0)

        :param ctx: Function context
        :param input_tensor: torch.Tensor
        :param pruning_mask: torch.ByteTensor
        :param pruning_empty_value: 1D torch.FloatTensor with single value
        :return: returns input_tensor filled with pruning_empty_value where pruning_mask is 1.
        """
        # ctx is a context object that can be used to stash information
        # for backward computation
        ctx.pruning_mask = pruning_mask
        ctx.pruning_empty_value = pruning_empty_value
        empty_value = pruning_empty_value
        return input_tensor.masked_fill(pruning_mask, empty_value)

    @staticmethod
    def backward(ctx, grad_output):
        """
        In contrast to forward here we always replace gradients with zeros where
        pruning_mask is 1.

        :param ctx: Function context
        :param grad_output:
        :return:
        """
        # We return as many input gradients as there were arguments.
        # Gradients of non-Tensor arguments to forward must be None.
        # return grad_output * ctx.constant, None

        return grad_output.masked_fill_(ctx.pruning_mask, 0.0), None, None


class NeuronPruningModule(nn.Module):
    def __init__(self, zeroVal, layerSize):
        nn.Module.__init__(self)
        if zeroVal == None:
            raise Exception('`zeroVal` parameter is None')

        if layerSize == None:
            raise Exception('`layerSize` parameter is None')

        if isinstance(zeroVal, str):
            zeroVal = zeroVal.lower()
            if ((zeroVal == 'hardtanh') or \
                    (zeroVal == 'hardshrink') or \
                    (zeroVal == 'softshrink') or \
                    (zeroVal == 'softmax') or \
                    (zeroVal == 'softmin') or \
                    (zeroVal == 'softplus') or \
                    (zeroVal == 'logsigmoid') or \
                    (zeroVal == 'logsoftmax') or \
                    (zeroVal == 'tanh') or \
                    (zeroVal == 'hardtanh') or \
                    (zeroVal == 'relu') or \
                    (zeroVal == 'hardtanh')):
                pruning_empty_value = 0.0

        elif ((zeroVal == 'softsign') or \
              (zeroVal == 'sigmoid') or \
              (zeroVal == 'prelu') or \
              (zeroVal == 'rrelu') or \
              (zeroVal == 'elu')):

            pruning_empty_value = 0.5

        if pruning_empty_value is None:
            raise Exception('Unknown type of activation function is passed=' + str(zeroVal))

        pruning_mask = torch.zeros(layerSize).byte()
        # requires_grad=False --> we don't want to optimize our parameters
        self.pruning_mask = nn.Parameter(pruning_mask, requires_grad=False)
        self.pruning_empty_value = nn.Parameter(torch.FloatTensor([pruning_empty_value]), requires_grad=False)

    def forward(self, input):
        return NeuronPruningFunction.apply(input, self.pruning_mask, self.pruning_empty_value[0].item())

    def extra_repr(self):
        # (Optional)Set the extra information about this module. You can test
        # it by printing an object of this class.
        return 'pruning_mask={}, pruning_empty_value={}'.format(
            self.pruning_mask, self.pruning_empty_value
        )

# @deprecated('Use NeuronPruningLayerF instead, as this class does not support normal model cloning.')
class NeuronPruningLayer(nn.Module):
    # Class implementing NeuronPruningLayer module.
    # It is basically a layer which nullifies( or set 's to other mean value)
    # outputs from previous layer.
    # -------------------------------------------
    # @ param zeroVal - Value to which we should nullify pruned neurons(1 's in prunning mask)
    #                   It is 0 in case Tanh is used(as Tanh y_pred belongs to[-1, +1]
    #                   and it should be assigned to 0.5 in case
    # @ param           layerSize - Number.Length / size of pruned layer
    #                   (we support only 1D layers - hence it is single number)
    def __init__(self, zeroVal, layerSize):
        nn.Module.__init__(self)
        self.mask_flag = False
        # print('Initializing NeuronPruningLayer zVal = '+str(zeroVal)+ ', size='+str(layerSize))
        if zeroVal == None:
            raise Exception('`zeroVal` parameter is None')

        if layerSize == None:
            raise Exception('`layerSize` parameter is None')

        self.pruningMask = torch.zeros(1, layerSize).byte()
        if isinstance(zeroVal, str):
            zeroVal = zeroVal.lower()
            if ((zeroVal == 'hardtanh') or \
                    (zeroVal == 'hardshrink') or \
                    (zeroVal == 'softshrink') or \
                    (zeroVal == 'softmax') or \
                    (zeroVal == 'softmin') or \
                    (zeroVal == 'softplus') or \
                    (zeroVal == 'logsigmoid') or \
                    (zeroVal == 'logsoftmax') or \
                    (zeroVal == 'tanh') or \
                    (zeroVal == 'hardtanh') or \
                    (zeroVal == 'relu') or \
                    (zeroVal == 'hardtanh')):
                zeroVal = 0

        elif ((zeroVal == 'softsign') or \
              (zeroVal == 'sigmoid') or \
              (zeroVal == 'prelu') or \
              (zeroVal == 'rrelu') or \
              (zeroVal == 'elu')):

            zeroVal = 0.5

        else:
            raise Exception('Unknown type of activation function is passed=' + str(zeroVal))

        self.setZeroVal(zeroVal)

    # Returns True if input
    # -------------------------------------------
    # @ param pruningMask - pruningMask.it is a Tensor, should contain only 0's and 1's. Here 1 means pruned neuron
    def __tensor_is_binary(self, input):
        unique_sum = torch.unique(input).sum().item()
        return unique_sum == 0 or unique_sum == 1

    # Setter for pruning mask
    # --------------------------------------------
    # @ param pruningMask - pruningMask. it is a Tensor, should contain only 0's and 1's. Here 1 means pruned neuron
    def setPruningMask(self, pruningMask):
        assert (pruningMask is not None, '<NeuronPruningLayer> illegal prunningMask, must be not None')
        assert (self.__tensor_is_binary(pruningMask),
                '<NeuronPruningLayer> illegal prunningMask, must contain only 0s and/or 1s')
        assert (pruningMask.type() == torch.ByteTensor().type(), 'pruningMask param should be of type torch.ByteTensor')
        assert (len(pruningMask.size()) == 1, 'We expecting mask to be single row (vector)')
        self.pruningMask = pruningMask.clone().byte()

    # Getter for pruning mask
    # --------------------------------------------
    # @ return clone of pruningMask
    def getPruningMask(self):
        return self.pruningMask.clone()  # return copy

    # Setter for zero val(y_pred value for pruned neurons)
    # --------------------------------------------
    # @ param zeroVal - It is a number which will be setted as y_pred for pruned neurons.
    def setZeroVal(self, zeroVal):
        self.zeroVal = zeroVal

    # Getter for zero value
    # --------------------------------------------
    # @ return zero value
    def getZeroVal(self):
        return self.zeroVal

    # Getter for zero value
    # --------------------------------------------
    # @ return
    def __assertSameSizes(self, list_a, list_b):
        return all(x == y for x, y in zip(list_a, list_b))

        # if ta.dim() != tb.dim():
        #     raise Exception('Tensor1 dim=' + str(ta.dim()) + ' does not equal Tensor2 dim=' + tb.dim())
        # else:
        #     taSize = ta.size()
        #     tbSize = tb.size()
        #     for i in range(1, len(taSize)):
        #         if taSize[i] != tbSize[i]:
        #             raise Exception('ta =' + str(ta) + ' does not equal tb =' + str(tb))
        # return

    def updateOutput(self, input):
        self.output.resizeAs(input).copy(input)
        if input.size().size() == 1:
            self.output.maskedFill(self.pruningMask, self.zeroVal)
        else:  # we are dealing with batches
            pruningMaskWidth = self.pruningMask.size(1)
            assert (input.size(1) == pruningMaskWidth)
            inputBatchSize = input.size(0)
            pruningMaskBatch = self.pruningMask.expand(inputBatchSize, pruningMaskWidth)
            self.__assertSameSizes(input, pruningMaskBatch)
            self.output.maskedFill(pruningMaskBatch, self.zeroVal)

        return self.output

    def updateGradInput(self, input, gradOutput):
        self.gradInput.resizeAs(gradOutput).copy(gradOutput)  # 0 - no error for pruned nodes
        if input.size().size() == 1:
            self.gradInput.maskedFill(self.pruningMask, 0)
        else:  # we are dealing with batches
            pruningMaskWidth = self.pruningMask.size(1)
            assert (gradOutput.size(1) == pruningMaskWidth)
            inputBatchSize = gradOutput.size(0)
            pruningMaskBatch = self.pruningMask.expand(inputBatchSize, pruningMaskWidth)
            self.__assertSameSizes(gradOutput, pruningMaskBatch)
            self.gradInput.maskedFill(pruningMaskBatch, 0)

        return self.gradInput

    def __tostring__(self):
        return torch.typename(self) + '(zeroval=%s, maskSize=(%d,%d))', self.zeroVal, self.pruningMask.size(
            1), self.pruningMask.size(2)

    def forward(self, x):
        if self.mask_flag == True:
            # return x * self.pruningMask
            # out = x.copy().resizeAs(x)
            out = x.clone()
            if len(x.size()) == 1:
                # not tested
                out.maskedFill(self.pruningMask, self.zeroVal)
            else:  # we are dealing with batches
                if len(self.pruningMask.size()) > 1:
                    pruningMaskWidth = self.pruningMask.size(1)
                else:
                    pruningMaskWidth = self.pruningMask.size()[0]
                assert (x.size(1) == pruningMaskWidth)
                inputBatchSize = x.size(0)
                pruningMaskBatch = self.pruningMask.expand(inputBatchSize, pruningMaskWidth)
                self.__assertSameSizes(list(x.size()), list(pruningMaskBatch.size()))
                out.masked_fill_(pruningMaskBatch, self.zeroVal)
            return out
        else:
            return x

def clone_pytorch_model(model1, opt1, model_builder_func, optimizer_builder_func):
    model2 = model_builder_func()  # get new instance
    model2.load_state_dict(model1.state_dict())  # copy state
    opt2 = None
    if opt1 is not None:
        opt2 = optimizer_builder_func(model2)
        # opt2 = torch.optim.Adam(model2.parameters(), lr=0.0001)  # get new optimiser
        opt2.load_state_dict(opt1.state_dict())  # copy state
    return model2, opt2

class NeuronPruning():

    def __init__(self, pruningLogic, dataUtil, runningUtil, modelPingUtil):
        self.modelPingUtil = modelPingUtil
        self.runningUtil = runningUtil
        self.dataUtil = dataUtil
        self.pruningLogic = pruningLogic

    def countPrunedNeurons(self, model):
        if torch.typename(model).lower() != 'nn.sequential':
            raise Exception('Neuron pruning can find pruned neurons count only for Seqential models')

        prunedNeuronsCounts = {}
        totalPrunedNeurons = 0
        for i in range(1, model.size()):
            layer = model.get(i)
            if torch.typename(layer) == 'nn.NeuronPruningLayer':
                pruningMask = layer.getPruningMask()
                prunedNeuronsCounts[i] = pruningMask[pruningMask.eq(1)].numel()
                totalPrunedNeurons = totalPrunedNeurons + prunedNeuronsCounts[i]

        prunedNeurons = {}
        prunedNeurons.total = totalPrunedNeurons
        prunedNeurons.perLayer = prunedNeuronsCounts
        return prunedNeurons

    # It is better for user to call pruneNeurons / unpruneNeurons functions.
    # pruneUnpruneFlag - 0 - means not pruned, 1 - means pruned
    # function NeuronPruning: pruneUnpruneNeurons(sensitivities, mlp, numberOfNeuronsToPrune, pruneUnpruneFlag)
    #    error('pruneUnpruneNeurons is not implemented')
    # 

    # Function for simple neurons pruning based on
    # provided training criterion and dataset.
    # --------------------------------------------
    # @ param sensitivities - Table with rows {layerIndx, neuronIndx, sensitivity}
    # @ param mlp - Layered neural network.Where 'nn.NeuronPruningLayer' layers should exist.
    #               otherwise nothing will happen.
    # @ param     - numberOfNeuronsToPrune - module.forward(input) how many neurons to prune. Default is 1.
    # @ return nothing. mlp will be updated.
    def __pruneNeurons__(self, sensitivities, mlp, numberOfNeuronsToPrune):
        self.pruningLogic.pruneUnpruneNeurons(sensitivities, mlp, numberOfNeuronsToPrune, 1)

    # Function for simple neurons unpruning based on
    # provided training criterion and dataset.
    # --------------------------------------------
    # @ param sensitivities - Table with rows {layerIndx, neuronIndx, sensitivity}
    # @ param mlp - Layered neural network.Where 'nn.NeuronPruningLayer' layers should exist.
    #               otherwise nothing will happen.
    # @ param numberOfNeuronsToPrune - module.forward(input) how many neurons to prune. Default is 1.
    # @ return nothing. mlp will be updated.
    def __unpruneNeurons__(self, sensitivities, mlp, numberOfNeuronsToPrune):
        self.pruningLogic.pruneUnpruneNeurons(sensitivities, mlp, numberOfNeuronsToPrune, 0)

    # Function for running provided neural network
    # with provided training criterion on given dataset.
    # --------------------------------------------
    # @ param mlp - Neural network
    # @ param dataset - Dataset to be used for classification.
    # @ return y - outputs of neural network for provided dataset.
    def __classify__(self, mlp, dataset):
        y = {}
        for i in range(1, len(dataset)):
            example = dataset[i]
            input = example[1]
            y[i] = mlp.forward(input)
        return y

    # Function for calculating accuracy
    # NB!!!! - works with ClassNLLCriterion or CrossEntropyCriterions - due to expected / actual labels calculations
    # --------------------------------------------
    # @ param dataset - Dataset to be used for classification.
    # @ param mlp - Neural network to be used
    # @ param classNames - Just table with one row of strings for class names
    # @ return confusionMatrix, actualLabels table and accuracy
    def __calcConfusionLabelsAccError__(self, dataset, mlp, classNames, criterion, labels2Targets):
        confusionMat = optim.ConfusionMatrix(classNames)  # new matrix
        confusionMat.zero()  # reset matrix
        predictedLabels = {}
        criterionError = 0

        X, y = self.dataUtil.tensorsFromDataset(dataset)
        if y.size(2) == 1:
            y = y.view(-1)

        actualOut = mlp.forward(X)
        criterionError, _ = criterion.forward(actualOut, y)
        _, actualLabels = torch.max(actualOut, 2)
        _, expectedLabels = torch.max(y, 2)
        confusionMat.batchAdd(actualLabels.view(-1), expectedLabels.view(-1))
        confusionMat.updateValids()
        return confusionMat, actualLabels, confusionMat.totalValid, criterionError

    def __countTotalPrunableNeurons__(self, mlp):
        totalPrunableNeurons = 0
        for i in range(1, mlp.size()):
            layer = mlp.get(i)
            if torch.typename(layer) == 'nn.NeuronPruningLayer':
                pruningMask = layer.getPruningMask()
                totalPrunableNeurons = totalPrunableNeurons + pruningMask.numel()

        return totalPrunableNeurons

    def runNeuronsPruning(self, config, dataset, mlp, criterion, optim, model_builder_func, opt, width_p, height_p, minX_p, minY_p, maxX_p, maxY_p):
        totalPrunableNeurons = self.__countTotalPrunableNeurons__(mlp)
        afterPruningRetrainEpochs = config.afterPruningRetrainEpochs
        #  retrainIterations = config.retrainIterations
        maxFallbacks = config.maxFallbacks
        errorWorsenForFallback = config.errorWorsenForFallback
        numberOfNeuronsToPrune = config.numberOfNeuronsToPrune
        maxNodesToPrune = config.maxNodesToPrune
        maxPruningIterations = config.maxPruningIterations
        classNames = config.classNames

        originalMaxIteration = opt.maxEpoch
        # opt.maxEpoch = config.retrainIterations

        # _, _, highestAccuracy, minCriterionError = self.__calcConfusionLabelsAccError__(dataset, mlp, classNames, criterion, labels2Targets)
        # highestAccuracy = self.runningUtil.test(mlp, dataset, classNames, opt.batchSize).totalValid
        highestAccuracy = self.runningUtil.test(mlp, dataset, criterion, opt).totalValid
        mlpBackup = None  # this backup model
        fallbackCounter = 0  # how many times in a row we pruned neuron & then reverted it back due to bad accuracy
        _iter = 1
        verbose = config.verbose  # can be 0, 1, 2

        pruningLog = {}
        pruningLog.confusionMat = {}
        pruningLog.accuracy = {}
        pruningLog.criterionError = {}
        pruningLog.prunedNeurons = {}
        pruningLog.confusionMatRestored = {}
        pruningLog.accuracyRestored = {}
        pruningLog.criterionErrorRestored = {}
        while maxNodesToPrune > self.countPrunedNeurons(mlp).total and _iter <= maxPruningIterations:

            #  backup network state
            if fallbackCounter == 0:
                mlpBackup = clone_pytorch_model(mlp, optim, model_builder_func)

            # calculate sensitivities
            sensitivities = self.pruningLogic.calcNeuronsSensitivities(mlp, criterion, dataset)
            if len(sensitivities) == 0:
                print('Nothing to prune!')
                break

            # if width_p is not None and height_p is not None and minX_p is not None and minY_p is not None:
            #     self.modelPingUtil.pingModelWriteFile(mlp, width_p, height_p, minX_p, minY_p, maxX_p, maxY_p,
            #                                           "trainedNetwork_beforePruning_" + str(_iter) + ".txt")

            # prune neuron(-s) based on sensitivities
            self.__pruneNeurons__(self, sensitivities, mlp, numberOfNeuronsToPrune)

            # if width_p is not None and height_p is not None and minX_p is not None and minY_p is not None:
            #     self.modelPingUtil.pingModelWriteFile(mlp, width_p, height_p, minX_p, minY_p, maxX_p, maxY_p,
            #                                           "trainedNetwork_afterPruning_" + str(_iter) + ".txt")

            # retrain after pruning
            opt.maxEpochs = afterPruningRetrainEpochs
            parameters, gradParameters = mlp.getParameters()
            confusion = optim.ConfusionMatrix(classNames)
            #  train(mlp, dataset, parameters, gradParameters, confusion, opt)
            confusionMat = self.runningUtil.train(mlp, criterion, dataset, classNames, opt)
            accuracyAfterPruning = confusionMat.totalValid
            # collect metrics & print them
            # confusionMat, _, accuracyAfterPruning, criterionErrorAfterPruning = calcConfusionLabelsAccError(dataset, mlp, classNames, criterion, labels2Targets)

            prunedNeuronsStats = self.countPrunedNeurons(mlp)
            if verbose >= 2:
                print(confusionMat)
                print('TotalPrunableNeurons=' + str(totalPrunableNeurons) + ' PrunedNeuronsStats=' + str(
                    prunedNeuronsStats) + ' accuracy=' + str(accuracyAfterPruning))
            if verbose >= 1:
                print('Accuracy beforePruning: ' + str(highestAccuracy) + ' || after prunning: ' + str(
                    accuracyAfterPruning))
                print('Pruning iteration/totalPrunableNeurons/total removed nodes: ' + str(_iter) + ' / ' + str(
                    totalPrunableNeurons) + ' / ' + str(prunedNeuronsStats))

            # gather some data for history
            pruningLog.confusionMat[_iter] = confusionMat
            pruningLog.accuracy[_iter] = accuracyAfterPruning
            #  pruningLog.criterionError[iter] = criterionError
            pruningLog.prunedNeurons[_iter] = prunedNeuronsStats
            pruningLog.confusionMatRestored[_iter] = None
            pruningLog.accuracyRestored[_iter] = None
            pruningLog.criterionErrorRestored[_iter] = None

            # revert pruned neuron if performance becomes too bad
            if accuracyAfterPruning != highestAccuracy and accuracyAfterPruning / highestAccuracy < 1 - errorWorsenForFallback:  # we allow some degradadion in accuracy / error
                print('Error becomes larger, reverting last removed neuron(-s) ...')
                self.__unpruneNeurons__(self, sensitivities, mlp, numberOfNeuronsToPrune)
                fallbackCounter = fallbackCounter + 1
            elif len(sensitivities == 0):
                print('Nothing to prune! Exiting.')
                break
            else:
                fallbackCounter = 0

            # if too many unsuccessfull attempts were made to prune, restore last known good network state and exit.
            if fallbackCounter >= maxFallbacks:
                print('Tried to prune without luck too many times. Restoring last good known network state...')
                # mlp = mlpBackup.clone()
                mlp = mlpBackup  #clone_pytorch_model(mlp, optim, model_builder_func)
                # confusionMat, _, accuracyAfterPruning, criterionErrorAfterPruning = calcConfusionLabelsAccError(dataset, mlp, classNames, criterion, labels2Targets)
                accuracyAfterPruning = self.runningUtil.test(mlp, dataset, classNames, opt.batchSize).totalValid
                if verbose >= 1:
                    prunedNeuronsStats = self.countPrunedNeurons(mlp)
                    print('Restored model Accuracy : ' + str(accuracyAfterPruning) + ' prunedNeurons=' + str(
                        prunedNeuronsStats))
                # gather some data for history
                pruningLog.confusionMatRestored[_iter] = confusionMat
                pruningLog.accuracyRestored[_iter] = accuracyAfterPruning
                # pruningLog.criterionErrorRestored[iter] = criterionErrorAfterPruning
                break

            # update best known error / accuracy
            # if minCriterionError > criterionErrorAfterPruning:
            # minCriterionError = criterionErrorAfterPruning
            if highestAccuracy < accuracyAfterPruning:
                highestAccuracy = accuracyAfterPruning;

        print('Iteration ' + str(_iter) + ' finished.')
        _iter = _iter + 1

        return pruningLog, mlp


class SensitivityPruningLogic:
    def __init__(self, dataUtil):
        self.dataUtil = dataUtil

    """
    -- Function is small wrapper to calculate error on given data using provided criterion 
    --------------------------------------------
    -- @param mlp         - Layered neural network.
    -- @param criterion   - Criterion used to train network. Required to test network to find errors - sensititivites.
    -- @param dataset     - Neural Network input data set. Should have rows consisting of two parts input and output.
    -- @return error - overall error over give dataset   
    """
    def calc_loss(self, mlp, criterion, dataset):
        # X, y = self.dataUtil.tensorsFromDataset(dataset)
        mlp.train(False)
        total_loss = 0
        for batch_idx, (input, target) in enumerate(dataset):

            # # Convert torch tensor to Variable
            input = Variable(input)
            target = Variable(target)

            outputs = mlp(input)  # Do the forward pass

            _target = target.data
            if len(target.shape) > 1:  # if batch_size == 1
                _target = target.data.squeeze(-1)
            loss = criterion(outputs, _target)  # Calculate the loss
            total_loss = total_loss + loss

        return total_loss
        # actualOut = mlp.forward(X)
        # criterionError, _ = criterion.forward(actualOut, y.view(-1))
        # return criterionError

    """
    -- It is better for user to call pruneNeurons / unpruneNeurons functions.
    -- pruneUnpruneFlag - 0 - means not prune, 1 - means prune
    """
    def pruneUnpruneNeurons(self, sensitivities, mlp, numberOfNeuronsToPrune, pruneUnpruneFlag):
        # table.sort(sensitivities, function(a,b) return a.sensitivity < b.sensitivity end)
        for i in range(1, numberOfNeuronsToPrune+1):
            if (i <= len(sensitivities)):
                sensitivity = sensitivities[i]
                layerIndex = sensitivity['layerIndx']
                neuronIndex = sensitivity['neuronIndx']

                layer = mlp.model[layerIndex]
                pruningMask = layer.getPruningMask()

                flatMask = pruningMask.clone().resize(pruningMask.numel())
                flatMask[neuronIndex] = pruneUnpruneFlag
                newMask = flatMask  #.resize(pruningMask.size())
                layer.setPruningMask(newMask)
            else:
                break

    def pruneUnpruneNeuronsInModules(self, sensitivities, mlp, numberOfNeuronsToPrune, pruneUnpruneFlag):
        # table.sort(sensitivities, function(a,b) return a.sensitivity < b.sensitivity end)
        sensitivities_list = []
        for key, value in sensitivities.items():
            temp = [key, value]
            sensitivities_list.append(temp)

        sensitivities_list.sort(key=lambda kv: kv[1]['sensitivity'].item())

        for i in range(1, numberOfNeuronsToPrune+1):
            if (i <= len(sensitivities_list)):
                sensitivity = sensitivities_list[i][1]
                layerIndex = sensitivity['layerIndx']
                neuronIndex = sensitivity['neuronIndx']

                layer = mlp.model[layerIndex]
                pruningMask = layer.pruning_mask.data

                flatMask = pruningMask.clone().resize(pruningMask.numel())
                flatMask[neuronIndex] = pruneUnpruneFlag
                newMask = flatMask  #.resize(pruningMask.size())
                layer.pruning_mask.data = newMask
            else:
                break

    """
    -- Function for calculating sensitivities for given layered network using 
    -- provided training criterion and dataset. 
    --------------------------------------------
    -- @param mlp         - Layered neural network.
    -- @param criterion   - Criterion used to train network. Required to test network to find errors - sensititivites.
    -- @param dataset     - Neural Network input data set. Should have rows consisting of two parts input and output.
    -- @return sensitivities - table with rows {layerIndx, neuronIndx, sensitivity}   
    """
    def calcNeuronsSensitivities(self, mlp, criterion, dataset):
        sensitivities = {}
        sIndex = 1
        i = 0
        for layer in mlp.model:
            if isinstance(layer, NeuronPruningLayer):
                pruningMask = layer.getPruningMask().byte()
                # do not prune this layer - only one neuron has left
                if (pruningMask[pruningMask.eq(0)].numel() != 1):
                    # let's find sensitivities for each neuron in this layer
                    for n in range(0, pruningMask.numel()):
                        flatMask = pruningMask.clone().resize(pruningMask.numel())
                        if (flatMask[n] == 0):
                            flatMask[n] = 1
                            # newMask = flatMask.resize(pruningMask.size()).byte()
                            if (len(pruningMask.size()) > 1):
                                newMask = flatMask.resize_((pruningMask.size()[0], pruningMask.size()[1])).byte()
                            else:
                                newMask = flatMask.resize_((1, pruningMask.size()[0])).byte()

                            layer.setPruningMask(newMask)

                            currentError = self.calc_loss(mlp, criterion, dataset)
                            # save stats
                            sensitivities[sIndex] = {'layerIndx': i, 'neuronIndx': n, 'sensitivity': currentError}
                            sIndex = sIndex + 1
                            # restore pruning layer state
                            layer.setPruningMask(pruningMask)
            i = i + 1

        return sensitivities

    """
    -- Function for calculating sensitivities for given layered network using 
    -- provided training criterion and dataset. 
    --------------------------------------------
    -- @param mlp         - Layered neural network.
    -- @param criterion   - Criterion used to train network. Required to test network to find errors - sensititivites.
    -- @param dataset     - Neural Network input data set. Should have rows consisting of two parts input and output.
    -- @return sensitivities - table with rows {layerIndx, neuronIndx, sensitivity}   
    """

    def calcNeuronsSensitivitiesInModules(self, mlp, criterion, dataset):
        sensitivities = {}
        sIndex = 1
        i = 0
        for layer in mlp.model:
            if isinstance(layer, NeuronPruningModule):
                pruningMask = layer.pruning_mask.data.byte()
                # do not prune this layer - only one neuron has left
                if (pruningMask[pruningMask.eq(0)].numel() != 1):
                    # let's find sensitivities for each neuron in this layer
                    for n in range(0, pruningMask.numel()):
                        flatMask = pruningMask.clone().resize(pruningMask.numel())
                        if (flatMask[n] == 0):
                            flatMask[n] = 1
                            # newMask = flatMask.resize(pruningMask.size()).byte()
                            if (len(pruningMask.size()) > 1):
                                newMask = flatMask.resize_((pruningMask.size()[0], pruningMask.size()[1])).byte()
                            else:
                                newMask = flatMask.resize_((1, pruningMask.size()[0])).byte()

                            newMask.requires_grad = False
                            layer.pruning_mask.data = newMask

                            current_loss = self.calc_loss(mlp, criterion, dataset)
                            # save stats
                            sensitivities[sIndex] = {'layerIndx': i, 'neuronIndx': n, 'sensitivity': current_loss}
                            sIndex = sIndex + 1
                            # restore pruning layer state
                            layer.pruning_mask.data = pruningMask
            i = i + 1

        return sensitivities


class NeuronPruning:
    def __init__(self, pruningLogic, dataUtil, runningUtil):
        self.pruningLogic = pruningLogic
        self.dataUtil = dataUtil
        self.runningUtil = runningUtil

    def countPrunedNeurons(self, model):
        if 'sequential' not in torch.typename(model.model).lower():
            print('Current model typename is: ' + torch.typename(model.model).lower())
            raise Exception('Neuron pruning can find pruned neurons count only for Seqential models')

        prunedNeuronsCounts = {}
        totalPrunedNeurons = 0

        i = 1
        for m in model.model:
            if isinstance(m, NeuronPruningLayer):
                pruningMask = m.getPruningMask()
                prunedNeuronsCounts[i] = pruningMask[pruningMask.eq(1)].numel()
                totalPrunedNeurons = totalPrunedNeurons + prunedNeuronsCounts[i]
            i = i + 1

        return {'total': totalPrunedNeurons, 'perLayer': prunedNeuronsCounts}


    def countPrunedNeuronsInModules(self, model):
        if 'sequential' not in torch.typename(model.model).lower():
            print('Current model typename is: ' + torch.typename(model.model).lower())
            raise Exception('Neuron pruning can find pruned neurons count only for Seqential models')

        prunedNeuronsCounts = {}
        totalPrunedNeurons = 0

        i = 1
        for m in model.model:
            if isinstance(m, NeuronPruningModule):
                pruningMask = m.pruning_mask.data
                prunedNeuronsCounts[i] = pruningMask[pruningMask.eq(1)].numel()
                totalPrunedNeurons = totalPrunedNeurons + prunedNeuronsCounts[i]
            i = i + 1

        return {'total': totalPrunedNeurons, 'perLayer': prunedNeuronsCounts}

    """
    -- It is better for user to call pruneNeurons / unpruneNeurons functions.
    -- pruneUnpruneFlag - 0 - means not prune, 1 - means prune
    --function NeuronPruning:pruneUnpruneNeurons(sensitivities, mlp, numberOfNeuronsToPrune, pruneUnpruneFlag)
    --    error('pruneUnpruneNeurons is not implemented')
    --end
    
    -- Function for simple neurons pruning based on  
    -- provided training criterion and dataset. 
    --------------------------------------------
    -- @param sensitivities-Table with rows {layerIndx, neuronIndx, sensitivity}
    -- @param mlp         - Layered neural network. Where 'nn.NeuronPruningLayer' layers should exist. 
    --                      Otherwise nothing will happen.
    -- @param numberOfNeuronsToPrune -module:forward(input) how many neurons to prune. Default is 1.
    -- @return nothing. mlp will be updated. 
    """
    def pruneNeurons(self, sensitivities, mlp, numberOfNeuronsToPrune):
        self.pruningLogic.pruneUnpruneNeurons(sensitivities, mlp, numberOfNeuronsToPrune, 1)

    def pruneNeuronsInModules(self, sensitivities, mlp, numberOfNeuronsToPrune):
        self.pruningLogic.pruneUnpruneNeuronsInModules(sensitivities, mlp, numberOfNeuronsToPrune, 1)

    """
    -- Function for simple neurons unpruning based on  
    -- provided training criterion and dataset. 
    --------------------------------------------
    -- @param sensitivities-Table with rows {layerIndx, neuronIndx, sensitivity}
    -- @param mlp         - Layered neural network. Where 'nn.NeuronPruningLayer' layers should exist. 
    --                      Otherwise nothing will happen.
    -- @param numberOfNeuronsToPrune -module:forward(input) how many neurons to prune. Default is 1.
    -- @return nothing. mlp will be updated.  
    """
    def unpruneNeurons(self, sensitivities, mlp, numberOfNeuronsToPrune):
        self.pruningLogic.pruneUnpruneNeurons(sensitivities, mlp, numberOfNeuronsToPrune, 0)

    def unpruneNeuronsInModules(self, sensitivities, mlp, numberOfNeuronsToPrune):
        self.pruningLogic.pruneUnpruneNeuronsInModules(sensitivities, mlp, numberOfNeuronsToPrune, 0)


    """
    -- Function for running provided neural network  
    -- with provided training criterion on given dataset. 
    --------------------------------------------
    -- @param mlp         - Neural network
    -- @param dataset     - Dataset to be used for classification.
    -- @return y - outputs of neural network for provided dataset.  
    """
    def classify(self, mlp, dataset):
        y = {}
        for i in range(0, len(dataset)):
            example = dataset[i]
            input = example[1]
            y[i] = mlp.forward(input)
        return y

    """
    -- Function for calculating accuracy   
    -- NB!!!! - works with ClassNLLCriterion or CrossEntropyCriterions - due to expected/actual labels calculations
    --------------------------------------------
    -- @param dataset     - Dataset to be used for classification.
    -- @param mlp         - Neural network to be used
    -- @param classNames  - Just table with one row of strings for class names
    -- @return confusionMatrix, actualLabels table and accuracy 
    """
    def calcConfusionLabelsAccError(self, dataset, mlp, classNames, criterion, labels2Targets):
        confusionMat = optim.ConfusionMatrix(classNames)      # new matrix
        confusionMat.zero()                                   # reset matrix
        predictedLabels = {}
        criterionError = 0

        X, y = self.dataUtil.tensorsFromDataset(dataset)
        if (y.size(2) == 1):
            y = y.view(-1)

        actualOut = mlp.forward(X)
        criterionError, _ = criterion.forward(actualOut, y)
        _, actualLabels = torch.max(actualOut, 2)
        _, expectedLabels = torch.max(y, 2)
        confusionMat.batchAdd(actualLabels.view(-1), expectedLabels.view(-1))
        confusionMat.updateValids()
        return confusionMat, actualLabels, confusionMat.totalValid, criterionError

    def countTotalPrunableNeurons(self, model):
        totalPrunableNeurons = 0
        for layer in model.modules():
            if 'NeuronPruningLayer' in torch.typename(layer):
                pruningMask = layer.getPruningMask()
                totalPrunableNeurons = totalPrunableNeurons + pruningMask.numel()

        return totalPrunableNeurons

    def countTotalPrunableNeuronsInModules(self, model):
        totalPrunableNeurons = 0
        for layer in model.modules():
            if 'NeuronPruningModule' in torch.typename(layer):
                totalPrunableNeurons = totalPrunableNeurons + layer.pruning_mask.data.numel()

        return totalPrunableNeurons

    def prune_neurons(self, config, dataset, mlp, criterion, opt, optimizer_builder_func):
        totalPrunableNeurons = self.countTotalPrunableNeurons(mlp)
        afterPruningRetrainEpochs = config.afterPruningRetrainEpochs
        #retrainIterations = config.retrainIterations
        maxFallbacks = config.maxFallbacks
        errorWorsenForFallback = config.errorWorsenForFallback
        numberOfNeuronsToPrune = config.numberOfNeuronsToPrune
        maxNodesToPrune = config.maxNodesToPrune
        maxPruningIterations = config.maxPruningIterations
        classNames = config.classNames

        originalMaxIteration = opt.epochs  #maxEpoch
        #opt.maxEpoch = config.retrainIterations

        #_, _, highestAccuracy, minCriterionError = calcConfusionLabelsAccError(dataset, mlp, classNames, criterion, labels2Targets)
        # highestAccuracy = self.runningUtil.test(mlp, dataset, classNames, opt.batchSize).totalValid
        highestAccuracy = self.runningUtil.test(mlp, dataset, criterion, opt)  #.totalValid
        mlpBackup = None      # this is model version to store
        fallbackCounter = 0 # how many times in a row we pruned neuron & then reverted it back due to bad accuracy
        iter = 1
        verbose = config.verbose # can be 0, 1, 2
        pruningLog = {
            'confusionMat': [], \
            'accuracy': [], \
            'criterionError': [], \
            'prunedNeurons': [], \
            'confusionMatRestored': [], \
            'accuracyRestored': [], \
            'criterionErrorRestored': [] \
            }
        while (maxNodesToPrune > self.countPrunedNeurons(mlp)['total'] and iter <= maxPruningIterations):

            # backup network state
            if (fallbackCounter == 0):
                acc = self.runningUtil.test(mlp, dataset, criterion, opt)
                print('backing up model, it`s accuracy is='+str(acc))
                mlpBackup = mlp.clone()

            # calculate sensitivities
            sensitivities = self.pruningLogic.calcNeuronsSensitivities(mlp, criterion, dataset)
            if (len(sensitivities) == 0):
                print('Nothing to prune!')
                break

            #modelPingUtil.pingModelWriteFile(mlp, width_p, height_p, minX_p, minY_p, maxX_p, maxY_p, "trainedNetwork_beforePruning_"..tostring(iter)..".txt")

            # prune neuron(-s) based on sensitivities
            self.pruneNeurons(sensitivities, mlp, numberOfNeuronsToPrune)

            #modelPingUtil.pingModelWriteFile(mlp, width_p, height_p, minX_p, minY_p, maxX_p, maxY_p, "trainedNetwork_afterPruning_"..tostring(iter)..".txt")

            # retrain after pruning
            # opt.maxEpochs = afterPruningRetrainEpochs
            opt.epochs = afterPruningRetrainEpochs
            # # parameters, gradParameters = mlp.getParameters()
            # # confusion = optim.ConfusionMatrix(classNames)
            # #train(mlp, dataset, parameters, gradParameters, confusion, opt)
            # # confusionMat = self.runningUtil.train(mlp, criterion, dataset, classNames, opt)
            # optimizer = torch.optim.Adamax(mlp.parameters(), betas=(0.9, 0.999), lr=1e-4, weight_decay=1e-4)
            # optimizer = optimizer_builder_func(mlp)
            loss_history, accuracy_history = self.runningUtil.train(mlp, criterion, optimizer_builder_func, dataset, opt, epochs_to_print=100)

            # accuracyAfterPruning = confusionMat.totalValid
            accuracyAfterPruning = accuracy_history[len(accuracy_history) - 1]

            # backup network state - after it was pruned
            accOfBackedModel = self.runningUtil.test(mlpBackup, dataset, criterion, opt)
            if (fallbackCounter == 0) and accuracyAfterPruning > accOfBackedModel:
                print('backing up model, it`s accuracy is=' + str(accOfBackedModel))
                mlpBackup = mlp.clone()

            # collect metrics & print them
            #local confusionMat, _, accuracyAfterPruning, criterionErrorAfterPruning = calcConfusionLabelsAccError(dataset, mlp, classNames, criterion, labels2Targets)
            prunedNeuronsStats = self.countPrunedNeurons(mlp)
            if verbose >= 2:
                # print(confusionMat)
                print(accuracyAfterPruning)
                print('TotalPrunableNeurons=' +str(totalPrunableNeurons)+ ' PrunedNeuronsStats='+pp.pformat(prunedNeuronsStats) + ' accuracy='+str(accuracyAfterPruning))

            if verbose >= 1:
                print('Accuracy beforePruning: '+str(highestAccuracy)+' || after prunning: ' + str(accuracyAfterPruning))
                print('Pruning iteration/totalPrunableNeurons/total removed nodes: '+str(iter)+' / '+str(totalPrunableNeurons)+ ' / '+pp.pformat(prunedNeuronsStats))

            # gather some data for history
            # pruningLog.get('confusionMat').append(confusionMat)
            pruningLog.get('accuracy').append(accuracyAfterPruning)
            # pruningLog.get('criterionError').append(criterionError)
            pruningLog.get('prunedNeurons').append(prunedNeuronsStats)
            # pruningLog.get('confusionMatRestored[iter]').append(None)
            # pruningLog.get('accuracyRestored[iter]').append(None)
            # pruningLog.get('criterionErrorRestored').append(None)

            # revert pruned neuron if performance becomes too bad
            if (accuracyAfterPruning != highestAccuracy and accuracyAfterPruning/highestAccuracy < 1 - errorWorsenForFallback):  # we allow some degradadion in accuracy/error
                print('Error becomes larger, reverting last removed neuron(-s) ...')
                self.unpruneNeurons(sensitivities, mlp, numberOfNeuronsToPrune)
                fallbackCounter = fallbackCounter + 1
            elif len(sensitivities) == 0:
                print('Nothing to prune! Exiting.')
                break
            else:
                fallbackCounter = 0

            # if too many unsuccessfull attempts were made to prune, restore last known good network state and exit.
            if fallbackCounter >= maxFallbacks:
                print('Tried to prune without luck too many times. Restoring last good known network state...')
                mlp = mlpBackup.clone()
                # local confusionMat, _, accuracyAfterPruning, criterionErrorAfterPruning = calcConfusionLabelsAccError(dataset, mlp, classNames, criterion, labels2Targets)
                accuracyAfterPruning = self.runningUtil.test(mlp, dataset, criterion, opt)
                if verbose >= 1:
                    prunedNeuronsStats = self.countPrunedNeurons(mlp)
                    print('Restored net Accuracy : ' +str(accuracyAfterPruning) +' prunedNeurons='+pp.pformat(prunedNeuronsStats))
                # gather some data for history
                # # pruningLog.confusionMatRestored[iter] = confusionMat
                # pruningLog.accuracyRestored[iter] = accuracyAfterPruning
                # #pruningLog.criterionErrorRestored[iter] = criterionErrorAfterPruning
                break

            #    # update best known error/accuracy
            #    if (minCriterionError > criterionErrorAfterPruning) then
            #        minCriterionError = criterionErrorAfterPruning;
            #    end
            if (highestAccuracy < accuracyAfterPruning):
                highestAccuracy = accuracyAfterPruning

            print('Iteration '+str(iter)+' finished.')
            iter = iter + 1

        return pruningLog, mlp

    def prune_module_neurons(self, config, dataset, mlp, criterion, opt, optimizer_builder_func):
        totalPrunableNeurons = self.countTotalPrunableNeuronsInModules(mlp)
        afterPruningRetrainEpochs = config.afterPruningRetrainEpochs
        #retrainIterations = config.retrainIterations
        maxFallbacks = config.maxFallbacks
        errorWorsenForFallback = config.errorWorsenForFallback
        numberOfNeuronsToPrune = config.numberOfNeuronsToPrune
        maxNodesToPrune = config.maxNodesToPrune
        maxPruningIterations = config.maxPruningIterations
        classNames = config.classNames

        originalMaxIteration = opt.epochs  #maxEpoch
        #opt.maxEpoch = config.retrainIterations

        #_, _, highestAccuracy, minCriterionError = calcConfusionLabelsAccError(dataset, mlp, classNames, criterion, labels2Targets)
        # highestAccuracy = self.runningUtil.test(mlp, dataset, classNames, opt.batchSize).totalValid
        highestAccuracy = self.runningUtil.test(mlp, dataset, criterion, opt)  #.totalValid
        mlpBackup = None      # this is model version to store
        fallbackCounter = 0 # how many times in a row we pruned neuron & then reverted it back due to bad accuracy
        iter = 1
        verbose = config.verbose # can be 0, 1, 2
        pruningLog = {
            'confusionMat': [], \
            'accuracy': [], \
            'criterionError': [], \
            'prunedNeurons': [], \
            'confusionMatRestored': [], \
            'accuracyRestored': [], \
            'criterionErrorRestored': [] \
            }
        while (maxNodesToPrune > self.countPrunedNeuronsInModules(mlp)['total'] and iter <= maxPruningIterations):

            # backup network state
            if (fallbackCounter == 0):
                print('Prunning iteration starting, measuring model performance...')
                acc = self.runningUtil.test(mlp, dataset, criterion, opt)
                print('backing up model, it`s accuracy is='+str(acc))
                mlpBackup = mlp.clone()

            # calculate sensitivities
            sensitivities = self.pruningLogic.calcNeuronsSensitivitiesInModules(mlp, criterion, dataset)
            if (len(sensitivities) == 0):
                print('Nothing to prune!')
                break

            #modelPingUtil.pingModelWriteFile(mlp, width_p, height_p, minX_p, minY_p, maxX_p, maxY_p, "trainedNetwork_beforePruning_"..tostring(iter)..".txt")

            # prune neuron(-s) based on sensitivities
            self.pruneNeuronsInModules(sensitivities, mlp, numberOfNeuronsToPrune)
            print('Neurons pruned, measuring model performance...')
            afterPruneBeforeTrainAccuracy = self.runningUtil.test(mlp, dataset, criterion, opt)
            #modelPingUtil.pingModelWriteFile(mlp, width_p, height_p, minX_p, minY_p, maxX_p, maxY_p, "trainedNetwork_afterPruning_"..tostring(iter)..".txt")

            # retrain after pruning
            # opt.maxEpochs = afterPruningRetrainEpochs
            opt.epochs = afterPruningRetrainEpochs
            # # parameters, gradParameters = mlp.getParameters()
            # # confusion = optim.ConfusionMatrix(classNames)
            # #train(mlp, dataset, parameters, gradParameters, confusion, opt)
            # # confusionMat = self.runningUtil.train(mlp, criterion, dataset, classNames, opt)
            # optimizer = torch.optim.Adamax(mlp.parameters(), betas=(0.9, 0.999), lr=1e-4, weight_decay=1e-4)
            # optimizer = optimizer_builder_func(mlp)
            loss_history, accuracy_history = self.runningUtil.train(mlp, criterion, optimizer_builder_func, dataset, opt, epochs_to_print=100)

            # accuracyAfterPruning = confusionMat.totalValid
            accuracyAfterPruning = accuracy_history[len(accuracy_history) - 1]

            # backup network state - after it was pruned
            accOfBackedModel = self.runningUtil.test(mlpBackup, dataset, criterion, opt)
            if (fallbackCounter == 0) and accuracyAfterPruning > accOfBackedModel:
                print('backing up model, it`s accuracy is=' + str(accOfBackedModel))
                mlpBackup = mlp.clone()

            # collect metrics & print them
            #local confusionMat, _, accuracyAfterPruning, criterionErrorAfterPruning = calcConfusionLabelsAccError(dataset, mlp, classNames, criterion, labels2Targets)
            prunedNeuronsStats = self.countPrunedNeuronsInModules(mlp)
            if verbose >= 2:
                # print(confusionMat)
                print(accuracyAfterPruning)
                print('TotalPrunableNeurons=' +str(totalPrunableNeurons)+ ' PrunedNeuronsStats='+pp.pformat(prunedNeuronsStats) + ' accuracy='+str(accuracyAfterPruning))

            if verbose >= 1:
                print('Accuracy beforePruning: '+str(highestAccuracy)+' || after prunning: ' + str(accuracyAfterPruning))
                print('Pruning iteration/totalPrunableNeurons/total removed nodes: '+str(iter)+' / '+str(totalPrunableNeurons)+ ' / '+pp.pformat(prunedNeuronsStats))

            # gather some data for history
            # pruningLog.get('confusionMat').append(confusionMat)
            pruningLog.get('accuracy').append(accuracyAfterPruning)
            # pruningLog.get('criterionError').append(criterionError)
            pruningLog.get('prunedNeurons').append(prunedNeuronsStats)
            # pruningLog.get('confusionMatRestored[iter]').append(None)
            # pruningLog.get('accuracyRestored[iter]').append(None)
            # pruningLog.get('criterionErrorRestored').append(None)

            # revert pruned neuron if performance becomes too bad
            if (accuracyAfterPruning != highestAccuracy and accuracyAfterPruning/highestAccuracy < 1 - errorWorsenForFallback):  # we allow some degradadion in accuracy/error
                print('Error becomes larger, reverting last removed neuron(-s) ...')
                self.unpruneNeuronsInModules(sensitivities, mlp, numberOfNeuronsToPrune)
                fallbackCounter = fallbackCounter + 1
            elif len(sensitivities) == 0:
                print('Nothing to prune! Exiting.')
                break
            else:
                fallbackCounter = 0

            # if too many unsuccessfull attempts were made to prune, restore last known good network state and exit.
            if fallbackCounter >= maxFallbacks:
                print('Tried to prune without luck too many times. Restoring last good known network state...')
                mlp = mlpBackup.clone()
                # local confusionMat, _, accuracyAfterPruning, criterionErrorAfterPruning = calcConfusionLabelsAccError(dataset, mlp, classNames, criterion, labels2Targets)
                accuracyAfterPruning = self.runningUtil.test(mlp, dataset, criterion, opt)
                if verbose >= 1:
                    prunedNeuronsStats = self.countPrunedNeuronsInModules(mlp)
                    print('Restored net Accuracy : ' +str(accuracyAfterPruning) +' prunedNeurons='+pp.pformat(prunedNeuronsStats))
                # gather some data for history
                # # pruningLog.confusionMatRestored[iter] = confusionMat
                # pruningLog.accuracyRestored[iter] = accuracyAfterPruning
                # #pruningLog.criterionErrorRestored[iter] = criterionErrorAfterPruning
                break

            #    # update best known error/accuracy
            #    if (minCriterionError > criterionErrorAfterPruning) then
            #        minCriterionError = criterionErrorAfterPruning;
            #    end
            if (highestAccuracy < accuracyAfterPruning):
                highestAccuracy = accuracyAfterPruning

            print('Iteration '+str(iter)+' finished.')
            iter = iter + 1

        return pruningLog, mlp

class ModelPingUtil:

    def __init__(self):
        pass

    # function for assessing model that accepts 2D inputs, produces 2D y_pred.
    # So in the end linear 2D space is created and fed into model, y_pred acquired, index of minimum class is acquired.
    # textual file is printed with lines containing: X, Y, modelMinOutIndex
    # ------------------------------------------
    # @ param model - model.
    # @ param width, height - amount of points on X, Y axes.
    # @ param minX, minY, maxX, maxY - parameters for generating linear space.
    # @ param fileName - to print y_pred to. If file exists it will be appended!
    def ping_model_write_file(self, model, width, height, minX, minY, maxX, maxY, fileName):
        batchInputs = self.prepare_batch_inputs(width, height, minX, minY, maxX, maxY)
        preds = model(batchInputs)
        # _, classIndx = torch.min(preds, 1)
        _, classIndx = torch.max(preds, 1)


        with open(fileName, "w") as text_file:
            for row in range(0, batchInputs.shape[0]):
                # text_file.write("Purchase Amount: %s" % TotalAmount)
                # text_file.write('{:f},{:f},{:d}\n'.format(batchInputs[row][0].item(), batchInputs[row][1].item(), classIndx[row][0].item()))
                text_file.write('{:f},{:f},{:d}\n'.format(batchInputs[row][0].item(), batchInputs[row][1].item(), classIndx[row].item()))

    # function for assessing model that accepts 2D inputs, produces 2D y_pred.
    # So in the end linear 2D space is created and fed into model, y_pred acquired, index of minimum class is acquired.
    # textual file is printed with lines containing: X, Y, modelMinOutIndex
    # ------------------------------------------
    # @ param rulegen, treeRoot - rulesGenerator and tree it will run.
    # @ param width, height - count of points on X, Y axes.
    # @ param minX, minY, maxX, maxY - parameters for generating linear space.
    # @ param fileName - to print y_pred to. If file exists it will be appended!
    def ping_tree_write_file(self, rulegen, treeRoot, width, height, minX, minY, maxX, maxY, fileName):

        batchInputs = self.prepare_batch_inputs(width, height, minX, minY, maxX, maxY)

        print("PingTreeWriteFile:")
        with open(fileName, "w") as text_file:
            for row in range(1, batchInputs.size(1)):
                input = batchInputs[row]
                print(input)
                actualOutput = rulegen.runTree(treeRoot, input)
                actualOutput = actualOutput - 1
                actualOutput = actualOutput[1]
                #    print(actualOutput)
                text_file.write(batchInputs[row][1]+','+batchInputs[row][2]+','+actualOutput+'\n')

    def prepare_batch_inputs(self, width, height, minX, minY, maxX, maxY):
        totalDX = maxX - minX
        totalDY = maxY - minY
        xStep = totalDX / width
        yStep = totalDY / height
        width_x_height = width * height
        batchInputs = torch.Tensor(width_x_height, 2).zero_()
        row = 0
        for i in range(0, width):
            for j in range(0, height):
                x = minX + xStep * i
                y = minY + yStep * j
                batchInputs[row][0] = x
                batchInputs[row][1] = y
                row = row + 1

        return batchInputs

class ActivationModifierUtil():
    def __init__(self, tensorUtil, runningUtil):
        self.runningUtil = runningUtil
        self.tensorUtil = tensorUtil

    # This function is called from outside to build intervals tensor from given activations.
    # Intervals tensor will hold 2 columns - begin and  of interval.
    # @ param intervals - is basically activations expanded to 2 column vector.
    # @ param fromIndx, toIndx - should be 1 and intervals.size()[1].
    # @ param intervalsAreAcceptableFunc - function used to test neural network - to decide about merging of intervals
    def build_intervals(self, intervals, fromIndx, toIndx, intervalsAreAcceptableFunc):
        # isinstance(activations, torch.Tensor)

        if intervals.size()[1] == 1 or fromIndx == toIndx:
            return intervals  # nothing to clusterize - there is single interval

        intervalsBackup = intervals.clone()
        continueMergingFlag = True
        mergedIntervals, iIndx, jIndx = None, None, None
        while (continueMergingFlag):
            mergedIntervals, iIndx, jIndx = self.merge_intervals(intervals, fromIndx, toIndx)
            intervalsAreGood = intervalsAreAcceptableFunc(mergedIntervals) == True
            continueMergingFlag = intervalsAreGood and mergedIntervals.size()[1] > 1 and fromIndx != toIndx
            if continueMergingFlag:
                intervals = mergedIntervals
                intervalsBackup = mergedIntervals.clone()
                toIndx = toIndx - 1  # intervals collection become 1 row shorter
            elif intervalsAreGood == False:
                mergedIntervals = intervalsBackup

        if mergedIntervals.size()[1] > 1:
            # skip these merged intervals and clusterize to the left and to the right from merge point
            mergedLeftIntervals = self.build_intervals(mergedIntervals, fromIndx, iIndx, intervalsAreAcceptableFunc)
            mergedLeftAndRightIntervals = self.build_intervals(mergedLeftIntervals, jIndx, toIndx,
                                                               intervalsAreAcceptableFunc)
            return mergedLeftAndRightIntervals
        else:
            # we have merged everything to single interval - return it
            return mergedIntervals

    def merge_intervals(self, intervals, fromIndx, toIndx):
        if fromIndx == toIndx:
            return intervals, fromIndx, toIndx

        dMin, iIndx, jIndx = self.tensorUtil.findNearestElements(intervals[{{fromIndx, toIndx}, {1}}])
        iIndx = iIndx + fromIndx - 1  # make these indexes absolute
        jIndx = jIndx + fromIndx - 1  # make these indexes absolute

        jValue = intervals[jIndx][2]
        intervals[iIndx][2] = jValue
        intervals = self.tensorUtil.delete2DTensorRow(intervals, jIndx)
        return intervals, iIndx, jIndx

    # @ param activations - must ne 2D tensor with 1 column - all activations for given neuron
    # @ param epsilon - distance for merging points into single cluster
    # @ result - clusters tensor with 1st column being clusterValue (center) and 2nd column being boundary between this cluster and next one (upper boundary?)
    #            these clusters will be used to map nonlinear neuron activations into clusterValues - thus it will do
    #            so called activations discretization
    def build_act_clusters(self, activations, epsilon):  # TODO - remove this method
        initialClusters = activations.clone().cat(activations.clone(), 2)
        for i in range(1, initialClusters.size()[1] - 1):
            initialClusters[i][2] = initialClusters[i][2] + torch.abs(initialClusters[i][2] - initialClusters[i + 1][2]) / 2
        return self.build_act_clusters_x(initialClusters, 1, initialClusters.size()[1], epsilon)

    # entry point for building clusters of activations
    def clusterize(self, initialClusters, epsilon):  # TODO - remove this method
        return self.build_act_clusters_x(initialClusters, 1, initialClusters.size()[0], epsilon)

    def build_act_clusters_x(self, clusters, fromIndx, toIndx, epsilon):
        # # isinstance(clusters, torch.Tensor)
        # if 'torch' not in torch.typename(clusters) or 'Tensor' not in torch.typename(clusters):
        #     raise Exception('t should be torch Tensor!')

        if clusters.size()[1] == 1 or fromIndx == toIndx:
            return clusters.clone()  # nothing to clusterize - there is single interval

        c = clusters[:, 1]
        if c is None:
            print('None')

        x = clusters[:, 0]
        xnumpy = x.detach().numpy()
        xnumpy = xnumpy.reshape(-1, 1)  # this is required by DBSCAN
        db = DBSCAN(eps=epsilon, min_samples=1).fit(xnumpy)
        lb = db.labels_
        unique_clusters = np.unique(lb)

        new_clusters = []
        new_clusters_boundaries = []
        previous_cluster_max = None
        i = 0
        for c in unique_clusters:
            cluster_elements = x[np.where(lb == c)]
            cluster_center = cluster_elements.mean()
            cluster_min = cluster_elements.min()
            cluster_max = cluster_elements.max()
            if i > 0 is not None:
                cluster_boundary = (previous_cluster_max + cluster_min) / 2
                new_clusters_boundaries.append(cluster_boundary)
            new_clusters.append(cluster_center)

            previous_cluster_max = cluster_max
            i = i + 1
        new_clusters_boundaries.append(MAX_FLOAT_BOUND)

        return torch.Tensor(np.column_stack((new_clusters, new_clusters_boundaries)))

        # # input_vectors = c
        # # input_vectors_sorted, _ = input_vectors.sort()
        # # pairwise_distances = input_vectors_sorted[1:] - input_vectors_sorted[:-1]
        # # pairwise_distances[pairwise_distances != pairwise_distances] = 0.0
        # # small_pairs_indices = torch.where(pairwise_distances < epsilon, torch.ones(pairwise_distances.shape), torch.zeros(pairwise_distances.shape))
        # # # small_pairs_indices = np.where(pairwise_distances < epsilon)[0]
        # # input_vectors_count = input_vectors.shape[0]
        # # rows_pairs_indices_tuples_list = list(combinations(range(input_vectors_count), 2))
        # # pairs_to_remove = torch.Tensor(rows_pairs_indices_tuples_list)[small_pairs_indices]
        # # indices_to_remove = torch.Tensor(pairs_to_remove)[:, 0]
        # # cleaned_input_vectors = torch.delete(input_vectors, indices_to_remove, axis=0)
        # # # return cleaned_input_vectors, indices_to_remove
        #
        #
        # # now 'c' contains clusters centers, we will merge centers that are too close to each other (in vicinity of epsilon)
        # # indx = torch.abs(c[{{1, -2}, {}}] - c[{{2, -1}, {}}]).le(epsilon)  # these are indexes for what we should merge if index i = 1 = > means i & i+1 activations are in one cluster
        # indx = torch.abs(c[1:-2] - c[2:-1]).le(epsilon)  # these are indexes for what we should merge if index i = 1 = > means i & i+1 activations are in one cluster
        # mergedClusters = torch.Tensor([MIN_FLOAT_BOUND])
        #
        # if indx.sum() == 0:
        #     # no clusters will be formed
        #     return clusters.clone()
        # else:
        #     initialized = False
        #     sum = 0
        #     count = 0
        #     inCluster = False
        #
        #     for i in range(0, indx.shape[0]):
        #         if inCluster == False and indx[i] == 1:
        #             inCluster = True
        #             sum = c[i] + c[i + 1]
        #             count = 2
        #         elif inCluster and indx[i] == 1:
        #             sum = sum + c[i + 1]
        #             count = count + 1
        #         elif inCluster and indx[i] == 0:
        #             clusterCenter = sum / count
        #             if initialized == False:
        #                 initialized = True
        #                 mergedClusters[0] = clusterCenter
        #             else:
        #                 mergedClusters = torch.cat((mergedClusters, torch.Tensor([clusterCenter])), 0)
        #
        #             inCluster = False
        #             sum = 0
        #             count = 0
        #         elif inCluster == False and indx[i] == 0:
        #             # just add activation value as a standalone cluster
        #             if initialized == False:
        #                 initialized = True
        #                 mergedClusters[0] = c[i]
        #             else:
        #                 # mergedClusters = torch.cat(mergedClusters, torch.Tensor([c[i]]), 0)
        #                 mergedClusters = torch.cat((mergedClusters, torch.Tensor([c[i]])), 0)
        #
        #     if inCluster:
        #         clusterCenter = sum / count
        #         if initialized == False:
        #             initialized = True
        #             mergedClusters[0] = clusterCenter
        #         else:
        #             mergedClusters = torch.cat((mergedClusters, torch.Tensor([clusterCenter])), 0)
        #
        # # add second column - where boundaries will be held
        # mergedClusters = mergedClusters.resize(mergedClusters.shape[0], 1)  # make it 2D tensor
        # mergedClusters = torch.cat((mergedClusters, torch.Tensor(mergedClusters.shape[0], 1).zero_()), 1)
        #
        # # now let's rebuild clusters boundaries - second column
        # if mergedClusters.shape[0] > 1:
        #     for i in range(0, mergedClusters.shape[0]-1):
        #         mean = (mergedClusters[i][0] + mergedClusters[i + 1][0]) / 2
        #         mergedClusters[i][1] = mean
        #
        # mergedClusters[mergedClusters.shape[0]-1][1] = MIN_FLOAT_BOUND
        #
        # return mergedClusters.clone()

    # This method merges two closest _clusters. When it does so, it returns clusters table
    # torch.FloatTensor(m, n), where m - is number of rows. Each row contains two columns
    # first is cluster centers, and second is boundary between current cluster and next.
    # So if m_0 is first cluster center, then n_0 is boundary between m_0 and m_1.
    #
    # skip_bounds is tensor containing bounds between clusters that should not be merged.
    #
    # Two merged clusters (rows) are substituted with single one holding new cluster
    # center=((m_t + m_t+1) / 2) and new cluster boundary=n_t+1
    #
    # This function does not measures any metric - allowing to estimate was clusterization done nicely, or should
    # be reversed as neural net mode metric worsened.
    #
    def clusterize_inputs(self, clusters: torch.FloatTensor, n: int, skip_bounds: torch.FloatTensor):
        clusters = clusters.clone()

        # clusters are already sorted (ascending)
        # let's find distances between neibourhooding elements
        c = clusters[:, 0]

        dist = torch.abs(c[:-1] - c[1:])
        # # if two rows have same clusters centers - we need to skip them from distances matrix
        # # (as there is no need to merge them)
        dist = dist + (MAX_FLOAT_BOUND * dist.eq(0.0)).type(torch.FloatTensor)
        _, indices = torch.sort(dist)

        # default new cluster center is mean between two closest centers
        if len(skip_bounds) > 0:
            # drop values from indices that are pointing to bounds present in skip_bounds
            b_indexed = clusters[indices, 1]
            b_indexed_repeated = b_indexed.view(-1, 1).repeat(1, skip_bounds.shape[0])
            indices = indices[b_indexed_repeated.eq(skip_bounds).sum(dim=1).eq(0)]
            if len(indices) == 0:
                return None, None, 0
        rng = min(len(indices), n)
        indexes_of_mins = indices[list(range(rng))]

        merge_bounds = []
        merged_clusters = clusters
        for index_of_min in indexes_of_mins:
            new_cluster_center = (c[index_of_min] + c[index_of_min+1]) / 2
            clusters[index_of_min+1, 0] = new_cluster_center
            merge_bound = clusters[index_of_min, 1]
            merge_bounds.append(merge_bound)
            merged_clusters = torch.cat([merged_clusters[0:index_of_min, :], merged_clusters[index_of_min+1:, :]])
            # now all indexes greater than current should be decreased
            indexes_of_mins[indexes_of_mins.gt(index_of_min)] = indexes_of_mins[indexes_of_mins.gt(index_of_min)] - 1
        return merged_clusters, merge_bounds, rng


        # clusters - MxN torch.FloatTensor, M - number of clusters centers with bounds
        # example: [[0.1, 0.2], [0.3, 0.5], [0.7, float(+inf)]] -

        # # input_vectors = c
        # # input_vectors_sorted, _ = input_vectors.sort()
        # # pairwise_distances = input_vectors_sorted[1:] - input_vectors_sorted[:-1]
        # # pairwise_distances[pairwise_distances != pairwise_distances] = 0.0
        # # small_pairs_indices = torch.where(pairwise_distances < epsilon, torch.ones(pairwise_distances.shape), torch.zeros(pairwise_distances.shape))
        # # # small_pairs_indices = np.where(pairwise_distances < epsilon)[0]
        # # input_vectors_count = input_vectors.shape[0]
        # # rows_pairs_indices_tuples_list = list(combinations(range(input_vectors_count), 2))
        # # pairs_to_remove = torch.Tensor(rows_pairs_indices_tuples_list)[small_pairs_indices]
        # # indices_to_remove = torch.Tensor(pairs_to_remove)[:, 0]
        # # cleaned_input_vectors = torch.delete(input_vectors, indices_to_remove, axis=0)
        # # # return cleaned_input_vectors, indices_to_remove
        #
        #
        # # now 'c' contains clusters centers, we will merge centers that are too close to each other (in vicinity of epsilon)
        # # indx = torch.abs(c[{{1, -2}, {}}] - c[{{2, -1}, {}}]).le(epsilon)  # these are indexes for what we should merge if index i = 1 = > means i & i+1 activations are in one cluster
        # indx = torch.abs(c[1:-2] - c[2:-1]).le(epsilon)  # these are indexes for what we should merge if index i = 1 = > means i & i+1 activations are in one cluster
        # mergedClusters = torch.Tensor([MIN_FLOAT_BOUND])
        #
        # if indx.sum() == 0:
        #     # no clusters will be formed
        #     return clusters.clone()
        # else:
        #     initialized = False
        #     sum = 0
        #     count = 0
        #     inCluster = False
        #
        #     for i in range(0, indx.shape[0]):
        #         if inCluster == False and indx[i] == 1:
        #             inCluster = True
        #             sum = c[i] + c[i + 1]
        #             count = 2
        #         elif inCluster and indx[i] == 1:
        #             sum = sum + c[i + 1]
        #             count = count + 1
        #         elif inCluster and indx[i] == 0:
        #             clusterCenter = sum / count
        #             if initialized == False:
        #                 initialized = True
        #                 mergedClusters[0] = clusterCenter
        #             else:
        #                 mergedClusters = torch.cat((mergedClusters, torch.Tensor([clusterCenter])), 0)
        #
        #             inCluster = False
        #             sum = 0
        #             count = 0
        #         elif inCluster == False and indx[i] == 0:
        #             # just add activation value as a standalone cluster
        #             if initialized == False:
        #                 initialized = True
        #                 mergedClusters[0] = c[i]
        #             else:
        #                 # mergedClusters = torch.cat(mergedClusters, torch.Tensor([c[i]]), 0)
        #                 mergedClusters = torch.cat((mergedClusters, torch.Tensor([c[i]])), 0)
        #
        #     if inCluster:
        #         clusterCenter = sum / count
        #         if initialized == False:
        #             initialized = True
        #             mergedClusters[0] = clusterCenter
        #         else:
        #             mergedClusters = torch.cat((mergedClusters, torch.Tensor([clusterCenter])), 0)
        #
        # # add second column - where boundaries will be held
        # mergedClusters = mergedClusters.resize(mergedClusters.shape[0], 1)  # make it 2D tensor
        # mergedClusters = torch.cat((mergedClusters, torch.Tensor(mergedClusters.shape[0], 1).zero_()), 1)
        #
        # # now let's rebuild clusters boundaries - second column
        # if mergedClusters.shape[0] > 1:
        #     for i in range(0, mergedClusters.shape[0]-1):
        #         mean = (mergedClusters[i][0] + mergedClusters[i + 1][0]) / 2
        #         mergedClusters[i][1] = mean
        #
        # mergedClusters[mergedClusters.shape[0]-1][1] = MIN_FLOAT_BOUND
        #
        # return mergedClusters.clone()

    # Function takes clusters tensor and looks for two nearest points.
    # if epsilon parameter is supplied then if points are merged into cluster if distance between them
    # is smaller than epsilon. If epsilon is not provided it is defaulted to MAX_FLOAT_BOUND.
    # Function operates only on subtensor rows deteremined by fromIndx and toIndx parameters.
    def merge_clusters(self, clusters, fromIndx, toIndx, epsilon, clusterizeFromEnd):
        if clusterizeFromEnd == None:
            clusterizeFromEnd = False

        if fromIndx == toIndx:
            return clusters, fromIndx, toIndx

        if epsilon == None:
            epsilon = MAX_FLOAT_BOUND

        dMin, iIndx, jIndx = self.tensorUtil.findNearestElements(clusters[{{fromIndx, toIndx}, {1}}], clusterizeFromEnd)
        iIndx = iIndx + fromIndx - 1  # make these indexes absolute
        jIndx = jIndx + fromIndx - 1  # make these indexes absolute
        continue_flag = False
        if dMin < epsilon:
            # adjust cluster value(center) for row iIndx - it should be between iIndx center and jIndex center
            mean = torch.dist(torch.Tensor({clusters[iIndx][1]}), torch.Tensor({clusters[jIndx][1]})) / 2
            clusters[iIndx][1] = clusters[iIndx][1] - mean
            clusters = self.tensorUtil.delete2DTensorRow(clusters, jIndx)
            continue_flag = True

        return clusters, iIndx, jIndx, continue_flag

    @timeit
    def record_activations(self, model, data_loader):
        # BEGIN of activations collection
        # enable activations collection
        def enable_record_flag_func(layer):
            layer.recordFlag = True

        self.runningUtil.applyToFilteredLayers(model, 'ActivationRecorder', enable_record_flag_func)
        # collect activations

        model.eval()  # Put the network into evaluation mode
        for i, (input, target) in enumerate(data_loader):
            # # Convert torch tensor to Variable
            input = Variable(input)
            model(input)  # now ActivationRecorder 's will collect activations

        # disable activations collection
        def disable_record_flag_func(layer):
            layer.recordFlag = False

        self.runningUtil.applyToFilteredLayers(model, 'ActivationRecorder', disable_record_flag_func)
        # END of activations collection


class ActivationClusterizerUtil():

    def __init__(self, tensorUtil, tableUtil):
        self.tensorUtil = tensorUtil
        self.tableUtil = tableUtil

    # function for flattening given tensor.
    # ------------------------------------------
    # @param t - tensor.
    # @ return 1D tensor containg all values from input tensor t.
    def flatten(self, t):
        isinstance(t, torch.Tensor)
        return t.view(t.numel())

    # function for returning key(index) of the element in the tensor.
    # ------------------------------------------
    # @ param t - tensor.
    # @ param value - value to look for
    # @ return tensor containg indices for values.If t is 2 dimensional, indices will be 2D.
    def keyForValue(self, t, value):
        isinstance(t, torch.Tensor)
        return torch.linspace(1, t.numel(), t.numel())[t.eq(value)]

    # function for returning indices of folds
    # ------------------------------------------
    # @ param t - tensor.
    # @ return uniqueset - tensor with unique t values
    # @ return ic
    def uniqueTensorValues(self, t):
        isinstance(t, torch.Tensor)
        valsetTable = self.tensorUtil.frequencies(t)
        # now convert that valset table keys into normal table with values
        uniqueset = {}
        n = 0
        for k, _ in enumerate(valsetTable):
            n = n + 1
            uniqueset[n] = k

        ic = self.tableUtil.icForUnique(uniqueset, t.totable())

        return uniqueset, ic

    # function for checking table contains element
    # --------------------------------------------
    # @ param t - tensor.
    # @ param value - value to check.
    # @ return True / False
    def contains(self, t, value):
        isinstance(t, torch.Tensor)
        if 0 == t[t.eq(value)].numel():
            return False
        else:
            return True

    # function returns table with keys equal to unique values from input tensor
    # and values equal to number of values encountered in input
    def unique_counts(self, input):
        isinstance(input, torch.Tensor)

        size = input.numel()
        y = input.view(size)  # flatten input
        b = {}
        for i in range(1, size):
            a = y[i]
            if b[a] == None:
                b[a] = 1
            else:
                b[a] = b[a] + 1
        return b

    # function for returning frequencies of the elements in the table.
    # ------------------------------------------
    # @ param t - tensor.
    # @ return table containing frequencies.
    def frequencies(self, t):
        isinstance(t, torch.Tensor)

        valset = {}
        t.view(t.numel())
        i = 1
        for v in t[i]:
            if (valset[v] == None):
                valset[v] = 1
            else:
                valset[v] = valset[v] + 1
        return valset

    # function for returning minimal distance between 2 elements of tensor.
    # ------------------------------------------
    # @ param t - must be sorted(!) 1 column tensor with at least 1 element
    # @ return minDistance, index1, index2.
    def findNearestElements(self, tensor):
        if (tensor.numel() == 1):
            return 0, 1, 1

        dMin = MAX_FLOAT_BOUND
        iIndx = -1
        jIndx = -1
        for i in range(1, tensor.numel() - 1):
            dist = math.abs(tensor[i][1] - tensor[i + 1][1])
            if (dist < dMin):
                dMin = dist
                iIndx = i
                jIndx = i + 1

        return dMin, iIndx, jIndx

    # function for deleting 2D tensor row.
    # --------------------------------------------
    # @ param tensor - must be 2D tensor
    # @ param row - row index to remove
    # @ return tensor without specified row
    def delete2DTensorRow(self, tensor, row):
        mask = torch.ByteTensor(tensor.size()).fill(1)
        mask[row].fill(0)
        newSize = tensor.size()
        newSize[1] = newSize[1] - 1
        return tensor.maskedSelect(mask).resize(newSize)


# function for returning frequencies of the rows elements in the table.
# -------------------------------------------------
# @ param t - 2D tensor.
# @ return uniqueTensorRows - tensor with unique rows
# @ return frequencies - list with appropriate unique tensor values counts.
def rowsFrequencies_standalone(t):
    isinstance(t, torch.Tensor)
    if len(t.shape) == 1:
        t = t.resize(t.shape[0], 1)

    t_unique = torch.unique(t, sorted=False)
    #
    counts = []
    for i in range(0, t_unique.shape[0]):
        unique_count = (t == t_unique[i]).sum().item()
        counts.append(unique_count)

    return t_unique, counts

    # # rowLength = t.size()[1]
    # uniqueTensorRows = torch.FloatTensor()
    # frequencies = {}
    #
    # for i in range(0, t.size()[0]):
    #     tensorRow = t[i].clone()  #.resize(1, rowLength)
    #     if uniqueTensorRows.size()[0] == 0:
    #         uniqueTensorRows = tensorRow
    #         frequencies[0] = 1
    #     elif False == tensorContainsRow_standalone(tensorRow, uniqueTensorRows):
    #         uniqueTensorRows = torch.cat((uniqueTensorRows, tensorRow), 0)
    #         frequencies[len(frequencies)] = 1
    #     else:
    #         rowIndex = findRow_standalone(tensorRow, uniqueTensorRows)
    #         frequencies[rowIndex] = frequencies[rowIndex] + 1
    #
    # return uniqueTensorRows, frequencies

# function returns TRUE if given 1D tensor(row) is contained within given 2D tensor(holding rows)
# row - is a 1D tensor
# tensor - is a 2D tensor which will be checked(does it contains row under question)
def tensorContainsRow_standalone(row, tensor):
    for i in range(0, tensor.size()[0]):
        tensorRow = tensor[i]

        if (torch.all(torch.eq(row, tensorRow))):
            return True
    return False

# function for returning index of row in tensor containing given row.
# --------------------------------------------------
# @ param t - 2D tensor. Within this tensor we will look for rows equal to given one.
# Index of first equal will be returned. If nothing is found - 1 is returned.
#
# @ return uniqueTensorRows - tensor with unique rows
# @ return frequencies - table containing frequencies.Key is row index in uniqueTensorRows tensor.
def findRow_standalone(row, t):
    isinstance(t, torch.Tensor)

    for i in range(0, t.size()[0]):
        if torch.all(torch.eq(row, t[i])):
            return i
    return -1

class TensorUtil:

    def __init__(self, tableUtil):
        self.tableUtil = tableUtil

    # function for flattening given tensor.
    # --------------------------------------------
    # @ param t - tensor.
    # @ return 1D tensor containg all values from input tensor t.
    def flatten(self, t):
        isinstance(t, torch.Tensor)
        return t.view(t.numel())

    # function for returning key(index) of the element in the tensor.
    # --------------------------------------------
    # @ param t - tensor.
    # @ param value - value to look for
    # @ return tensor containg indices for values.If t is 2 dimensional, indices will be 2D.
    def keyForValue(self, t, value):
        isinstance(t, torch.Tensor)
        return torch.linspace(1, t.numel(), t.numel())[t.eq(value)]

    # function for returning indices of folds
    # --------------------------------------------
    # @ param t - tensor.
    # @ return uniqueset - tensor with unique t values
    # @ return ic

    # use torch.unique https://pytorch.org/docs/master/torch.html#torch.unique
    def uniqueTensorValues(self, t):
        isinstance(t, torch.Tensor)
        valsetTable = self.frequencies(t)
        # now convert that valset table keys into normal table with values
        uniqueset = {}
        n = 0
        for k, _ in valsetTable.items():  # we assume valsetTable is dict
            n = n + 1
            uniqueset[n] = k

        ic = self.tableUtil.icForUnique(uniqueset, t.totable())

        return uniqueset, ic

    # function for returning indices of folds
    #--------------------------------------------
    # @ param t - 2D tensor.
    # @ return uniqueset - tensor with unique t values
    # @ return ic
    def uniqueTensorRows(self, t):
        isinstance(t, torch.Tensor)
        valsetTable = self.frequencies(t)
        # now convert that valset table keys into normal table with values
        uniqueset = {}
        n = 0
        for k, _ in valsetTable.items():  #we assume valsetTable is dict
            n = n + 1
            uniqueset[n] = k

        ic = self.tableUtil.icForUnique(uniqueset, t.totable())

        return uniqueset, ic

    # function for checking table contains element
    # -------------------------------------------
    # @ param t - tensor.
    # @ param value - value to check.
    # @ return True / False
    #
    # Duplicate of TableUtil.*
    def contains(self, X, value):
        if 'torch' not in torch.typename(X) or 'Tensor' not in torch.typename(X):
            raise Exception('t should be torch Tensor!')

        _ones = torch.ones(X.size())
        _zeros = torch.zeros(X.size())
        x_eq = torch.where(X == value, _zeros, _ones)
        return x_eq.sum() > 0

    # function returns table with keys equal to unique values from input tensor
    # and values equal to number of values encountered in input
    def unique_counts(self, input):
        isinstance(input, torch.Tensor)

        size = input.numel()
        y = input.view(size)        #flatten input
        b = {}
        for i in range(1, size):
            a = y[i]
            if b[a] == None:
                b[a] = 1
            else:
                b[a] = b[a] + 1
        return b

    # function for returning frequencies of the elements in the tensor.
    # -----------------------------------------------
    # @ param t - 1D tensor.
    # @ return table containing frequencies.
    def frequencies(self, t):
        isinstance(t, torch.Tensor)

        valset = {}
        tt = t.view(-1)
        i = 1
        for i in range(1, tt.numel()):
            v = tt[i]
            if (valset[v] == None):
                valset[v] = 1
            else:
                valset[v] = valset[v] + 1
        return valset

    # function for returning frequencies of the rows elements in the table.
    # -------------------------------------------------
    # @ param t - 2D tensor.
    # @ return uniqueTensorRows - tensor with unique rows
    # @ return frequencies - table containing frequencies.Key is row index in uniqueTensorRows tensor.
    def rowsFrequencies(self, t):
        isinstance(t, torch.Tensor)
        if len(t.shape) == 1:
            t = t.resize(t.shape[0], 1)

        # t_unique, inverse_indices = torch.unique(t, sorted=False, return_inverse=True)
        # #
        # counts = {}
        # for i in range(0, t_unique.shape[0]):
        #     unique_count = (inverse_indices == t_unique[i]).sum().item()
        #     counts.append(unique_count)

        # rowLength = t.size()[1]
        uniqueTensorRows = torch.FloatTensor()
        frequencies = {}

        for i in range(0, t.size()[0]):
            tensorRow = t[i].clone()  #.resize(1, rowLength)
            if uniqueTensorRows.size()[0] == 0:
                uniqueTensorRows = tensorRow
                frequencies[0] = 1
            elif False == self.tensorContainsRow(tensorRow, uniqueTensorRows):
                uniqueTensorRows = torch.cat((uniqueTensorRows, tensorRow), 0)
                frequencies[len(frequencies)] = 1
            else:
                rowIndex = self.findRow(tensorRow, uniqueTensorRows)
                frequencies[rowIndex] = frequencies[rowIndex] + 1

        return uniqueTensorRows, frequencies


    # function for returning index of row in tensor containing given row.
    # --------------------------------------------------
    # @ param t - 2D tensor. Within this tensor we will look for rows equal to given one.
    # Index of first equal will be returned. If nothing is found - 1 is returned.
    #
    # @ return uniqueTensorRows - tensor with unique rows
    # @ return frequencies - table containing frequencies.Key is row index in uniqueTensorRows tensor.
    def findRow(self, row, t):
        isinstance(t, torch.Tensor)

        for i in range(0, t.size()[0]):
            if self.tensorsEqual2(row, t[i]):
                return i
        return -1


    # function for returning minimal distance between 2 elements of tensor.
    # --------------------------------------------------
    # @ param t - must be sorted(!) 1 column tensor with at least 1 element
    # @ return minDistance, index1, index2.
    def findNearestElements(self, tensor, fromEnd):
        if tensor.numel() == 1:
            return 0, 1, 1

        dMin = MAX_FLOAT_BOUND
        iIndx = -1
        jIndx = -1
        for i in range(0, tensor.numel() - 1):
            if (fromEnd is not None and fromEnd == True):
                i = tensor.numel() - i

        dist = MAX_FLOAT_BOUND
        if tensor.size().size() == 2:
            dist = math.abs(tensor[i][1] - tensor[i + 1][1])
        elif(tensor.size().size() == 1):
            dist = math.abs(tensor[i] - tensor[i + 1])
        else:
            dim = tensor.size().size()
            raise Exception("expected 1 column tensor or vector , but got "+str(dim)+" dimensions")

        if dist < dMin:
            dMin = dist
            iIndx = i
            jIndx = i + 1

        return dMin, iIndx, jIndx

    # function for deleting 2D tensor row.
    # ----------------------------------------------------
    # @ param tensor - must be 2D tensor
    # @ param row - row index to remove
    # @ return tensor without specified row
    def delete2DTensorRow(self, tensor, row):
        isinstance(tensor, torch.Tensor)

        mask = torch.ByteTensor(tensor.size()).fill(1)
        mask[row].fill(0)

        newSize = tensor.size()
        newSize[1] = newSize[1] - 1
        return tensor.maskedSelect(mask).resize(newSize)


    def typesEqual(self, o1, o2):
        return torch.typename(o1) == torch.typename(o2)


    def storagesEqual(self, s1, s2):
        # typeUtil.assert (s1, 'Storage')
        # typeUtil.assert (s2, 'Storage')
        if type(s1) == type(s2):
            if (s1.size() == s2.size()):
                for i in range(1, s1.size()):
                    if s1[i] != s2[i]:
                        return False
                return True
        return False


    # function returns True if 2 passed tensors are equal(by shape and contents)
    def tensorsEqual(self, t1, t2):
        # return torch.all(torch.eq(t1, t2)) -- this does not respect dimensionalities
        t1 = t1.clone()
        t2 = t2.clone()
        # typeUtil.assert (t1, 'Tensor')
        # typeUtil.assert (t2, 'Tensor')

        if 'torch' not in torch.typename(t1) or 'Tensor' not in torch.typename(t1):
            raise Exception('first param should be torch Tensor!')
        if 'torch' not in torch.typename(t2) or 'Tensor' not in torch.typename(t2):
            raise Exception('second param should be torch Tensor!')

        if (self.storagesEqual(t1.size(), t2.size())):
            flatT1 = self.flatten(t1)
            flatT2 = self.flatten(t2)
            return flatT1.add(flatT2.mul(-1)).eq(0).sum() == flatT1.numel()

        return False


    # function returns True if 2 passed tensors are equal(by contents!)
    def tensorsEqual2(self, t1, t2):
        return torch.all(torch.eq(t1, t2)) # this does not respect dimensionalities

    # function returns TRUE if given 1D tensor(row) is contained within given 2D tensor(holding rows)
    # row - is a 1D tensor
    # tensor - is a 2D tensor which will be checked(does it contains row under question)
    def tensorContainsRow(self, row, tensor):
        for i in range(0, tensor.size()[0]):
            tensorRow = tensor[i]
            if (self.tensorsEqual2(row, tensorRow)):
                return True
        return False


    # function scales 2D tensor columns into specified range
    # assumes there are no NaN's
    def scaleTensor(self, t, newMinVal, newMaxVal):
        # remove nan's rows
        nans = t.ne(t).max(2)
        nansIndices = torch.linspace(1, t.size(1), t.size(1)).long()
        datain = t.index(1, nansIndices[nans.eq(0)]).clone()


        oldMinValue = datain.min()
        oldMaxValue = datain.max()
        oldRange = oldMaxValue - oldMinValue
        if (oldRange == 0):
            return datain.apply(lambda x: oldMinValue), oldMinValue, oldMaxValue;
        else:
            newRange = newMaxVal - newMinVal
            return datain.apply(lambda x: (((x - oldMinValue) * newRange) / oldRange) + newMinVal), oldMinValue, oldMaxValue


    # function assumes there is linear table, it's values will be used to fill tensor
    def buildTensorFromConfusionsTable(self, table, extractorFunc):
        size = self.tableUtil.getSize(table)

        tensor = torch.Tensor(size)
        for i in range(1, size):
            tensor[i] = extractorFunc(table[i])
            i = i + 1

        return tensor


    # Function for writing tensor to csv file.
    # Should be used like: 
    #  out = assert (io.open("./dump.csv", "w"))
    #  tensorUtil.writeTensorToCsv(t, out, ",")
    #  out: close()
    def writeTensorToCsv(self, t, out, splitter):
        for i in range(1, t.size(1)):
            for j in range(1, t.size(2)):
                out.write(t[i][j])
                if j == t.size(2):
                    out.write("\n")
                else:
                    out.write(splitter)

def generate_full_boundary_dataset(_model, input_clusters):  #, batch_size=100

    list_of_lists = []
    for input_attr_indx in range(input_clusters.shape[0]):
        attribute_clusters_with_boundaries = input_clusters[input_attr_indx]
        clusters_centers = attribute_clusters_with_boundaries[:, 0]
        clusters_centers = clusters_centers[clusters_centers < MAX_FLOAT_BOUND]
        list_of_lists.append(clusters_centers.tolist())
    all_combinations = itertools.product(*list_of_lists)

    batch = []
    predict_list = []
    for centers_combination in all_combinations:
        batch.append(centers_combination)
        # if len(batch) == batch_size:
        #     batch_tensor = torch.FloatTensor(batch)
        #     print(batch_tensor)
        #     batch = []
        #
        #     outputs = _model(input)  # Do the forward pass
        #     _, predicted = torch.max(outputs.data, 1)
        #     train_predict_list.append(predicted)



    batch_tensor = torch.FloatTensor(batch)
    outputs = _model(batch_tensor)
    _, predicted = torch.max(outputs.data, 1)
    dummy_y = predicted  #torch.zeros(batch_tensor.shape[0])
    return batch_tensor, dummy_y, XYDataset(batch_tensor, dummy_y)

    # # get train data evaluation
    # train_predict_list = []
    # X_train = []
    # y_train = []
    # for batch_idx, (input, target) in enumerate(_dataLoader):
    #     outputs = _model(input)  # Do the forward pass
    #     _, predicted = torch.max(outputs.data, 1)
    #     train_predict_list.append(predicted)
    #     X_train.append(input.squeeze(0))
    #     y_train.append(target.squeeze(0))


def get_point_neigbourhoods_indices(point: List, min_bounds: List, max_bounds: List, only_increasing_neighbourhoods=False):
    """
    We are dealing with integer indices.
    Point - vector holding component values - indices (like [0,0,0] for 3D space)
    Point should be between or equal to min_bounds and max_bounds which are N-dim points similar to point.

    Function returns list of points (lists) that are neighbourhoods of our 'point'.
    Examples in 2D space:

    (point = [0,0], min_bounds = [0,0], max_bounds=[10,10], False) ==>  [[1,0], [0,1]]
    (point = [3,0], min_bounds = [0,0], max_bounds=[10,10], False) ==>  [[2,0], [3,1], [4,0]]
    (point = [3,1], min_bounds = [0,0], max_bounds=[10,10], False) ==>  [[3,0], [4,1], [3,2], [2,1]]
    (point = [10,10], min_bounds = [0,0], max_bounds=[10,10], False) ==>  [[10,9], [9,10]]

    (point = [0,0], min_bounds = [0,0], max_bounds=[10,10], True) ==>  [[1,0], [0,1]]
    (point = [3,0], min_bounds = [0,0], max_bounds=[10,10], True) ==>  [[3,1], [4,0]]
    (point = [3,1], min_bounds = [0,0], max_bounds=[10,10], True) ==>  [[4,1], [3,2]]
    (point = [10,10], min_bounds = [0,0], max_bounds=[10,10], True) ==>  []

    :param point: List of length N containing integers
    :param min_bounds: List of length N containing integers
    :param max_bounds: List of length N containing integers
    :param only_increasing_neighbourhoods: True - all neighbourghhods returned, False - only with increasing indexes.
    :return: list of neighbourhhods (thinkk of them as nearest cells on checkers board (for 2D space case)).
    """

    neighbourghoods = []
    for i in range(len(point)):
        p_i = point[i]
        min_i = min_bounds[i]
        max_i = max_bounds[i]

        if p_i > min_i:
            if not only_increasing_neighbourhoods:
                p = copy.deepcopy(point)
                p[i] = p[i] - 1
                neighbourghoods.append(p)

        if p_i < max_i:
            p = copy.deepcopy(point)
            p[i] = p[i] + 1
            neighbourghoods.append(p)

    return neighbourghoods


def shift_array(arr, axis, steps):
    """
    Shifts array +1 (forward=True) or -1 (forward=False) along given dimension (axis)
    and fills new row/column with last/first row/column:

    x = [[1,2],
         [3,4],
         [5,6]]
    shift_array(x, 0, True) ==>>
        [[1,2],
         [1,2],
         [3,4]]
    shift_array(x, 0, False) ==>>
        [[3,4],
         [5,6],
         [5,6]]
    shift_array(x, 1, True) ==>>
        [[1,1],
         [3,3],
         [5,5]]

    :param arr: ndim numpy array
    :param axis: dimension index
    :param steps: +1 , -1  -direction to make shift
    :return: shifted numpy array
    """
    if steps > 0:
        slc = [slice(None)] * len(arr.shape)
        slc[axis] = slice(0, arr.shape[axis]-1)
        a = arr[slc]
        # now add missing part
        slc = [slice(None)] * len(arr.shape)
        slc[axis] = slice(0, 1)
        beginning = arr[slc]
        return np.concatenate((beginning, a), axis=axis)
    elif steps < 0:
        slc = [slice(None)] * len(arr.shape)
        slc[axis] = slice(1, arr.shape[axis])
        a = arr[slc]
        # now add missing part
        slc = [slice(None)] * len(arr.shape)
        slc[axis] = slice(arr.shape[axis]-1, arr.shape[axis])
        endining = arr[slc]
        return np.concatenate((a, endining), axis=axis)
    else:
        return arr

def generate_boundary_dataset(_model, input_clusters, bound_steps=3):  #, batch_size=100

    list_of_points = []
    list_of_points_indices = []
    list_of_dims_lengths = []
    for input_attr_indx in range(input_clusters.shape[0]):
        attribute_clusters_with_boundaries = input_clusters[input_attr_indx]
        clusters_centers = attribute_clusters_with_boundaries[:, 0]
        clusters_centers = clusters_centers[clusters_centers < MAX_FLOAT_BOUND]
        list_of_points.append(clusters_centers.tolist())
        clusters_count = clusters_centers.shape[0]
        list_of_points_indices.append(list(range(clusters_count)))
        list_of_dims_lengths.append(clusters_count)

    all_points = list(itertools.product(*list_of_points))
    # all_points_indices = itertools.product(*list_of_points_indices)

    # all_points = []
    # predict_list = []
    # for centers_combination in itertools.product(*list_of_points):
    #     all_points.append(centers_combination)
    #     # if len(all_points) == batch_size:
    #     #     X = torch.FloatTensor(all_points)
    #     #     print(X)
    #     #     all_points = []
    #     #
    #     #     outputs = _model(input)  # Do the forward pass
    #     #     _, predicted = torch.max(outputs.data, 1)
    #     #     train_predict_list.append(predicted)

    X = torch.FloatTensor(all_points)
    outputs = _model(X)
    _, y = torch.max(outputs.data, 1)

    ########################################
    # Stage 2 - let's drop all points that have all nearby points of same class (label)
    n_dim = len(list_of_dims_lengths)

    # Let's put all our points (1D list) into a n-dim numpy array to ease access to neighbourhoods
    # define datatype for cell
    # pointdt = np.dtype([('point_coords', np.float64, n_dim)])
    dtype_str = ','.join(['float' for _ in range(n_dim)])
    pointdt = np.dtype(dtype_str)
    n_dim_shape = tuple(list_of_dims_lengths)
    points_array_1Dim = np.array(all_points, dtype=pointdt)
    points_array_NDim = points_array_1Dim.reshape(n_dim_shape)
    y_array_NDim = y.numpy().reshape(n_dim_shape)

    y_diffs = np.zeros(n_dim_shape)

    for dim_indx in range(len(list_of_dims_lengths)):  # across all dimensions

        curr_dim_steps = min(bound_steps, list_of_dims_lengths[dim_indx]-1)
        for s in range(curr_dim_steps):
            dim_back = shift_array(y_array_NDim, dim_indx, s)
            dim_forward = shift_array(y_array_NDim, dim_indx, -1 * s)
            y_diffs_back = np.abs(np.subtract(dim_back, y_array_NDim))
            y_diffs_forward = np.abs(np.subtract(dim_forward, y_array_NDim))
            y_diffs = y_diffs + y_diffs_back + y_diffs_forward

    X_np = points_array_NDim[y_diffs > 0].flatten()
    Y_np = y_array_NDim[y_diffs > 0].flatten()

    if X_np.shape[0] > 0:
        X = torch.FloatTensor(np.array(X_np.tolist()))  # X_np is 1D array of tuples - hence magic with tolist -> toarray
        y = torch.FloatTensor(np.array(Y_np.tolist()))  # Y_np is 1D array of tuples - hence magic with tolist -> toarray
    else:  # return all points
        X = torch.FloatTensor(all_points)

    return X, y, XYDataset(X, y)

    # # get train data evaluation
    # train_predict_list = []
    # X_train = []
    # y_train = []
    # for batch_idx, (input, target) in enumerate(_dataLoader):
    #     outputs = _model(input)  # Do the forward pass
    #     _, predicted = torch.max(outputs.data, 1)
    #     train_predict_list.append(predicted)
    #     X_train.append(input.squeeze(0))
    #     y_train.append(target.squeeze(0))

def splitTables_standalone(t, tClasses, colNum, colSplitValue):
    column = t[:, colNum]
    smallerIdx = column < colSplitValue
    largerIdx = column > colSplitValue
    if smallerIdx.sum().item() + largerIdx.sum().item() != column.shape[0]:
        print(t.numpy())
        print(tClasses.numpy())
        print(colNum)
        print(colSplitValue)
        raise Exception('Indexing went wrong, seems colSplitValue equals to values - it is wrong! ColSplitValue is cluster boundary - it should be somewhere in between any of the `t` values (input values).')
    return t[smallerIdx], tClasses[smallerIdx], t[largerIdx], tClasses[largerIdx]

def _tensorInformationGain_wrapper(resortedT, resortedTClasses, colNum, tmpSplitValue):
    tmpInfoGain = tensorInformationGain_standalone(resortedT, resortedTClasses, colNum, tmpSplitValue)
    return (colNum, tmpSplitValue, tmpInfoGain)

# We are making single cut. Thus we will get binary classification trees.
# Function calculates information gain in case of splitting original
# tensor 't' and corresponding classes tensor 'tClasses' over column 'colNum'
# (column number) using specified value (where cut will be made) colSplitValue
# ------------------------------------------
# @ param t - 2D tensor containing vectors as rows.
# @ param tClasses - 2D tensor containing vectors as rows for classes.Each class is a row having 1 or more elements.
# @ param colNum - column number to make cut on.
# @ param colSplitValue - value to split over given column number.
# @ return information gain for defined split of the table.
def tensorInformationGain_standalone(t, tClasses, colNum, colSplitValue):
    if 'torch' not in torch.typename(t) or 'Tensor' not in torch.typename(t):
        raise Exception('first param should be torch Tensor!')
    # if (_DEBUG == true):
    #     if (t.size().size() != 2):
    #           raise Exception("expected 2D tensor, but got " + str(t.size().size() + "D")
    # totalRowsCount = tClasses:size()[1]

    entropyBefore = tensorEntropy_standalone(tClasses)
    smallerT, smallerTClasses, largerT, largerTClasses = splitTables_standalone(t, tClasses, colNum, colSplitValue)

    smallerTClassesEntropy = 0
    smallerRowsCount = 0
    if smallerT.numel() != 0:
        smallerRowsCount = smallerTClasses.size()[0]
        smallerTClassesEntropy = tensorEntropy_standalone(smallerTClasses)

    largerTClassesEntropy = 0
    largerRowsCount = 0
    if largerT.numel() != 0:
        largerRowsCount = largerTClasses.size()[0]
        largerTClassesEntropy = tensorEntropy_standalone(largerTClasses)

    totalRowsCount = t.size()[0]

    entropyAfter = (smallerRowsCount / totalRowsCount) * smallerTClassesEntropy + \
                        (largerRowsCount / totalRowsCount) * largerTClassesEntropy

    infoGain = entropyBefore - entropyAfter

    return infoGain

# function for entropy estimation of the given vertical tensor.
# we assume that each row of the tensor is separate vector.
# ------------------------------------------
# @ param t - 2D tensor.
# @ return entropy of the vectors set.
def tensorEntropy_standalone(tClasses):
    if 'torch' not in torch.typename(tClasses) or 'Tensor' not in torch.typename(tClasses):
        raise Exception('first param should be torch Tensor!')

    uniqueRowsTensor, frequencies = rowsFrequencies_standalone(tClasses)
    rowsCount = tClasses.shape[0]
    entropy = 0
    for v in frequencies:
        a = v / rowsCount
        entropy = entropy + (-1) * a * math.log(a) / math.log(2)
    return entropy


class MathUtil:

    def __init__(self, is_debug, tensorUtil):
        self.tensorUtil = tensorUtil
        self.is_debug = is_debug


    # ------------------------------------------
    # @ param x - number.
    # @ return log2(x)
    def log2(self, x):
        return math.log(x) / math.log(2)

    # function for entropy estimation of the given vertical tensor.
    # we assume that each row of the tensor is separate vector.
    # ------------------------------------------
    # @ param t - 2D tensor.
    # @ return entropy of the vectors set.
    def tensorEntropy(self, tClasses):
        if 'torch' not in torch.typename(tClasses) or 'Tensor' not in torch.typename(tClasses):
            raise Exception('first param should be torch Tensor!')
        if self.is_debug:
            if tClasses.size().size() != 2:
                raise Exception("expected 2D tensor, but got " + str(tClasses.size().size()) + "D")

        uniqueRowsTensor, frequencies = self.tensorUtil.rowsFrequencies(tClasses)
        rowsCount = tClasses.shape[0]
        entropy = 0
        for k, v in frequencies.items():
            a = v / rowsCount
            entropy = entropy + (-1) * a * self.log2(a)
        return entropy

    def entropy(self, x):
        """
        x is assumed to be an (nsignals, nsamples) array containing integers between
        0 and n_unique_vals
        """
        x = np.atleast_2d(x)
        nrows, ncols = x.shape
        nbins = x.max() + 1

        # count the number of occurrences for each unique integer between 0 and x.max()
        # in each row of x
        counts = np.vstack((np.bincount(row, minlength=nbins) for row in x))

        # divide by number of columns to get the probability of each unique value
        p = counts / float(ncols)

        # compute Shannon entropy in bits
        return -np.sum(p * np.log2(p), axis=1)

    # We are making single cut. Thus we will get binary classification trees.
    # Function calculates information gain in case of splitting original
    # tensor 't' and corresponding classes tensor 'tClasses' over column 'colNum'
    # (column number) using specified value (where cut will be made) colSplitValue
    # ------------------------------------------
    # @ param t - 2D tensor containing vectors as rows.
    # @ param tClasses - 2D tensor containing vectors as rows for classes.Each class is a row having 1 or more elements.
    # @ param colNum - column number to make cut on.
    # @ param colSplitValue - value to split over given column number.
    # @ return information gain for defined split of the table.
    def tensorInformationGain(self, t, tClasses, colNum, colSplitValue):
        if 'torch' not in torch.typename(t) or 'Tensor' not in torch.typename(t):
            raise Exception('first param should be torch Tensor!')
        # if (_DEBUG == true):
        #     if (t.size().size() != 2):
        #           raise Exception("expected 2D tensor, but got " + str(t.size().size() + "D")
        # totalRowsCount = tClasses:size()[1]

        entropyBefore = self.tensorEntropy(tClasses)
        smallerT, smallerTClasses, largerT, largerTClasses = self.splitTables(t, tClasses, colNum, colSplitValue)

        smallerTClassesEntropy = 0
        smallerRowsCount = 0
        if smallerT.numel() != 0:
            smallerRowsCount = smallerTClasses.size()[0]
            smallerTClassesEntropy = self.tensorEntropy(smallerTClasses)

        largerTClassesEntropy = 0
        largerRowsCount = 0
        if largerT.numel() != 0:
            largerRowsCount = largerTClasses.size()[0]
            largerTClassesEntropy = self.tensorEntropy(largerTClasses)

        totalRowsCount = t.size()[0]

        entropyAfter = (smallerRowsCount / totalRowsCount) * smallerTClassesEntropy + \
                            (largerRowsCount / totalRowsCount) * largerTClassesEntropy

        infoGain = entropyBefore - entropyAfter

        return infoGain

    # This function splits t tensor and tClasses tensor based on columnNumber and splitValue for that column( in t tensor)
    # t is being sorted on specified column and splitted.
    def splitTables(self, t, tClasses, colNum, colSplitValue):
        column = t[:, colNum]
        smallerIdx = column < colSplitValue
        largerIdx = column > colSplitValue
        if smallerIdx.sum().item() + largerIdx.sum().item() != column.shape[0]:
            print(t.numpy())
            print(tClasses.numpy())
            print(colNum)
            print(colSplitValue)
            raise Exception('Indexing went wrong, seems colSplitValue equals to values - it is wrong! ColSplitValue is cluster boundary - it should be somewhere in between any of the `t` values (input values).')
        return t[smallerIdx], tClasses[smallerIdx], t[largerIdx], tClasses[largerIdx]



        # totalRowsCount = t.shape[0]
        # column = t[:, colNum]
        #
        # sortedCol, indx = column.sort(0, descending=False)  #torch.sort(column, 1)
        #
        # i = 0
        # while i + 1 < totalRowsCount:
        #     if sortedCol[i + 1][0] <= colSplitValue:
        #         i = i + 1
        #     else:
        #         break
        #
        #
        # # if (sortedCol[i + 1][0] <= colSplitValue):
        # #     i = i + 1
        #
        # # get table part which holds values smaller than given colSplitValue
        # # extract corresponding tClasses as well
        #
        # linRange = torch.arange(0, indx.numel()-2).long()
        # if len(indx.shape) == 1:
        #     smallerIndexes = indx[linRange][sortedCol.le(colSplitValue)].long()
        # else:
        #     smallerIndexes = indx[:, linRange][sortedCol.le(colSplitValue)].long()
        # smallerRowsCount = 0
        #
        # smallerTClasses = torch.Tensor()
        # if smallerIndexes.numel() != 0:
        #     oneDimSmallerIndexes = smallerIndexes.resize(smallerIndexes.numel())
        #     if len(indx.shape) == 1:
        #         smallerT = t[oneDimSmallerIndexes]
        #         smallerTClasses = tClasses[oneDimSmallerIndexes]
        #     else:
        #         smallerT = t[:, oneDimSmallerIndexes]
        #         smallerTClasses = tClasses[:, oneDimSmallerIndexes]
        #
        #
        # # get table part which holds values greater than given colSplitValue
        # # extract corresponding tClasses as well
        #
        # if len(indx.shape) == 1:
        #     largerIndexes = indx[linRange][sortedCol.gt(colSplitValue)].long()
        # else:
        #     largerIndexes = indx[:, linRange][sortedCol.gt(colSplitValue)].long()
        # largerT = torch.Tensor()
        # largerTClasses = torch.Tensor()
        # if (largerIndexes.numel() != 0):
        #     oneDimLargerIndexes = largerIndexes.resize(largerIndexes.numel())
        #     if len(indx.shape) == 1:
        #         largerT = t[oneDimLargerIndexes]
        #         largerTClasses = tClasses[oneDimLargerIndexes]
        #     else:
        #         largerT = t[:, oneDimLargerIndexes]
        #         largerTClasses = tClasses[:, oneDimLargerIndexes]
        #
        # return smallerT, smallerTClasses, largerT, largerTClasses


    # Function accepts two tables, one contains data clusters - each row is
    # a tensor with cluster centers.And the other is y_pred clusters
    # (in case we have single neuron there will be single valued tensors) values.
    # Function calculates index of attribute and value between two rows for data split.
    # --------------------------------------------
    # @ param t - 2D tensor containing vectors as rows.
    # @ param tClasses - 2D tensor containing vectors as rows for classes.Each class is a row having 1 or more elements.
    # @ param inputClustersWithBoundaries - table holding 2D tensors that in each row hold clustered value in column  # 1 and upper cluster boundary (splitValue) in column #2
    # @ return colNum - column number to make cut on.
    # @ return colSplitValue - value to split over given column number.
    # @ return calculationsPerformed - true if everything worked, false if no calculations was performed
    def findBestSplit(self, X, y, inputClustersWithBoundaries):
        # typeUtil.assert (X, "Tensor")
        if 'torch' not in torch.typename(X) or 'Tensor' not in torch.typename(X):
            raise Exception('first param should be torch Tensor!')

        # if self._DEBUG == true:
        #     if (X.size().size()!= 2):
        #          raise Exception("expected 2D tensor, but got " + str(X.size().size()) + "D")

        calculationsPerformed = False
        maxInfoGain = MIN_FLOAT_BOUND
        columnIndex = -1
        splitValue = MIN_FLOAT_BOUND
        columnsCount = X.size()[1]
        totalRowsCount = X.size()[0]
        estimatedColumnsAndSplits = []
        for colNum in range(0, columnsCount):
            column = X[:, colNum].clone()
            sortedCol, indx = column.sort(0, descending=False)  #torch.sort(column, 1)

            # adjust y sorting

            # indexes = indx.index(1, torch.range(1, indx.numel()).long()).long().resize(indx.numel())
            # resortedTClasses = y.index(0, indx)
            if len(y.shape) == 1:
                resortedTClasses = y[indx]
            else:
                resortedTClasses = y[indx, :]
            # resortedT = X.index(0, indx)
            resortedT = X[indx, :]

            # # Visualize our resorting excersises
            # plt.subplot(1, 2, 1)
            # t_numpy = X.numpy()
            # t_classes_numpy = y.numpy()
            # plt.scatter(t_numpy[:, 0], t_numpy[:, 1], c=t_classes_numpy, cmap=ListedColormap(['#FF0000', '#00FF00']))
            # plt.subplot(1, 2, 2)
            # t_numpy2 = resortedT.numpy()
            # t_classes_numpy2 = resortedTClasses.numpy()
            # plt.scatter(t_numpy2[:, 0], t_numpy2[:, 1], c=t_classes_numpy2, cmap=ListedColormap(['#FF0000', '#00FF00']))
            # plt.show()

            for rowNum in range(0, (totalRowsCount-1)):
                currentRowValue = resortedT[rowNum, colNum]  #[0][0]
                #nextRowValue = resortedT[rowNum + 1, colNum][0][0]
                currentRowClassValue = resortedTClasses[rowNum]  #[0]
                nextRowClassValue = resortedTClasses[rowNum + 1]  #[0]
                if currentRowClassValue != nextRowClassValue:
                    clustersForGivenNeuron = inputClustersWithBoundaries[colNum]
                    # splitRowNum = self.tensorUtil.keyForValue(clustersForGivenNeuron[:, 0], currentRowValue)[0]
                    # if splitRowNum >= clustersForGivenNeuron.shape[0]:
                    #     tmpSplitValue = MIN_FLOAT_BOUND
                    # else:
                    #     tmpSplitValue = clustersForGivenNeuron[splitRowNum.long().item(), 1][0][0]
                    clustersForGivenNeuronBounds = clustersForGivenNeuron[:, 1]
                    # tmpSplitValue = clustersForGivenNeuronBounds[clustersForGivenNeuronBounds > currentRowValue].max()
                    tmpSplitValue, _ = torch.min(clustersForGivenNeuronBounds[clustersForGivenNeuronBounds < currentRowValue], dim=0)
                    if [tmpSplitValue, colNum] not in estimatedColumnsAndSplits:
                        tmpInfoGain = self.tensorInformationGain(resortedT, resortedTClasses, colNum, tmpSplitValue)
                        estimatedColumnsAndSplits.append([tmpSplitValue, colNum])
                        if tmpInfoGain > maxInfoGain:
                            maxInfoGain = tmpInfoGain
                            columnIndex = colNum
                            splitValue = tmpSplitValue
                            calculationsPerformed = True


        return columnIndex, splitValue, calculationsPerformed

    def findBestSplitFromClusters(self, X, y, inputClustersWithBoundaries):
        # typeUtil.assert (t, "Tensor")
        if 'torch' not in torch.typename(X) or 'Tensor' not in torch.typename(X):
            raise Exception('first param should be torch Tensor!')

        # if self._DEBUG == true:
        #     if (t.size().size()!= 2):
        #          raise Exception("expected 2D tensor, but got " + str(t.size().size()) + "D")

        calculationsPerformed = False
        maxInfoGain = MIN_FLOAT_BOUND
        columnIndex = -1
        splitValue = MIN_FLOAT_BOUND
        columnsCount = X.size()[1]
        totalRowsCount = X.size()[0]
        estimatedColumnsAndSplits = []
        for colNum in range(0, columnsCount):
            column = X[:, colNum].clone()
            sortedCol, indx = column.sort(0, descending=False)  #torch.sort(column, 1)

            # adjust tClasses sorting

            # indexes = indx.index(1, torch.range(1, indx.numel()).long()).long().resize(indx.numel())
            # resortedTClasses = tClasses.index(0, indx)
            if len(y.shape) == 1:
                resortedTClasses = y[indx]
            else:
                resortedTClasses = y[indx, :]
            # resortedT = t.index(0, indx)
            resortedT = X[indx, :]

            # # Visualize our resorting excersises
            # plt.subplot(1, 2, 1)
            # t_numpy = t.numpy()
            # t_classes_numpy = tClasses.numpy()
            # plt.scatter(t_numpy[:, 0], t_numpy[:, 1], c=t_classes_numpy, cmap=ListedColormap(['#FF0000', '#00FF00']))
            # plt.subplot(1, 2, 2)
            # t_numpy2 = resortedT.numpy()
            # t_classes_numpy2 = resortedTClasses.numpy()
            # plt.scatter(t_numpy2[:, 0], t_numpy2[:, 1], c=t_classes_numpy2, cmap=ListedColormap(['#FF0000', '#00FF00']))
            # plt.show()

            for rowNum in range(0, (totalRowsCount-1)):
                currentRowValue = resortedT[rowNum, colNum]  #[0][0]
                #nextRowValue = resortedT[rowNum + 1, colNum][0][0]
                currentRowClassValue = resortedTClasses[rowNum]  #[0]
                nextRowClassValue = resortedTClasses[rowNum + 1]  #[0]
                if currentRowClassValue != nextRowClassValue:
                    clustersForGivenNeuron = inputClustersWithBoundaries[colNum]
                    # splitRowNum = self.tensorUtil.keyForValue(clustersForGivenNeuron[:, 0], currentRowValue)[0]
                    # if splitRowNum >= clustersForGivenNeuron.shape[0]:
                    #     tmpSplitValue = MIN_FLOAT_BOUND
                    # else:
                    #     tmpSplitValue = clustersForGivenNeuron[splitRowNum.long().item(), 1][0][0]
                    clustersForGivenNeuronBounds = clustersForGivenNeuron[:, 1]
                    # tmpSplitValue = clustersForGivenNeuronBounds[clustersForGivenNeuronBounds > currentRowValue].max()
                    # tmpSplitValue, _ = torch.min(clustersForGivenNeuronBounds[clustersForGivenNeuronBounds < currentRowValue], dim=0)
                    boundsSmallerThanCurentRowValue = clustersForGivenNeuronBounds[clustersForGivenNeuronBounds < currentRowValue]
                    if boundsSmallerThanCurentRowValue.shape[0] == 0:
                        tmpSplitValue = MIN_FLOAT_BOUND
                    else:
                        tmpSplitValue, _ = torch.min(boundsSmallerThanCurentRowValue, dim=0)
                    if [tmpSplitValue, colNum] not in estimatedColumnsAndSplits:
                        tmpInfoGain = self.tensorInformationGain(resortedT, resortedTClasses, colNum, tmpSplitValue)
                        estimatedColumnsAndSplits.append([tmpSplitValue, colNum])
                        if tmpInfoGain > maxInfoGain:
                            maxInfoGain = tmpInfoGain
                            columnIndex = colNum
                            splitValue = tmpSplitValue
                            calculationsPerformed = True


        return columnIndex, splitValue, calculationsPerformed

    def findBestSplitFromClusters2(self, X, y, inputClustersWithBoundaries, n_jobs=1):
        # typeUtil.assert (t, "Tensor")
        if 'torch' not in torch.typename(X) or 'Tensor' not in torch.typename(X):
            raise Exception('first param should be torch Tensor!')

        calculationsPerformed = False
        maxInfoGain = MIN_FLOAT_BOUND
        columnIndex = -1
        splitValue = MIN_FLOAT_BOUND
        columnsCount = X.size()[1]
        for colNum in range(0, columnsCount):
            column = X[:, colNum].clone()
            sortedCol, indx = column.sort(0, descending=False)
            # adjust tClasses sorting
            if len(y.shape) == 1:
                resortedTClasses = y[indx]
            else:
                resortedTClasses = y[indx, :]
            resortedT = X[indx, :]

            clustersForGivenNeuronBounds = inputClustersWithBoundaries[colNum][:, 1]
            clustersForGivenNeuronBounds = clustersForGivenNeuronBounds[clustersForGivenNeuronBounds < MAX_FLOAT_BOUND]

            # if n_jobs > 1:
            #     cols_splits_gains = Parallel(n_jobs=n_jobs)(delayed(_tensorInformationGain_wrapper)(resortedT, resortedTClasses, colNum, clustersForGivenNeuronBounds[c_index]) for c_index in range(clustersForGivenNeuronBounds.shape[0]))
            # else:
            cols_splits_gains = [_tensorInformationGain_wrapper(resortedT, resortedTClasses, colNum, clustersForGivenNeuronBounds[c_index]) for c_index in range(clustersForGivenNeuronBounds.shape[0])]

            for col, tmpSplitValue, tmpInfoGain in cols_splits_gains:
                if tmpInfoGain > maxInfoGain:
                    maxInfoGain = tmpInfoGain
                    columnIndex = colNum
                    splitValue = tmpSplitValue
                    calculationsPerformed = True

            # for c_index in range(clustersForGivenNeuronBounds.shape[0]):
            #     tmpSplitValue = clustersForGivenNeuronBounds[c_index]
            #     tmpInfoGain = self.tensorInformationGain(resortedT, resortedTClasses, colNum, tmpSplitValue)
            #     if tmpInfoGain > maxInfoGain:
            #         maxInfoGain = tmpInfoGain
            #         columnIndex = colNum
            #         splitValue = tmpSplitValue
            #         calculationsPerformed = True

        return columnIndex, splitValue, calculationsPerformed


    # Function accepts two tables, one contains data clusters - each row is
    # a tensor with cluster centers.And the other is y_pred clusters
    # (in case we have single neuron there will be single valued tensors) values.
    # Function calculates index of attribute and value between two rows for data split.
    # --------------------------------------------
    # @ param x - Number.
    # @ param min - Old range minimum
    # @ param max - Old range maximum
    # @ param newMin - New range minimum
    # @ param newMax - New range maximum
    # @ return x scaled to new range
    def scaleValue(self, x, oldMin, oldMax, newMin, newMax):
        scalingFactor = (newMax - newMin) / (oldMax - oldMin)
        return scalingFactor * (x - oldMin) + newMin


class RandomForest():
    def __init__(self, x, y, n_trees, n_features, sample_sz, depth=10, min_leaf=5):
        np.random.seed(12)
        if n_features == 'sqrt':
            self.n_features = int(np.sqrt(x.shape[1]))
        elif n_features == 'log2':
            self.n_features = int(np.log2(x.shape[1]))
        else:
            self.n_features = n_features
        print(self.n_features, "sha: ", x.shape[1])
        self.x, self.y, self.sample_sz, self.depth, self.min_leaf = x, y, sample_sz, depth, min_leaf
        self.trees = [self.create_tree() for i in range(n_trees)]

    def create_tree(self):
        idxs = np.random.permutation(len(self.y))[:self.sample_sz]
        f_idxs = np.random.permutation(self.x.shape[1])[:self.n_features]
        return DecisionTree(self.x.iloc[idxs], self.y[idxs], self.n_features, f_idxs,
                            idxs=np.array(range(self.sample_sz)), depth=self.depth, min_leaf=self.min_leaf)

    def predict(self, x):
        return np.mean([t.predict(x) for t in self.trees], axis=0)


def std_agg(cnt, s1, s2): return math.sqrt((s2 / cnt) - (s1 / cnt) ** 2)

class DecisionTree:
    def __init__(self, x, y, n_features, f_idxs, idxs, depth=10, min_leaf=5, inputClustersWithBoundaries=None):
        self.x, self.y, self.idxs, self.min_leaf, self.f_idxs = x, y, idxs, min_leaf, f_idxs
        self.inputClustersWithBoundaries = inputClustersWithBoundaries
        self.depth = depth
        # print(f_idxs)
        #         print(self.depth)
        self.n_features = n_features
        self.n, self.c = len(idxs), x.shape[1]
        self.val = np.mean(y[idxs])
        self.score = float('inf')
        self.find_varsplit()

    def find_varsplit(self):

        for i in self.f_idxs:
            self.find_better_split(i)

        if self.is_leaf:
            return

        x = self.split_col
        lhs = np.nonzero(x <= self.split)[0]
        rhs = np.nonzero(x > self.split)[0]

        if self.inputClustersWithBoundaries is not None:
            # now drop split value from clusters table
            del_indx = np.equal(self.inputClustersWithBoundaries[self.var_idx][:, 1], self.split)
            # adjusted_clusters = np.delete(self.inputClustersWithBoundaries[self.var_idx][:], obj=del_indx, axis=0)
            adjusted_clusters = self.inputClustersWithBoundaries[self.var_idx][~del_indx]
            # now we need to maintain its size - let take last row (which is INF bound) and put its copy at the end
            while adjusted_clusters.shape[0] < self.inputClustersWithBoundaries[self.var_idx].shape[0]:
                adjusted_clusters = np.vstack([adjusted_clusters, adjusted_clusters[-1]])
            self.inputClustersWithBoundaries[self.var_idx] = adjusted_clusters

        lf_idxs = np.random.permutation(self.x.shape[1])[:self.n_features]
        rf_idxs = np.random.permutation(self.x.shape[1])[:self.n_features]
        self.lhs = DecisionTree(self.x, self.y, self.n_features, lf_idxs, self.idxs[lhs], depth=self.depth - 1,
                                min_leaf=self.min_leaf, inputClustersWithBoundaries=self.inputClustersWithBoundaries)
        self.rhs = DecisionTree(self.x, self.y, self.n_features, rf_idxs, self.idxs[rhs], depth=self.depth - 1,
                                min_leaf=self.min_leaf, inputClustersWithBoundaries=self.inputClustersWithBoundaries)

    def find_better_split(self, var_idx):
        x, y = self.x.values[self.idxs, var_idx], self.y[self.idxs]
        sort_idx = np.argsort(x)
        sort_y, sort_x = y[sort_idx], x[sort_idx]
        if np.unique(sort_y).size == 1:
            return
        rhs_cnt, rhs_sum, rhs_sum2 = self.n, sort_y.sum(), (sort_y ** 2).sum()
        lhs_cnt, lhs_sum, lhs_sum2 = 0, 0., 0.

        if self.inputClustersWithBoundaries is not None:
            var_clusters_boundaries = self.inputClustersWithBoundaries[var_idx][:, 1]
            sort_idc = np.argsort(var_clusters_boundaries)
            sort_c = var_clusters_boundaries[sort_idc]
            current_cluster_boundary = float('-inf')

        for i in range(0, self.n - self.min_leaf - 1):
            xi, yi = sort_x[i], sort_y[i]
            lhs_cnt += 1;
            rhs_cnt -= 1
            lhs_sum += yi;
            rhs_sum -= yi
            lhs_sum2 += yi ** 2;
            rhs_sum2 -= yi ** 2
            if i < self.min_leaf or xi == sort_x[i + 1]:
                continue

            split_value = xi
            if self.inputClustersWithBoundaries is not None:
                # pick next boundary bigger than current xi
                new_current_cluster_boundary = sort_c[np.greater(sort_c, xi)][0]
                # if this is new boundary - evaluate split on it
                if new_current_cluster_boundary > current_cluster_boundary and new_current_cluster_boundary < MAX_FLOAT_BOUND:
                    split_value = new_current_cluster_boundary
                    current_cluster_boundary = new_current_cluster_boundary

                    lhs_std = std_agg(lhs_cnt, lhs_sum, lhs_sum2)
                    rhs_std = std_agg(rhs_cnt, rhs_sum, rhs_sum2)
                    curr_score = lhs_std * lhs_cnt + rhs_std * rhs_cnt
                    if curr_score < self.score:
                        self.var_idx, self.score, self.split = var_idx, curr_score, split_value
            else:
                lhs_std = std_agg(lhs_cnt, lhs_sum, lhs_sum2)
                rhs_std = std_agg(rhs_cnt, rhs_sum, rhs_sum2)
                curr_score = lhs_std * lhs_cnt + rhs_std * rhs_cnt
                if curr_score < self.score:
                    self.var_idx, self.score, self.split = var_idx, curr_score, split_value

    @property
    def split_name(self):
        return self.x.columns[self.var_idx]

    @property
    def split_col(self):
        return self.x.values[self.idxs, self.var_idx]

    @property
    def is_leaf(self):
        return self.score == float('inf') or self.depth <= 0

    def predict(self, x):
        return np.array([self.predict_row(xi) for xi in x])

    def predict_row(self, xi):
        if self.is_leaf:
            return self.val
        t = self.lhs if xi[self.var_idx] <= self.split else self.rhs
        return t.predict_row(xi)

    def get_number_of_leafs(self):
        return self.__leafs_number__()

    def __leafs_number__(self):
        if self.is_leaf:
            return 1
        else:
            return self.lhs.__leafs_number__() + self.rhs.__leafs_number__()


class RulesGenerator:

    def __init__(self, runningUtil, mathUtil, tensorUtil):
        self.tensorUtil = tensorUtil
        self.mathUtil = mathUtil
        self.runningUtil = runningUtil

        self.currentLayerToSave = 1
        self.modifiedActivations = {}
        self.modifiedActivationsCoveredMarkers = {}
        self.protoRulesForLayers = {}
        self.protoRuleNodesForLayers = {}

    def addModifiedActivationsForLayer(self, modifiedActivationsForLayer):
        # # if 'torch' not in torch.typename(modifiedActivationsForLayer) or 'Tensor' not in torch.typename(modifiedActivationsForLayer):
        # #     raise Exception('first param should be torch Tensor! typename is = ['+str(torch.typename(modifiedActivationsForLayer))+'], value='+str(modifiedActivationsForLayer))
        # # m = [item for sublist in mod ifiedActivationsForLayer for item in sublist]
        # modifiedActivationsForLayer = torch.stack(modifiedActivationsForLayer)
        self.modifiedActivations[self.currentLayerToSave] = modifiedActivationsForLayer.clone()  # this is what ActivationsRecorder have recorded after ActivationsModiefier clustered modification has taken place
        self.modifiedActivationsCoveredMarkers[self.currentLayerToSave] = torch.Tensor(modifiedActivationsForLayer.shape[1]).zero_()  # this tensor will serve as flags
        self.currentLayerToSave = 1 + self.currentLayerToSave

    def scaleTree(self, root, oldMin, oldMax, newMin, newMax):
        #  typeUtil. assert (modifiedActivationsForLayer, 'Tensor')
        if ('DecisionTreeNode' in torch.typename(root)):
            leftScaledChild = self.scaleTree(root.leftChild, oldMin, oldMax, newMin, newMax)
            rightScaledChild = self.scaleTree(root.rightChild, oldMin, oldMax, newMin, newMax)
            colNum = root.columnNumber
            splitVal = root.splitValue
            scaledSplitVal = self.mathUtil.scaleValue(splitVal, oldMin, oldMax, newMin, newMax)  # we will be using tanh function
            node = DecisionTreeNode(colNum, scaledSplitVal)
            node.setLeft(leftScaledChild)
            node.setRight(rightScaledChild)
            return node
        elif ('DecisionTreeLeaf' in torch.typename(root)):
            rootClassTensor = root.classTensor.clone()
            return DecisionTreeLeaf(classTensor=rootClassTensor, tensorUtil=self.tensorUtil)
        else:
            raise Exception('Unknown node type=['+torch.typename(root)+']')

    # @ param tableOfNodes is regular table containing graph.Node() 's with node.data == ProtoRule. Each ProtoRule have field 'protoRule'
    # @ param protoRule is tensor containing clusters - we will search for it
    def __findNode(self, tableOfNodes, protoRuleTensorToFind):
        if tableOfNodes is not None:
            # for _, node in pairs(tableOfNodes):
            for _, node in tableOfNodes.items():
                protoRuleFromNode = node.data
                if self.tensorUtil.tensorsEqual(protoRuleFromNode.protoRule, protoRuleTensorToFind):
                    return node

        return None

    # Function returns tensor (which denotes class ) that holds most frequent  class
    # as well it returns count of entries of that class and total entries count
    def __getMajorityClass(self, tClasses):
        uniqueRowsTensor, frequencies = self.tensorUtil.rowsFrequencies(tClasses)
        mostFrequentClassCount = 0
        mostFrequentClass = None
        totalClassesCount = 0
        for k, v in frequencies.items():
            if v > mostFrequentClassCount:
                mostFrequentClassCount = v
                mostFrequentClass = tClasses[k]
            totalClassesCount = totalClassesCount + v
        return mostFrequentClass, mostFrequentClassCount, totalClassesCount

    def __getMajorityClass2(self, y):
        uniqueRowsTensor = y.unique()
        mostFrequentClassCount = 0
        mostFrequentClass = None
        totalClassesCount = y.size()[0]
        for i in range(uniqueRowsTensor.size()[0]):
            c = uniqueRowsTensor[i].item()
            v = torch.sum(y == c).item()
            if v > mostFrequentClassCount:
                mostFrequentClassCount = v
                mostFrequentClass = c
            # totalClassesCount = totalClassesCount + v
        return mostFrequentClass, mostFrequentClassCount, totalClassesCount

    def constructTree(self, t, tClasses, inputClustersWithBoundaries, currentDepth, maxDepth):
        mostFrequentClass, mostFrequentClassCount, totalCount = self.__getMajorityClass(tClasses)
        if (mostFrequentClassCount == totalCount):
            # we have single class - let's create node with it
            return DecisionTreeLeaf(classTensor=mostFrequentClass, tensorUtil=self.tensorUtil)
        else:
            if (currentDepth == maxDepth):
                # generate LeafNode with most frequent class
                return DecisionTreeLeaf(classTensor=mostFrequentClass, tensorUtil=self.tensorUtil)
            else:
                colNum, splitValue, calculationsPerformed = self.mathUtil.findBestSplit(t, tClasses, inputClustersWithBoundaries)
                node = DecisionTreeNode(colNum, splitValue)

                leftT, leftTClasses, rightT, rightTClasses = self.mathUtil.splitTables(t, tClasses, colNum, splitValue)
                if len(leftTClasses) == 0:  # (leftTClasses.size().size() == 0):
                    return DecisionTreeLeaf(classTensor=mostFrequentClass, tensorUtil=self.tensorUtil)
                # end
                if len(rightTClasses) == 0:  #(rightTClasses.size().size() == 0):
                    return DecisionTreeLeaf(classTensor=mostFrequentClass, tensorUtil=self.tensorUtil)
                # end
                leftNode = self.constructTree(leftT, leftTClasses, inputClustersWithBoundaries, currentDepth + 1, maxDepth)
                rightNode = self.constructTree(rightT, rightTClasses, inputClustersWithBoundaries, currentDepth + 1, maxDepth)

                node.setLeft(leftNode)
                node.setRight(rightNode)

                return node
    #         end
    #     end
    # end

    def constructTreeFromInputClusters(self, X, y, inputClustersWithBoundaries, currentDepth, maxDepth):
        mostFrequentClass, mostFrequentClassCount, totalCount = self.__getMajorityClass(y)
        if (mostFrequentClassCount == totalCount):
            # we have single class - let's create node with it
            return DecisionTreeLeaf(classTensor=mostFrequentClass, tensorUtil=self.tensorUtil)
        else:
            if (currentDepth == maxDepth):
                # generate LeafNode with most frequent class
                return DecisionTreeLeaf(classTensor=mostFrequentClass, tensorUtil=self.tensorUtil)
            else:
                colNum, splitValue, _ = self.mathUtil.findBestSplitFromClusters(X, y, inputClustersWithBoundaries)
                node = DecisionTreeNode(colNum, splitValue)

                leftT, leftTClasses, rightT, rightTClasses = self.mathUtil.splitTables(X, y, colNum, splitValue)
                if len(leftTClasses) == 0:  # (leftTClasses.size().size() == 0):
                    return DecisionTreeLeaf(classTensor=mostFrequentClass, tensorUtil=self.tensorUtil)
                # end
                if len(rightTClasses) == 0:  #(rightTClasses.size().size() == 0):
                    return DecisionTreeLeaf(classTensor=mostFrequentClass, tensorUtil=self.tensorUtil)
                # end
                leftNode = self.constructTreeFromInputClusters(leftT, leftTClasses, inputClustersWithBoundaries, currentDepth + 1, maxDepth)
                rightNode = self.constructTreeFromInputClusters(rightT, rightTClasses, inputClustersWithBoundaries, currentDepth + 1, maxDepth)

                node.setLeft(leftNode)
                node.setRight(rightNode)

                return node
    #         end
    #     end
    # end

    def constructTreeFromInputClusters2(self, X, y, inputClustersWithBoundaries, currentDepth, maxDepth):
        mostFrequentClass, mostFrequentClassCount, totalCount = self.__getMajorityClass2(y)
        if (mostFrequentClassCount == totalCount):
            # we have single class - let's create node with it
            return DecisionTreeLeaf(classTensor=mostFrequentClass, tensorUtil=self.tensorUtil)
        else:
            if (currentDepth == maxDepth):
                # generate LeafNode with most frequent class
                return DecisionTreeLeaf(classTensor=mostFrequentClass, tensorUtil=self.tensorUtil)
            else:
                colNum, splitValue, _ = self.mathUtil.findBestSplitFromClusters2(X, y, inputClustersWithBoundaries)
                node = DecisionTreeNode(colNum, splitValue)

                leftT, leftTClasses, rightT, rightTClasses = self.mathUtil.splitTables(X, y, colNum, splitValue)
                if len(leftTClasses) == 0:  # (leftTClasses.size().size() == 0):
                    return DecisionTreeLeaf(classTensor=mostFrequentClass, tensorUtil=self.tensorUtil)
                # end
                if len(rightTClasses) == 0:  #(rightTClasses.size().size() == 0):
                    return DecisionTreeLeaf(classTensor=mostFrequentClass, tensorUtil=self.tensorUtil)
                # end
                leftNode = self.constructTreeFromInputClusters2(leftT, leftTClasses, inputClustersWithBoundaries, currentDepth + 1, maxDepth)
                rightNode = self.constructTreeFromInputClusters2(rightT, rightTClasses, inputClustersWithBoundaries, currentDepth + 1, maxDepth)

                node.setLeft(leftNode)
                node.setRight(rightNode)

                return node
    #         end
    #     end
    # end

    def runTree(self, rootNode, value):
        if rootNode.isLeaf():  # not leaf - dig deeper
            return rootNode.classTensor
        else:
            if value[rootNode.columnNumber] < rootNode.splitValue:
                return self.runTree(rootNode.leftChild, value)
            else:
                return self.runTree(rootNode.rightChild, value)

    @timeit
    def generateTree(self, inputClustersWithBoundaries, maxDepth, folder_prefix=None):
        print('Generating tree')

        self.allLayersNodes = {}
        # g = graph.Graph()
        # self.graph = g

        # nextLayerNodes = {}
        # layer = self.currentLayerToSave - 1
        # lastLayerIndx = layer
        # # for all layers starting from last one
        # while (layer > 0):
        #     print('Processing layer = ' + str(layer))
        #
        #     coveredLayerActivations = self.modifiedActivationsCoveredMarkers[layer].clone()  # tensor holding flags for each activations row
        #     layerActivations = self.modifiedActivations[layer].clone()  # clustered neuron activations for given layer
        #     if (layer == lastLayerIndx):
        #         # as we are dealing with ClassNLLCriterion - let's convert last layer outputs to class labels
        #         _, layerActivations = layerActivations.max(2)
        #
        #
        #     layerNodes = {}
        #
        #     # This cycle picks current layer(starting from very last one ) and leaves only unique clustered activation values
        #     # It does so in a smart way - each new row (from activations table) - clustered activation values tuple is considered as a proto rule
        #     # and then any equal(same) activations combination (in subsequent rows) are marked (in a separate table -
        #     # -modifiedActivationsCoveredMarkersForLayer) as covered (data point covered by 'rule')
        #     # such covered proto rules (clustered activation values tuples) are discared from subsequent processing
        #
        #     # We are creating protoRuleNode for graph with activation values - protoRule and covered points count coveredPoints fields of ProtoRule class.
        #     # If Current layer is not last one, then corresponding (same row) activations values from next layer is used
        #     # to find already existing graph node (in nextLayerNodes table) and use it to build edge in the graph
        #     # continue while we have not covered activations
        #
        #     i = 0
        #     while coveredLayerActivations.shape[0] > coveredLayerActivations.sum():
        #         if coveredLayerActivations[i] == 0:
        #             activations = layerActivations[i].lone()
        #             # mark all similar activations and collect their count
        #             coveredLayerActivations[i] = 1
        #             equalActivationsCount = 1
        #             if i < layerActivations.shape[0]:
        #                 for j in range(i+1, layerActivations.shape[1]):
        #                     if coveredLayerActivations[j] == 0 and self.tensorUtil.tensorsEqual(activations, layerActivations[j]):
        #                         equalActivationsCount = equalActivationsCount + 1
        #                         coveredLayerActivations[j] = 1
        #             # create node
        #             premise = ProtoRule(activations, equalActivationsCount)
        #             premiseNode = graph.Node(premise)
        #             layerNodes[self.tableUtil.getSize(layerNodes) + 1] = premiseNode
        #             if (self.tableUtil.getSize(nextLayerNodes) > 0):
        #                 nextLayerActivations = self.modifiedActivations[layer + 1][i].clone()
        #                 if (layer + 1 == lastLayerIndx):
        #                     # as we are dealing with ClassNLLCriterion - let's convert last layer outputs to class labels
        #                     _, nextLayerActivations = nextLayerActivations.max(1)
        #
        #                 consequenceNode = self.findNode(nextLayerNodes, nextLayerActivations)
        #                 g.add(graph.Edge(premiseNode, consequenceNode))
        #         #     end
        #         # end
        #         i = i + 1
        #     # end
        #     self.allLayersNodes[layer] = layerNodes
        #     nextLayerNodes = layerNodes
        #     layer = layer - 1
        # # end
        #
        # #  print('Done with clusterization network building');
        #
        # # for now built graph is not used, what we reallly want is clustered modifiedActivations themselves ( in the input layer )

        for key, t in self.modifiedActivations.items():
            # t = self.modifiedActivations[1]  # first layer activations clustered values
            tClasses = self.modifiedActivations[len(self.modifiedActivations)]  # corresponding last layer clusterd activation values - classes
            # as we are using ClassNLLCriterion - we have several y_pred neurons - the one with maximum value (all of them are < 0) is predicted class
            _, tClasses = tClasses.max(1)  # get indexes of max outputs (per each row - input vector)

            if len(t.shape) == 2 and t.shape[1] == 2:
                # Visualize our resorting excersises
                t_numpy = t.clone().detach().numpy()
                t_classes_numpy = tClasses.numpy()
                plt.scatter(t_numpy[:, 0], t_numpy[:, 1], c=t_classes_numpy, cmap=ListedColormap(['#FF0000', '#00FF00']))

                if not folder_prefix is None:
                    file_name = os.path.join(folder_prefix, "t_tClasses_"+str(key)+"__"+str(time.time())+'.png')
                    plt.savefig(file_name, dpi=None, facecolor='w', edgecolor='w', orientation='portrait')
                else:
                    plt.show()

        rootNode = self.constructTree(t, tClasses, inputClustersWithBoundaries, 1, maxDepth)
        #  print('Done with tree building');
        return rootNode, t, tClasses
        #y = RulesGenerator:runTree(rootNode, torch.Tensor({-0.8, -0.8}))
    # end

    @timeit
    def generateTreeFromInput2(self, inputClustersWithBoundaries, maxDepth, folder_prefix=None, _trainDataLoader=None):
        # Uses DecisionTree class to generate tree.
        # NB! Binary classification only!
        X, y = get_X_y_from_dataloader(_trainDataLoader)

        X = pd.DataFrame(data=X.numpy(), index=range(X.shape[0]), columns=[str(x) for x in range(X.shape[1])])
        Y = y.numpy().reshape(1, y.shape[0])[0]

        rows_num = X.shape[0]
        n_feat = X.shape[1]
        f_idxs = np.array(list(range(n_feat)))
        idxs = np.array(list(range(rows_num)))

        inputClustersWithBoundaries_numpy = None
        if inputClustersWithBoundaries is not None:
            inputClustersWithBoundaries_numpy = inputClustersWithBoundaries.numpy()

        d_tree = DecisionTree(x=X,
                              y=Y,
                              n_features=n_feat,
                              f_idxs=f_idxs,
                              idxs=idxs,
                              depth=maxDepth,
                              min_leaf=5,
                              inputClustersWithBoundaries=inputClustersWithBoundaries_numpy)
        return d_tree


    @timeit
    def generateTreeFromInput(self, inputClustersWithBoundaries, maxDepth, folder_prefix=None, _trainDataLoader=None):
        print('Generating tree')

        if _trainDataLoader is None:
            raise Exception('Empty data loader!')

        self.allLayersNodes = {}

        X, y = get_X_y_from_dataloader(_trainDataLoader)

        rootNode = self.constructTreeFromInputClusters(X, y, inputClustersWithBoundaries, 1, maxDepth)
        print('Done with tree generation')
        return rootNode

    @timeit
    def generateTreeFromInputOld2(self, inputClustersWithBoundaries, maxDepth, folder_prefix=None, _trainDataLoader=None):
        print('Generating tree')

        if _trainDataLoader is None:
            raise Exception('Empty data loader!')

        self.allLayersNodes = {}

        X, y = get_X_y_from_dataloader(_trainDataLoader)

        rootNode = self.constructTreeFromInputClusters2(X, y, inputClustersWithBoundaries, 1, maxDepth)
        print('Done with tree generation')
        return rootNode

    @timeit
    def initialize(self, model, dataset):

        # # firstly let's save non modified activation values and reset recorder layers
        # for recorderLayer in model.modules():
        #     if 'ActivationRecorder' in torch.typename(recorderLayer):
        #         recorderLayer.reset()
        #
        # # BEGIN of activations collection
        # # enable activations collection
        # def enableRecordFlagFunc(layer):
        #     layer.recordFlag = True
        #
        # self.runningUtil.applyToFilteredLayers(model, 'ActivationRecorder', enableRecordFlagFunc)
        # # collect activations
        #
        # model.eval()  # Put the network into evaluation mode
        # for i, (input, target) in enumerate(dataset):
        #     # # Convert torch tensor to Variable
        #     input = Variable(input)
        #     model(input)  # now ActivationRecorder 's will collect activations
        #
        # # disable activations collection
        # def disableRecordFlagFunc(layer):
        #     layer.recordFlag = True
        #
        # self.runningUtil.applyToFilteredLayers(model, 'ActivationRecorder', disableRecordFlagFunc)
        # # END of activations collection

        # Collect all input batches into single X tensor
        # this is not memory friendly, but for now we live with that, as next step will require all inputs.
        inputs_list = []
        for batch_idx, (input, target) in enumerate(dataset):
            inputs_list.append(input)
        X = torch.cat(inputs_list)

        # Initialize quantization layers with inputs (initial clusters tables will be formed from them.
        # modules_list = []
        for key, l in model.model._modules.items():
            layer_type = torch.typename(l)
            if QuantizationLayer.NAME in layer_type:
                X = l(X)
                # save quantized output
                self.addModifiedActivationsForLayer(X)
            else:
                X = l(X)  # Do the forward pass
            # modules_list.append(l)


        # for recorderLayer in model.modules():
        #     if 'ActivationRecorder' in torch.typename(recorderLayer):
        #         # acts = recorderLayer.activations
        #         acts = recorderLayer.get_activations()
        #         self.addModifiedActivationsForLayer(acts)



class DecisionTreeNode:

    def __init__(self, columnNumber, splitValue):
        self.columnNumber = columnNumber
        self.splitValue = splitValue
        self.leftChild = None
        self.rightChild = None

    def setLeft(self, leftChild):
        self.leftChild = leftChild

    def setRight(self, rightChild):
        self.rightChild = rightChild

    def isLeaf(self):
        return False

    def treeStats(self):
        leftStats = self.leftChild.treeStats()
        rightStats = self.rightChild.treeStats()
        depth = max(leftStats.maxDepth, rightStats.maxDepth) + 1
        leafs = leftStats.leafsCount + rightStats.leafsCount
        return DecisionTreeStats(depth, leafs)

    def asString(self):
        return '(col=' + str(self.columnNumber) +  ', splitVal=' + str(self.splitValue) + ')'
        # return '(col=' + str(self.columnNumber) +  ', splitVal=' + str(self.splitValue) + ', left=' + self.leftChild.asString() + ', right=' + self.rightChild.asString()+')'

    # def __tree_print(self, prefix, isTail):
    #     def getTailStr(isTail, yesStr, noStr):
    #         if isTail:
    #             return yesStr
    #         else:
    #             return noStr
    #
    #     if self.rightChild is not None:
    #         self.__tree_print(self.rightChild, prefix + getTailStr(isTail, "│    ", "     "), False)
    #
    #     if torch.typename(self) == 'DecisionTreeNode':
    #         colNum = self.columnNumber or 'n/a'
    #         splitVal = self.splitValue or 'n/a'
    #         print(prefix + getTailStr(isTail, "└─── ", "┌─── ") + '(splitFeature=' + str(colNum) + ', splitValue=' + str(splitVal) + ')')
    #     else: # we assume here is DecisionTreeLeaf
    #         print(prefix + getTailStr(isTail, "└─── ", "┌─── ") + self.asString())
    #     # end
    #     if self.leftChild is not None:
    #         self.__tree_print(self.leftChild, prefix + getTailStr(isTail, "     ", "│    "), True)
    # #     end
    # # end
    #
    # def print(self):
    #     self.__tree_print("", True)

class DecisionTreePrinter:
    def __init__(self):
        pass

    def __tree_print(self, node, prefix, isTail):

        def getTailStr(isTail, yesStr, noStr):
            if isTail:
                return yesStr
            else:
                return noStr

        if 'DecisionTreeNode' in torch.typename(node):
            if node.rightChild is not None:
                self.__tree_print(node.rightChild, prefix + getTailStr(isTail, "│    ", "     "), False)

            colNum = node.columnNumber
            splitVal = node.splitValue
            print(prefix + getTailStr(isTail, "└─── ", "┌─── ") + '(splitFeature=' + str(colNum) + ', splitValue=' + str(splitVal) + ')')

            if node.leftChild is not None:
                self.__tree_print(node.leftChild, prefix + getTailStr(isTail, "     ", "│    "), True)

        elif 'DecisionTreeLeaf' in torch.typename(node):  # we assume here is DecisionTreeLeaf
            print(prefix + getTailStr(isTail, "└─── ", "┌─── ") + node.asString())

        else:
            raise Exception('Unknown node type encountered: ' + torch.typename(node))
    #     end
    # end

    def print(self, rootNode):
        self.__tree_print(rootNode, "", True)

    def __tree_print2(self, node, prefix, isTail):

        def getTailStr(isTail, yesStr, noStr):
            if isTail:
                return yesStr
            else:
                return noStr

        if not node.is_leaf:
            if node.rhs is not None:
                self.__tree_print2(node.rhs, prefix + getTailStr(isTail, "│    ", "     "), False)

            colNum = node.var_idx
            splitVal = node.split
            print(prefix + getTailStr(isTail, "└─── ", "┌─── ") + '(splitFeature=' + str(colNum) + ', splitValue=' + str(splitVal) + ')')

            if node.lhs is not None:
                self.__tree_print2(node.lhs, prefix + getTailStr(isTail, "     ", "│    "), True)

        else:
            # if 'torch' in torch.typename(self.classTensor) and 'Tensor' in torch.typename(self.classTensor):
            #     # if string.find(torch.typename(self.classTensor), "Tensor") is not None:
            #     t = self.classTensor.clone().squeeze()
            #     if 'torch' in torch.typename(t) and 'Tensor' in torch.typename(t):
            #         # if string.find(torch.typename(t), "Tensor") is not None:
            #         tensorStr = "["
            #         if len(t.shape) == 0:
            #             return '(leaf class=' + str(t.item()) + ')'
            #         else:
            #             for i in range(0, t.shape[0]):
            #                 tensorStr = tensorStr + t[i]
            #             tensorStr = tensorStr + ']'
            #             return '(leaf class=' + tensorStr + ')'
            #     else:
            #         # if we had 1D single valued tensor after squeeze it will be a number
            #         return '(leaf class=' + t + ')'
            # else:
            #     return '(leaf class=' + self.classTensor + ')'
            #
            #
            # print(prefix + getTailStr(isTail, "└─── ", "┌─── ") + node.asString())
            print(prefix + getTailStr(isTail, "└─── ", "┌─── ") + '(leaf class=' + str(node.val)+')')

    def print2(self, tree2):
        self.__tree_print2(tree2, "", True)


class DecisionTreeLeaf:

    def __init__(self, classTensor, tensorUtil):
        self.tensorUtil = tensorUtil
        self.classTensor = classTensor

    def equals(self, otherDecisionTreeLeafData):
        return self.tensorUtil.tensorsEqual(self.classTensor, otherDecisionTreeLeafData.classTensor)

    def isLeaf(self):
        return True

    def treeStats(self):
        return DecisionTreeStats(1, 1)

    def asString(self):
        if 'torch' in torch.typename(self.classTensor) and 'Tensor' in torch.typename(self.classTensor):
        # if string.find(torch.typename(self.classTensor), "Tensor") is not None:
            t = self.classTensor.clone().squeeze()
            if 'torch' in torch.typename(t) and 'Tensor' in torch.typename(t):
            # if string.find(torch.typename(t), "Tensor") is not None:
                tensorStr = "["
                if len(t.shape) == 0:
                    return '(leaf class=' + str(t.item()) + ')'
                else:
                    for i in range(0, t.shape[0]):
                        tensorStr = tensorStr + t[i]
                    tensorStr = tensorStr + ']'
                    return '(leaf class=' + tensorStr + ')'
            else:
                # if we had 1D single valued tensor after squeeze it will be a number
                return '(leaf class='+t+')'
        else:
            return '(leaf class='+str(self.classTensor)+')'
    #     end
    # end

    def print(self):
        print(self.asString())


class DecisionTreeStats:

    def __init__(self, maxDepth, leafsCount):
        self.maxDepth = maxDepth
        self.leafsCount = leafsCount

    def __tostring__(self):
        return 'DecisionTreeStats(maxDepth=' + str(self.maxDepth) + '; leafsCount=' + str(self.leafsCount) + ')'


class ProtoRule:

    def __init__(self, protoRule, coveredPoints, tensorUtil):
        self.protoRule = protoRule.clone()
        self.coveredPoints = coveredPoints
        self.tensorUtil = tensorUtil

    def equals(self, otherProtoRule):
        return self.tensorUtil.tensorsEqual(self.protoRule, otherProtoRule)
