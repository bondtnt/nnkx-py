from unittest import TestCase
import pickle
from nnkx import TableUtil
from nnkx import DataUtil
from nnkx import TensorUtil
from nnkx import RunningUtil
from nnkx import MathUtil
from nnkx import RulesGenerator
from nnkx import DecisionTreePrinter
from data_utils import XYDataset
from matplotlib import *
import matplotlib.pyplot as plt
import torch
from matplotlib.colors import ListedColormap
import numpy as np


def visualize_tree_classification_boundary(rulegen, treeRoot, _dataLoader):
    print('Visualizing tree decision boundary')

    h = .02  # step size in the visualization mesh
    cmap_light = ListedColormap(['#FFAAAA', '#AAFFAA', '#AAAAFF'])
    cmap_bold = ListedColormap(['#FF0000', '#00FF00', '#0000FF'])

    # Get mesh evaluation over decision tree
    x_min, x_max = _dataLoader.dataset.X[:, 0].min() - 1, _dataLoader.dataset.X[:, 0].max() + 1
    y_min, y_max = _dataLoader.dataset.X[:, 1].min() - 1, _dataLoader.dataset.X[:, 1].max() + 1
    xx, yy = np.meshgrid(np.arange(x_min, x_max, h), np.arange(y_min, y_max, h))
    XY_mesh = torch.FloatTensor(np.c_[xx.ravel(), yy.ravel()])

    y_pred = []
    for row_indx in range(0, XY_mesh.shape[0]):
        input = XY_mesh[row_indx, :]
        actualOutput = rulegen.runTree(treeRoot, input)
        y_pred.append(actualOutput)

    Z = np.vstack(y_pred).flatten()

    # get (X1, X2, y) for input data - to print dots on scatter plot
    X_train = []
    y_train = []
    for batch_idx, (input, target) in enumerate(_dataLoader):
        X_train.append(input.squeeze(0))
        y_train.append(target.squeeze(0))
    X_train = np.vstack(X_train)
    y_train = np.vstack(y_train).flatten()

    # Put the result into a color plot
    Z = Z.reshape(xx.shape)
    print(Z)
    # plt.figure()
    plt.pcolormesh(xx, yy, Z, cmap=cmap_light)

    # Plot also the training points
    # XY_mesh = XY_mesh.numpuy()
    plt.scatter(X_train[:, 0], X_train[:, 1], c=y_train, cmap=cmap_bold, edgecolor='k', s=20)
    plt.xlim(xx.min(), xx.max())
    plt.ylim(yy.min(), yy.max())
    plt.title("Classification boundary for trained model")

    plt.show()


def visualize_input_clusters_boundaries(inputClustersBoundaries, _dataLoader):
    print('Visualizing tree decision boundary')

    h = .02  # step size in the visualization mesh
    cmap_bold = ListedColormap(['#FF0000', '#00FF00', '#0000FF'])

    x_min, x_max = _dataLoader.dataset.X[:, 0].min() - 1, _dataLoader.dataset.X[:, 0].max() + 1
    y_min, y_max = _dataLoader.dataset.X[:, 1].min() - 1, _dataLoader.dataset.X[:, 1].max() + 1
    xx, yy = np.meshgrid(np.arange(x_min, x_max, h), np.arange(y_min, y_max, h))

    # Display input layer clusters boundaries
    X1_bounds = inputClustersBoundaries[0][:, 1].numpy()
    X2_bounds = inputClustersBoundaries[1][:, 1].numpy()
    plt.scatter(X1_bounds, np.ones(X1_bounds.shape)+0.2, c='black', marker='+')
    plt.scatter(np.ones(X2_bounds.shape)+0.2, X2_bounds, c='black', marker='+')

    # get (X1, X2, y) for input data - to print dots on scatter plot
    X_train = []
    y_train = []
    for batch_idx, (input, target) in enumerate(_dataLoader):
        X_train.append(input.squeeze(0))
        y_train.append(target.squeeze(0))
    X_train = np.vstack(X_train)
    y_train = np.vstack(y_train).flatten()

    # Plot the training points
    # XY_mesh = XY_mesh.numpuy()
    plt.scatter(X_train[:, 0], X_train[:, 1], c=y_train, cmap=cmap_bold, edgecolor='k', s=20)
    plt.xlim(xx.min(), xx.max())
    plt.ylim(yy.min(), yy.max())
    plt.title("Classification boundary for trained model")

    plt.show()


class TestRulesGenerator(TestCase):

    def test_generateTree(self):

        ##############################################
        # Prepare data and objects

        tableUtil = TableUtil()
        dataUtil = DataUtil(tableUtil)
        tensorUtil = TensorUtil(tableUtil)
        runningUtil = RunningUtil(tensorUtil, dataUtil, tableUtil)
        mathUtil = MathUtil(False, tensorUtil)
        rulesGenerator = RulesGenerator(runningUtil, mathUtil, tensorUtil)

        # Save using:
        # with open('filename.pickle', 'wb') as handle:
        #     pickle.dump(a, handle, protocol=pickle.HIGHEST_PROTOCOL)

        with open('inputClustersWithBoundaries2.pickle', 'rb') as handle:
            inputClustersWithBoundaries = pickle.load(handle)

        with open('t2.pickle', 'rb') as handle:
            t = pickle.load(handle)

        with open('tClasses2.pickle', 'rb') as handle:
            tClasses = pickle.load(handle)

        # with open('trainDataLoader.pickle', 'rb') as handle:
        with open('trainDataLoader2.pickle', 'rb') as handle:
            data_loader = pickle.load(handle)

        ##############################################
        # Visualize input clusters boundaries
        visualize_input_clusters_boundaries(inputClustersWithBoundaries, data_loader)

        ##############################################
        # Build decision tree

        # Visualize our t and tClasses
        t_numpy = t.numpy()
        t_classes_numpy = tClasses.numpy()
        plt.scatter(t_numpy[:, 0], t_numpy[:, 1], c=t_classes_numpy, cmap=ListedColormap(['#FF0000', '#00FF00']))
        plt.show()


        treeRoot = rulesGenerator.constructTree(t, tClasses, inputClustersWithBoundaries, currentDepth=1, maxDepth=100)

        ##############################################
        # Inspect acquired decision tree
        DecisionTreePrinter().print(treeRoot)

        print('-------------------------------------')
        # save extracted rules stats
        accuracy_score, f1_score, classification_report, conf_mat = runningUtil.calculateConfusion3(rulesGenerator,
                                                                                                    treeRoot,
                                                                                                    data_loader)
        treeStats = treeRoot.treeStats()
        print('Tree leafs=' + str(treeStats.leafsCount) + ' maxDepth=' + str(treeStats.maxDepth))
        print('Rules Train Confusion matrix: ')
        print(str(conf_mat))
        print('Rules Train accuracy=%.8f, f1=%.8f' % (accuracy_score, f1_score))
        print('Rules Train Classification report: ')
        print(str(classification_report))
        print('-------------------------------------')

        ##############################################
        # Visualize tree
        visualize_tree_classification_boundary(rulesGenerator, treeRoot, data_loader)

        ##############################################
        # We want to have tree with small leafs count - not complex one (and wrong one)
        self.assertTrue(treeStats.maxDepth < 10)
        self.assertTrue(accuracy_score > 0.8)

