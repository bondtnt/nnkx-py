import random
from json import dumps

import numpy as np
import torch
from unittest import TestCase
import pandas as pd

from sklearn.metrics import accuracy_score

from src.nnkx import RandomForest, get_point_neigbourhoods_indices, shift_array


class TestNNKX(TestCase):

    def test_similarity_mat(self):

        def similarity_matrix(mat):
            # get the product x * y
            # here, y = x.t()
            r = torch.mm(mat, mat.t())
            # get the diagonal elements
            diag = r.diag().unsqueeze(0)
            diag = diag.expand_as(r)
            # compute the distance matrix
            D = diag + diag.t() - 2 * r
            return D.sqrt()

        m1 = torch.FloatTensor([[0.0, 0.1, 0.2, 0.4, 0.7, 0.99, 1.0]])
        print(m1)
        pdist1 = torch.nn.functional.pdist(m1, p=2)
        print(pdist1)
        print(similarity_matrix(m1.t()))

    def test_random_forest(self):

        def generate_xor_x_y(num_of_points=100):
            X = [((random.randint(0, 100)) / 100, (random.randint(0, 100)) / 100) for k in range(num_of_points)]
            Xnp = np.asarray(X)
            X = Xnp  #torch.Tensor(X)

            y = []
            for i in range(X.shape[0]):
                # if (X[i][0] > 0 and X[i][1] < 0) or (X[i][0] < 0 and X[i][1] > 0):
                if (X[i][0] > 0.5 and X[i][1] < 0.5) or (X[i][0] < 0.5 and X[i][1] > 0.5):
                    y.append(0)
                else:
                    y.append(1)
            ynp = np.asarray(y).reshape(-1, 1)
            y = ynp  #torch.LongTensor(y)
            return X, y

        x, y = generate_xor_x_y(num_of_points=100)


        X = pd.DataFrame(data=x, index=range(x.shape[0]), columns=[str(x) for x in range(x.shape[1])])
        Y = y.reshape(1, y.shape[0])[0]  #pd.Series(y[0])

        forest = RandomForest(X, Y, n_trees=10, n_features=2, sample_sz=50, depth=10, min_leaf=5)
        y_pred = forest.predict(x)
        y_pred = np.around(y_pred, decimals=0).astype('int64')
        print(accuracy_score(y, y_pred))

    def test_get_point_neigbourhoods_indices(self):
        """
        (point = [0,0], min_bounds = [0,0], max_bounds=[10,10], False) ==>  [[1,0], [0,1]]
        (point = [3,0], min_bounds = [0,0], max_bounds=[10,10], False) ==>  [[2,0], [3,1], [4,0]]
        (point = [3,1], min_bounds = [0,0], max_bounds=[10,10], False) ==>  [[3,0], [4,1], [3,2], [2,1]]
        (point = [10,10], min_bounds = [0,0], max_bounds=[10,10], False) ==>  [[10,9], [9,10]]

        (point = [0,0], min_bounds = [0,0], max_bounds=[10,10], True) ==>  [[1,0], [0,1]]
        (point = [3,0], min_bounds = [0,0], max_bounds=[10,10], True) ==>  [[3,1], [4,0]]
        (point = [3,1], min_bounds = [0,0], max_bounds=[10,10], True) ==>  [[4,1], [3,2]]
        (point = [10,10], min_bounds = [0,0], max_bounds=[10,10], True) ==>  []
        """

        only_increasing_neighbourhoods = False
        neighbourhoods = get_point_neigbourhoods_indices([0, 0], [0, 0], [10, 10], only_increasing_neighbourhoods)
        self.assertEqual(dumps([[1, 0], [0, 1]]), dumps(neighbourhoods))
        neighbourhoods = get_point_neigbourhoods_indices([3, 0], [0, 0], [10, 10], only_increasing_neighbourhoods)
        self.assertEqual(dumps([[2, 0], [4, 0], [3, 1]]), dumps(neighbourhoods))
        neighbourhoods = get_point_neigbourhoods_indices([3, 1], [0, 0], [10, 10], only_increasing_neighbourhoods)
        self.assertEqual(dumps([[2, 1], [4, 1], [3, 0], [3, 2]]), dumps(neighbourhoods))
        neighbourhoods = get_point_neigbourhoods_indices([10, 10], [0, 0], [10, 10], only_increasing_neighbourhoods)
        self.assertEqual(dumps([[9, 10], [10, 9]]), dumps(neighbourhoods))

        only_increasing_neighbourhoods = True
        neighbourhoods = get_point_neigbourhoods_indices([0, 0], [0, 0], [10, 10], only_increasing_neighbourhoods)
        self.assertEqual(dumps([[1, 0], [0, 1]]), dumps(neighbourhoods))
        neighbourhoods = get_point_neigbourhoods_indices([3, 0], [0, 0], [10, 10], only_increasing_neighbourhoods)
        self.assertEqual(dumps([[4, 0], [3, 1]]), dumps(neighbourhoods))
        neighbourhoods = get_point_neigbourhoods_indices([3, 1], [0, 0], [10, 10], only_increasing_neighbourhoods)
        self.assertEqual(dumps([[4, 1], [3, 2]]), dumps(neighbourhoods))
        neighbourhoods = get_point_neigbourhoods_indices([10, 10], [0, 0], [10, 10], only_increasing_neighbourhoods)
        self.assertEqual(dumps([]), dumps(neighbourhoods))

    def test_shift_array(self):
        """
        x = [[1,2],
             [3,4],
             [5,6]]
        shift_array(x, 0, True) ==>>
            [[1,2],
             [1,2],
             [3,4]]
        shift_array(x, 0, False) ==>>
            [[3,4],
             [5,6],
             [5,6]]
        shift_array(x, 1, True) ==>>
            [[1,1],
             [3,3],
             [5,5]]
        """
        x = [[1, 2],
             [3, 4],
             [5, 6]]
        x = np.array(x)

        res = shift_array(x, 0, True)
        self.assertTrue(np.allclose(res, np.array([[1, 2], [1, 2], [3,4]])))

        res = shift_array(x, 0, False)
        self.assertTrue(np.allclose(res, np.array([[3, 4], [5, 6], [5, 6]])))

        res = shift_array(x, 1, True)
        self.assertTrue(np.allclose(res, np.array([[1, 1], [3, 3], [5, 5]])))
