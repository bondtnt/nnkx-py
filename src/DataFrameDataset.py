import torch

from pandas import DataFrame
from torch import Tensor
from torch.utils.data import Dataset
import numpy as np


class DataFrameDataset(Dataset):
    def __init__(self, df: DataFrame, label_col_name: str, target_type_int=False):
        """
        Args:
            df (pandas.DataFrame): dataframe without multiindex
        """
        self.df = df
        self.data_len = len(self.df.index)

        X_df = df.loc[:, df.columns != label_col_name]
        X_df = X_df.astype(np.float32)
        y_df = df.loc[:, label_col_name]
        if target_type_int:
            y_df = y_df.astype(np.int32)

        self.X = torch.Tensor(X_df.values)
        self.y = torch.Tensor(y_df.values)
        if target_type_int:
            self.y = self.y.type(torch.LongTensor)

        del X_df
        del y_df

    def __getitem__(self, index):
        # row = self.df.iloc[index.tolist(), :]
        # X = row.loc[~self.mask]
        # y = row.loc[self.mask]
        X = self.X[index, :]
        y = self.y[index]
        return (X, y)

    def __len__(self):
        return self.data_len
