import copy
import json
import os
import csv
import pandas as pd
from collections import OrderedDict
from pathlib import Path

import torch
import torch.optim as optim
import torch.nn as nn
from datetime import datetime
from nnkx import TableUtil
from nnkx import DataUtil
from nnkx import TensorUtil
from nnkx import ActivationModifierUtil
from nnkx import RunningUtil
from nnkx import MathUtil
from nnkx import ActivationModifier
from nnkx import ActivationRecorder
from nnkx import ActivationsDiscretizer
from nnkx import ModelPingUtil
from nnkx import RulesGenerator
from nnkx import timeit
from nnkx import DecisionTreePrinter
from nnkx import generate_boundary_dataset
from sklearn.metrics import f1_score, confusion_matrix, accuracy_score
from sklearn.model_selection import GridSearchCV, cross_val_score
from torch.autograd import Variable
from torch.utils.data import Dataset
from data_utils import XYDataset
import math
import argparse
import sklearn
import numpy as np
from matplotlib.colors import ListedColormap
import matplotlib.pyplot as plt
import random
import time
import datetime
import psutil

from src.DataFrameDataset import DataFrameDataset
from src.nnkx import SensitivityPruningLogic, NeuronPruning, \
    QuantizationLayer, NeuronPruningModule, MAX_FLOAT_BOUND, MIN_FLOAT_BOUND, get_X_y_from_dataloader, \
    DiscretizeMetricStrategyNormalRules, DiscretizeMetricStrategyCrispRules, DiscretizeMetricStrategyRelaxedRules

tableUtil = TableUtil()
dataUtil = DataUtil(tableUtil)
tensorUtil = TensorUtil(tableUtil)
runningUtil = RunningUtil(tensorUtil, dataUtil, tableUtil)
activationModifierUtil = ActivationModifierUtil(tensorUtil, runningUtil)
mathUtil = MathUtil(False, tensorUtil)
rulesGenerator = RulesGenerator(runningUtil, mathUtil, tensorUtil)

parser = argparse.ArgumentParser(description='PyTorch FC-ANN training / decision tree extraction')
parser.add_argument('--device', default='cpu', help='device assignment ("cpu" or "cuda")')
parser.add_argument('-b', '--batchSize', default='100', help='batch size', type=int)
parser.add_argument('-o', '--optimization', default='SGD', help='optimization ("SGD" | "LBFGS" | etc+.)')
parser.add_argument('-lr', '--learningRate', default='0.001', help='learning rate, for SGD only', type=float)
parser.add_argument('-ld', '--learningRateDecay', default='0.001', help='learning rate, for SGD only', type=float)
parser.add_argument('--epochs', default='300', help='learning rate, for SGD only', type=int)
parser.add_argument('-m', '--momentum', default='0.9', help='momentum, for SGD only', type=float)
parser.add_argument('-e', default='10', help='maximum nb of epochs per single run (fold)', type=int)
parser.add_argument('-i', default='3', help='maximum nb of iterations per batch, for LBFGS', type=int)
parser.add_argument('-coefL1', default='0', help='L1 penalty on the weights', type=float)
parser.add_argument('-coefL2', default='0', help='L2 penalty on the weights', type=float)
parser.add_argument('-t', '--threads', default='4', help='number of threads', type=int)
parser.add_argument('-tZero', default='1', help='start averaging at t0 (ASGD only), in nb of epochs', type=float)
parser.add_argument('--weightDecay', default='0.0', help='weightDecay', type=float)

parser.add_argument('--seed', default='1', help='random seed number', type=int)

parser.add_argument('--dataset_title', default="AdultDataset", help='just title to print in logs', type=str)
parser.add_argument('--dataset_file_name', default="adult_census/adult_featurized_transformed_minmax_v3_0-1_full.data", help='dataset', type=str)
parser.add_argument('--label_column', default="23", help='label column name', type=str)
parser.add_argument('--small_train_set_fraction', default='0.15', help='training set fraction to be used for discretization', type=float)  # out of training set
parser.add_argument('--train_set_fraction', default='0.75', help='training set fraction from whole set', type=float)

parser.add_argument('--hidden_1_size', default='15', help='hiddel layer 1 neurons count', type=int)

parser.add_argument('--afterPruningRetrainEpochs', default='30', help='how many epochs to retrain network after neuron removal', type=int)
parser.add_argument('--maxFallbacks', default='1', help='stop prunning if so many times after neuron removal and retrain accuracy lowered more than `errorWorsenForFallback`', type=int)
parser.add_argument('--errorWorsenForFallback', default='0.01', help='stop pruning if after neuron removal and retrain train accuracy lowers for more than this value', type=float)
#parser.add_argument('--maxFallbacks', default='5', help='stop prunning if so many times after neuron removal and retrain accuracy lowered more than `errorWorsenForFallback`', type=int)
#parser.add_argument('--errorWorsenForFallback', default='0.025', help='stop pruning if after neuron removal and retrain train accuracy lowers for more than this value', type=float)
parser.add_argument('--numberOfNeuronsToPrune', default='1', help='numberOfNeuronsToPrune at a single step', type=int)
parser.add_argument('--max_rules_depth', default='100', help='maximum depth of extracted binary classification decision tree', type=int)

args = parser.parse_args()

# use floats, for SGD
if args.optimization == 'SGD':
    torch.set_default_tensor_type('torch.FloatTensor')

# batch size?
if args.optimization == 'LBFGS' and args.batchSize < 100:
    raise Exception('LBFGS should not be used with small mini-batches; 1000 is recommended')

#save epochs
train_epochs = args.epochs

# fix seed
torch.manual_seed(args.seed)

# threads
# N_JOBS = 8
# torch.set_num_threads(N_JOBS)
# print('<torch> set nb of threads to ' + str(torch.get_num_threads()))
# threads
torch.set_num_threads(args.threads)
print('<torch> set nb of threads to ' + str(torch.get_num_threads()))

import psutil


def main():
    process = psutil.Process(os.getpid())

    # use floats, for SGD
    if args.optimization == 'SGD':
        torch.set_default_tensor_type('torch.FloatTensor')

    # batch size?
    if args.optimization == 'LBFGS' and args.batchSize < 100:
        raise Exception('LBFGS should not be used with small mini-batches; 1000 is recommed')

    originalMin = MAX_FLOAT_BOUND
    originalMax = MIN_FLOAT_BOUND
    newMin = 0
    newMax = 1

    class EmptyClass():
        def __init__(self):
            pass


    ### DATASET #################################
    # time = datetime.now()
    # timestamp = time.strftime('%Y-%m-%d_%H:%M:%S')
    print(args.dataset_title)

    labels2TargetsTe = [torch.FloatTensor([1, -1]), torch.FloatTensor([-1, 1])]
    labels2TargetsTr = [torch.FloatTensor([1, -1]), torch.FloatTensor([-1, 1])]
    classesNumber = 2

    project_dir = Path(__file__).resolve().parents[1]
    featurized_data_path = os.path.join(project_dir, "data", args.dataset_file_name)
    df = pd.read_csv(featurized_data_path, index_col=0)
    df[args.label_column] = df[args.label_column].astype(np.int64)
    print('Unique classes:')
    print(df[args.label_column].unique())

    full_dataset = DataFrameDataset(df, args.label_column, target_type_int=True)

    train_size = int(args.train_set_fraction * len(full_dataset))
    test_size = len(full_dataset) - train_size
    train_dataset, test_dataset = torch.utils.data.random_split(full_dataset, [train_size, test_size])

    # small train dataset is needed to speedup clusterization of neurons outputs
    small_train_size = int(args.small_train_set_fraction * len(train_dataset))
    small_test_size =len(train_dataset) - small_train_size
    small_train_dataset, _ = torch.utils.data.random_split(train_dataset, [small_train_size, small_test_size])

    # ##############################################################################################
    # # DECISION TREE CLASSIFIER TEST
    X, y = get_X_y_from_dataloader(torch.utils.data.DataLoader(dataset=train_dataset, batch_size=args.batchSize, shuffle=True))
    X, y = X.numpy(), y.numpy()

    # parameters = {'max_depth': range(2, 12)}
    # clf = GridSearchCV(sklearn.tree.DecisionTreeClassifier(), parameters, n_jobs=8, cv=10)
    # clf.fit(X=X, y=y)
    # tree_model = clf.best_estimator_
    # print(clf.best_score_, clf.best_params_)
    #
    # Xtst, ytst = get_X_y_from_dataloader(torch.utils.data.DataLoader(dataset=test_dataset, batch_size=args.batchSize, shuffle=True))
    # Xtst, ytst = Xtst.numpy(), ytst.numpy()
    #
    # print('Decision tree train:')
    # y_pred = tree_model.predict(X)
    # f1 = f1_score(y, y_pred, average='macro')
    # cm = confusion_matrix(y, y_pred)
    # print('Cross validation score:')
    # print(cross_val_score(clf, X, y, cv=10))
    # print('Accuracy:')
    # print(accuracy_score(y, y_pred))
    # print('F1:')
    # print(f1)
    # print('Confusion matrix:')
    # print(cm)
    #
    # print('Decision tree test:')
    # y_pred2 = tree_model.predict(Xtst)
    # f1_2 = f1_score(ytst, y_pred2, average='macro')
    # cm = confusion_matrix(ytst, y_pred2)
    # print(cross_val_score(clf, Xtst, ytst, cv=10))
    # print('Accuracy:')
    # print(accuracy_score(ytst, y_pred2))
    # print('F1:')
    # print(f1_2)
    # print('Confusion matrix:')
    # print(cm)
    # # # END OF DECISION TREE CLASSIFIER TEST
    # # ##############################################################################################

    classNames = {}
    for i in range(0, classesNumber):
      classNames[i] = ''+str(i)


    # ANN parameters
    inputsSize = full_dataset.X.size()[1]
    outputsSize = classesNumber # For ClassNNLCriterion - there must be classesNumber y_pred neurons.
    hidden1Size = args.hidden_1_size  #20
    # hidden2Size = 6

    class Net(nn.Module):

        def __init__(self, num_features, hidden1_units):
            super().__init__()
            self.num_features = num_features
            self.hidden1_units = hidden1_units
            # self.hidden2_units = hidden2_units
            self.model = nn.Sequential(
                QuantizationLayer(),
                NeuronPruningModule('relu', num_features),
                nn.Linear(num_features, hidden1_units),
                nn.ReLU(),
                nn.BatchNorm1d(hidden1_units),
                NeuronPruningModule('relu', hidden1_units),
                nn.Linear(hidden1_units, outputsSize),
                nn.LogSoftmax(),
            )
            for m in self.model:
                if isinstance(m, nn.Linear):
                    nn.init.kaiming_normal_(m.weight)
                    nn.init.constant_(m.bias, 0)

        def forward(self, input_tensor):
            x = self.model(input_tensor)
            return x

        def clone(self):
            model_copy = type(self)(num_features=self.num_features,
                                    hidden1_units=self.hidden1_units)  # get a new instance
            model_copy.load_state_dict(self.state_dict())  # copy weights and stuff
            return model_copy

        def get_state(self, optimizer):
            state = {
                'state_dict': self.state_dict(),
                'optimizer': optimizer.state_dict(),
            }
            return state

    def model_builder_func():
        return Net(num_features=inputsSize,
                   hidden1_units=hidden1Size)


    #=====================================================
    # criterion = nn.MSELoss()
    criterion = nn.CrossEntropyLoss()
    #criterion = nn.BCECriterion()
    #criterion = nn.MarginCriterion()
    #criterion = nn.SoftMarginCriterion()
    #criterion = nn.ClassNLLCriterion()

    verbose = True
    saveModels = False
    i = 1
    results = {}

    # # log results to files
    # trainLogger = optim.Logger(paths.concat('logs', dataSetName+'_'+timestamp+'train.log'))
    # testLogger = optim.Logger(paths.concat('logs', dataSetName+'_'+timestamp+'test.log'))
    # #prunningTrainLogger = optim.Logger(paths.concat('logs', dataSetName+'_'+timestamp+'prunedTrain.log'))
    # #prunningTestLogger = optim.Logger(paths.concat('logs', dataSetName+'_'+timestamp+'prunedTest.log'))
    # #prunedNeuronsCount = optim.Logger(paths.concat('logs', dataSetName+'_'+timestamp+'prunedNeuronsCount.log'))
    # rulesTrainLogger = optim.Logger(paths.concat('logs', dataSetName+'_'+timestamp+'rulesTrain.log'))
    # rulesTestLogger = optim.Logger(paths.concat('logs', dataSetName+'_'+timestamp+'rulesTest.log'))
    # rulesTestDepthLogger = optim.Logger(paths.concat('logs', dataSetName+'_'+timestamp+'rulesTestDepth.log'))
    # rulesTestLeafsLogger = optim.Logger(paths.concat('logs', dataSetName+'_'+timestamp+'rulesTestLeafs.log'))

    xValLogs = EmptyClass()
    xValLogs.trainConfusion = {}
    xValLogs.testConfusion = {}
    xValLogs.pruningTrainConfusion = []
    xValLogs.pruningTestConfusion = []
    xValLogs.rulesTrainConfusion = []
    xValLogs.rulesTestConfusion = []
    xValLogs.pruningLogs = []

    pruningConfig = EmptyClass()
    pruningConfig.afterPruningRetrainEpochs = args.afterPruningRetrainEpochs
    pruningConfig.maxFallbacks = args.maxFallbacks
    pruningConfig.errorWorsenForFallback = args.errorWorsenForFallback
    pruningConfig.numberOfNeuronsToPrune = args.numberOfNeuronsToPrune  # Number of neurons to prune at single step
    pruningConfig.maxNodesToPrune = hidden1Size + inputsSize
    pruningConfig.maxPruningIterations = inputsSize + hidden1Size + pruningConfig.maxFallbacks * 3 #20
    pruningConfig.classNames = classNames
    pruningConfig.verbose = 1


    time_stamp = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d_%H%M%S')

    folder_prefix = os.path.join(os.path.sep, os.getcwd(), time_stamp)
    if not os.path.exists(folder_prefix):
        os.makedirs(folder_prefix)

    with open(os.path.join(os.path.sep, os.getcwd(), time_stamp, args.dataset_title+'parameters.txt'), 'w') as f:
        dict = vars(args)
        json.dump(dict, indent=4, sort_keys=True, fp=f)


    def optimizer_builder_func(_model):
        # return optim.Adamax(_model.parameters(), betas=(0.9, 0.999), lr=1e-4, weight_decay=1e-4)
        # return optim.LBFGS(_model.parameters(), lr=0.01)
        # return optim.Adam(_model.parameters())
        # return optim.RMSprop(_model.parameters(), lr=1e-4)
        return optim.SGD(_model.parameters(), lr=args.learningRate, momentum=args.momentum)


    def train_test_func(model, _classNames, _trainDataLoader, _smallTrainDataLoader, _testDataLoader, xval_iteration=1):

        log_dict = {}

        runningUtil = RunningUtil(tensorUtil, dataUtil, tableUtil)
        activationModifierUtil = ActivationModifierUtil(tensorUtil, runningUtil)

        folder_prefix = os.path.join(os.path.sep, os.getcwd(), time_stamp, str(xval_iteration))
        if not os.path.exists(folder_prefix):
            os.makedirs(folder_prefix)

        # calc / save stats
        args.epochs = train_epochs
        loss_history_train, accuracy_history_train = runningUtil.train(model, criterion, optimizer_builder_func,
                                                                       _trainDataLoader, args, epochs_to_print=10)

        log_dict['loss_history_train'] = loss_history_train
        log_dict['accuracy_history_train'] = accuracy_history_train

        network_train_acc = runningUtil.test(model, _trainDataLoader, criterion, args.batchSize)
        network_test_acc = runningUtil.test(model, _testDataLoader, criterion, args.batchSize)
        print('Trained model TRAIN_ACCURACY: ' + str(network_train_acc))
        print('Trained model TEST_ACCURACY: ' + str(network_test_acc))
        log_dict['network_train_acc'] = network_train_acc
        log_dict['network_test_acc'] = network_test_acc

        # SAVING MODEL
        PATH = os.path.join(folder_prefix, 'TRAINED_MODEL' + str(time.time()) + '.model')
        torch.save(model.state_dict(), PATH)
        log_dict['network_model'] = PATH

        # we will run two prunings first with low settings (1 fallback and half-percent error worsening and second more aggressive
        max_fallbacks_list = [1, 5]
        error_worsen_list = [0.01, 0.025]

        log_dict['prunings'] = []

        for i in range(len(max_fallbacks_list)):

            pruningConfig.maxFallbacks = max_fallbacks_list[i]
            pruningConfig.errorWorsenForFallback = error_worsen_list[i]

            pruning_log_dict = {}
            pruning_log_dict['maxFallbacks'] = pruningConfig.maxFallbacks
            pruning_log_dict['errorWorsenForFallback'] = pruningConfig.errorWorsenForFallback

            # pruning -----------------------------------------------------------------------------------------------------
            # -------------------------------------------------------------------------------------------------------------
            sensPruningLogic = SensitivityPruningLogic(dataUtil)
            neuronPruning1 = NeuronPruning(sensPruningLogic, dataUtil, runningUtil)
            sensPruningLog, prunedModel = neuronPruning1.prune_module_neurons(pruningConfig, _trainDataLoader,
                                                                              copy.deepcopy(model), criterion, args,
                                                                              optimizer_builder_func)

            network_pruned_train_acc = runningUtil.test(prunedModel, _trainDataLoader, criterion, args.batchSize)
            network_pruned_test_acc = runningUtil.test(prunedModel, _testDataLoader, criterion, args.batchSize)
            print('Pruned model TRAIN_ACCURACY: ' + str(network_pruned_train_acc))
            print('Pruned model TEST_ACCURACY: ' + str(network_pruned_test_acc))
            pruning_log_dict['network_pruned_train_acc'] = network_pruned_train_acc
            pruning_log_dict['network_pruned_test_acc'] = network_pruned_test_acc

            # SAVING MODEL
            PATH = os.path.join(folder_prefix, 'PRUNED_MODEL' + str(time.time()) + '.model')
            torch.save(prunedModel.state_dict(), PATH)
            pruning_log_dict['network_pruned_model'] = PATH

            _model = prunedModel

            # PATH = os.path.join(os.path.sep, os.getcwd(), '2019-05-07_133237', '1', 'PRUNED_MODEL1557226358.7817602.model')
            # model = model_builder_func()
            # model.load_state_dict(torch.load(PATH))
            # model.eval()
            # _model = model

            # neuron outputs clusterization -------------------------------------------------------------------------------
            # -------------------------------------------------------------------------------------------------------------

            # def confusionCalcFuncTr(_model):
            #     def error_score(y_true, y_pred):
            #         # return 1 - sklearn.metrics.accuracy_score(y_true, y_pred)
            #         return 1 - sklearn.metrics.f1_score(y_true, y_pred)
            #
            #     # return runningUtil.calculateMetric(_model, _trainDataLoader, error_score)
            #     return runningUtil.calculateMetric(_model, _trainDataLoader, sklearn.metrics.f1_score)


            def confusionCalcFuncTr(_model):
                # data_loader = _trainDataLoader
                # def error_score():
                #     _model.train(False)
                #
                #     items_total = 0
                #
                #     epoch_accuracies = []
                #     epoch_f1_scores = []
                #     epoch_losses = []
                #
                #     for batch_idx, (input, target) in enumerate(data_loader):
                #
                #         # # Convert torch tensor to Variable
                #         input = Variable(input)
                #         target = Variable(target)
                #
                #           # Put the network into training mode
                #         outputs = _model(input)  # Do the forward pass
                #         _target = target.data
                #         if len(target.shape) > 1:  # if batch_size == 1
                #             _target = target.data.squeeze(-1)
                #         loss = criterion(outputs, _target)  # Calculate the loss
                #
                #         # Record the correct predictions for training data
                #         items_total += target.size(0)
                #         _, predicted = torch.max(outputs.data, 1)
                #
                #         # conf_mat = sklearn.metrics.confusion_matrix(target.data.squeeze(-1), predicted, labels=None, sample_weight=None)
                #
                #         epoch_losses.append(loss.item())
                #         # accuracy_score = sklearn.metrics.accuracy_score(_target, predicted)
                #         # epoch_accuracies.append(accuracy_score)
                #         # f1_score = sklearn.metrics.f1_score(target.data, predicted)
                #         # epoch_f1_scores.append(f1_score)
                #         # # classification_report = sklearn.metrics.classification_report(target.data.squeeze(-1), predicted)
                #
                #     # Book keeping
                #     # Record the loss & metrics
                #     full_epoch_loss = np.asarray(epoch_losses).sum()
                #     # full_epoch_accuracy = np.asarray(epoch_accuracies).mean()
                #     # full_f1_score = np.asarray(epoch_f1_scores).mean()
                #
                #     # return full_epoch_loss, full_epoch_accuracy, full_f1_score
                #     return 100 - full_epoch_loss
                # return error_score()

                def error_score(y_true, y_pred):
                    return sklearn.metrics.f1_score(y_true, y_pred, average='weighted')

                # return runningUtil.calculateMetric(_model, _trainDataLoader, sklearn.metrics.f1_score)
                return runningUtil.calculateMetric(_model, _trainDataLoader, error_score)

            modelPingUtil = ModelPingUtil()
            discretizer = ActivationsDiscretizer(runningUtil, tableUtil, modelPingUtil, activationModifierUtil)


            list_of_metric_strategies = [DiscretizeMetricStrategyCrispRules(),
                                         DiscretizeMetricStrategyRelaxedRules(0.99)]
            list_discretization_stats = []
            for metric_strategy_f in list_of_metric_strategies:

                __model = copy.deepcopy(_model)

                print("##############################################################################")
                print("##############################################################################")
                print(str(metric_strategy_f))
                print("------------------------------------------------------------------------------")
                print('Memory consumption is ' + str(process.memory_info().rss / 1024 / 1024) + 'Mbs')  # in Mbytes
                # visualizer_args = [_model, _trainDataLoader, folder_prefix, 'ann_pruned_discretized_']
                # discretizer.clusterize_discretize(_model, confusionCalcFuncTr, _smallTrainDataLoader, visualize_classification_boundary, visualizer_args)

                # # metric_strategy_f = DiscretizeMetricStrategyNormalRules()
                # metric_strategy_f = DiscretizeMetricStrategyCrispRules()
                # # metric_strategy_f = DiscretizeMetricStrategyRelaxedRules(0.9999)
                # # metric_strategy_f = DiscretizeMetricStrategyRelaxedRules(0.9997)
                # # metric_strategy_f = DiscretizeMetricStrategyRelaxedRules(0.9995)
                # discretizer.clusterize_discretize(__model, confusionCalcFuncTr, _smallTrainDataLoader, None, None, metric_strategy_f, quantization_epsilon=1000000)
                discretizer.clusterize_discretize(__model, confusionCalcFuncTr, _smallTrainDataLoader, None, None, metric_strategy_f, quantization_epsilon=1000)
                print('Recording activations DONE.')
                print('Memory consumption is ' + str(process.memory_info().rss / 1024 / 1024) + 'Mbs')  # in Mbytes

                discretized_model_train_acc = runningUtil.test(__model, _trainDataLoader, criterion, args.batchSize)
                discretized_model_test_acc = runningUtil.test(__model, _testDataLoader, criterion, args.batchSize)
                print('Discretized/Clusterized model TRAIN_ACCURACY: ' + str(discretized_model_train_acc))
                print('Discretized/Clusterized model TEST_ACCURACY: ' + str(discretized_model_test_acc))

                # # SAVE DISCRETIZED/CLUSTERIZED MODEL -- TBD ->> saves not full info (loading fails)
                # PATH = os.path.join(folder_prefix, 'DISCRETIZED_CLUSTERIZED_MODEL' + str(time.time()) + '.model')
                # torch.save(_model.state_dict(), PATH)
                #
                # PATH = os.path.join(os.path.sep, os.getcwd(), '2019-05-17_011507', '0', 'DISCRETIZED_CLUSTERIZED_MODEL1558044916.9616659.model')
                # model = model_builder_func()
                # model.load_state_dict(torch.load(PATH))
                # model.eval()
                # _model = model

                # extract rules
                maxDepth = args.max_rules_depth

                # get clustersTable from (input) first QuantizationLayer
                inputClustersWithBoundaries = None
                for key, l in __model.model._modules.items():
                    layer_type = torch.typename(l)
                    if QuantizationLayer.NAME in layer_type:
                        inputClustersWithBoundaries = l.clusters_table.data
                        break
                if inputClustersWithBoundaries is None:
                    raise Exception(
                        'ClustersTable was not collected from QuantizationLayer layers (do you have at least one?)!')

                # Now generate dataset following classification boundary
                X_bound, y_bound, boundaryDataset = generate_boundary_dataset(__model, inputClustersWithBoundaries)
                bound_data_set = torch.utils.data.DataLoader(dataset=XYDataset(X_bound, y_bound), batch_size=args.batchSize, shuffle=True)

                # extract_knowledge(__model, _testDataLoader, _trainDataLoader, folder_prefix, inputClustersWithBoundaries, maxDepth)
                discretization_stats = extract_knowledge(__model, _testDataLoader, _trainDataLoader, bound_data_set, folder_prefix, str(metric_strategy_f), inputClustersWithBoundaries, maxDepth)
                discretization_stats['discretized_model_train_acc'] = discretized_model_train_acc
                discretization_stats['discretized_model_test_acc'] = discretized_model_test_acc
                discretization_stats['discretization_strategy'] = str(metric_strategy_f)

                list_discretization_stats.append(discretization_stats)

            pruning_log_dict['list_discretization_stats'] = list_discretization_stats
            log_dict['prunings'].append(pruning_log_dict)

        print('Iteration ' + str(xval_iteration) + ' finished')
        xval_iteration = xval_iteration + 1
        return log_dict

    def extract_knowledge(_model, _testDataLoader, _trainDataLoader, bound_data_set, folder_prefix, file_name_prefix_ppostfix, input_clusters, maxDepth):
        stats = {}
        runningUtil = RunningUtil(tensorUtil, dataUtil, tableUtil)
        mathUtil = MathUtil(False, tensorUtil)
        rulegen = RulesGenerator(runningUtil, mathUtil, tensorUtil)
        rulegen.initialize(_model, _trainDataLoader)

        # ###############################
        # # New Tree Generation routines
        # tree2 = rulegen.generateTreeFromInput2(input_clusters.clone(), maxDepth, folder_prefix, _trainDataLoader)
        # print_new_tree_stats(_testDataLoader, _trainDataLoader, folder_prefix, tree2)
        # visualize_tree2_classification_boundary(tree2, _trainDataLoader, folder_prefix, 'tree2',
        #                                         input_clusters.clone())
        # ########
        # tree2orig = rulegen.generateTreeFromInput2(None, maxDepth, folder_prefix, _trainDataLoader)
        # print_new_tree_stats(_testDataLoader, _trainDataLoader, folder_prefix, tree2orig)
        # visualize_tree2_classification_boundary(tree2orig, _trainDataLoader, folder_prefix, 'tree2Orig',
        #                                         input_clusters.clone())
        ###############################

        ###############################
        # Old tree generation routines
        print('GenerateTree... Memory consumption is ' + str(process.memory_info().rss / 1024 / 1024) + 'Mbs')

        # rootNode = rulegen.generateTreeFromInput(input_clusters.clone(), maxDepth, folder_prefix, _trainDataLoader)

        rootNodeTensors = rulegen.generateTreeFromInputOld2(input_clusters.clone(), maxDepth, folder_prefix,
                                                            bound_data_set)
        print('GenerateTree DONE. Memory consumption is ' + str(process.memory_info().rss / 1024 / 1024) + 'Mbs')

        # print('rootNode=>\n')
        # DecisionTreePrinter().print(rootNode)
        # treeStats = rootNode.treeStats()
        # print('Tree leafs=' + str(treeStats.leafsCount) + ' maxDepth=' + str(treeStats.maxDepth))
        print('rootNodeTensors=>\n')
        DecisionTreePrinter().print(rootNodeTensors)
        treeStatsOld2 = rootNodeTensors.treeStats()
        print('treeOldTensors leafs=' + str(treeStatsOld2.leafsCount) + ' maxDepth=' + str(treeStatsOld2.maxDepth))
        # print('########################')
        # print_tree_stats(_trainDataLoader, 'Train', rootNode, rulegen, runningUtil)
        # print_tree_stats(_testDataLoader, 'Test', rootNode, rulegen, runningUtil)
        print('########################')
        accuracy_score_te, f1_score_te, classification_report_te, conf_mat_te = print_tree_stats(_trainDataLoader, 'TrainTreeTensors', rootNodeTensors, rulegen, runningUtil)
        accuracy_score_tr, f1_score_tr, classification_report_tr, conf_mat_tr = print_tree_stats(_testDataLoader, 'TestTreeTensors', rootNodeTensors, rulegen, runningUtil)
        print('########################')

        stats['tree.leafs'] = treeStatsOld2.leafsCount
        stats['tree.max_depth'] = treeStatsOld2.maxDepth
        stats['tree.accuracy_score_te'] = accuracy_score_te
        stats['tree.f1_score_te'] = f1_score_te
        stats['tree.accuracy_score_tr'] = accuracy_score_tr
        stats['tree.f1_score_tr'] = f1_score_tr
        return stats

    def print_new_tree_stats(_testDataLoader, _trainDataLoader, folder_prefix, tree2):
        print("Tree2 leafs #: " + str(tree2.get_number_of_leafs()))
        test_acc_score = get_tree2_acc(_testDataLoader, tree2)
        print("Tree2 test accuracy::" + str(test_acc_score))
        train_acc_score = get_tree2_acc(_trainDataLoader, tree2)
        print("Tree2 train accuracy::" + str(train_acc_score))
        DecisionTreePrinter().print2(tree2)

    def print_tree_stats(_trainDataLoader, dataset_name, rootNode, rulegen, runningUtil):
        print('-------------------------------------')
        # save extracted rules stats
        accuracy_score, f1_score, classification_report, conf_mat = runningUtil.calculateConfusion3(rulegen, rootNode,
                                                                                                    _trainDataLoader)
        print('Rules ' + dataset_name + ' Confusion matrix: ')
        print(str(conf_mat))
        print('Rules ' + dataset_name + ' accuracy=%.8f, f1_train=%.8f' % (accuracy_score, f1_score))
        print('Rules ' + dataset_name + ' Classification report: ')
        print(str(classification_report))
        print('-------------------------------------')

        return accuracy_score, f1_score, classification_report, conf_mat

    def get_tree2_acc(_testDataLoader, tree2):
        Xx, yy = get_X_y_from_dataloader(_testDataLoader)
        # Xx = pd.DataFrame(data=Xx.numpy(), index=range(Xx.shape[0]), columns=[str(x) for x in range(Xx.shape[1])])
        Yy = yy.numpy().reshape(1, yy.shape[0])[0]
        yy_pred = tree2.predict(Xx.numpy())
        yy_pred = np.around(yy_pred, decimals=0).astype('int64')
        acc_score = sklearn.metrics.accuracy_score(Yy, yy_pred)
        return acc_score

    #   trainLogger.add{['% mean class accuracy (train set)'] = trainConf.totalValid * 100}
    #   testLogger.add{['% mean class accuracy (test set)'] = testConf.totalValid * 100}
    # #  prunningTrainLogger.add{['% mean class accuracy (pruned train set)'] = pruningTrainConf.totalValid * 100}
    # #  prunningTestLogger.add{['% mean class accuracy (pruned test set)'] = pruningTestConf.totalValid * 100}
    # #  prunedNeuronsCount.add{['% pruning Log (total pruned neurons counts)'] = sensPruningLog.prunedNeurons[#sensPruningLog.prunedNeurons].total}
    #   rulesTrainLogger.add{['% mean class accuracy (rules train set)'] = rulesTrainConf.totalValid * 100}
    #   rulesTestLogger.add{['% mean class accuracy (rules test set)'] = rulesTestConf.totalValid * 100}
    #   rulesTestDepthLogger.add{['% rules maxDepth'] = treeStats.maxDepth}
    #   rulesTestLeafsLogger.add{['% rules count'] = treeStats.maxDepth}

    #   if True
    #     trainLogger.style{['% mean class accuracy (train set)'] = '-'}
    #     testLogger.style{['% mean class accuracy (test set)'] = '+'}
    # #    prunningTrainLogger.style{['% mean class accuracy (pruned train set)'] = '-'}
    # #    prunningTestLogger.style{['% mean class accuracy (pruned test set)'] = '+'}
    # #    prunedNeuronsCount.style{['% pruning Log (pruned neurons counts)'] = '-' }
    #     rulesTrainLogger.style{['% mean class accuracy (rules train set)'] = '-'}
    #     rulesTestLogger.style{['% mean class accuracy (rules test set)'] = '+'}
    #     rulesTestDepthLogger.style{['% mean class accuracy (rules test set)'] = '-'}
    #     rulesTestLeafsLogger.style{['% mean class accuracy (rules test set)'] = '-'}
    #
    #     trainLogger.plot()
    #     testLogger.plot()
    # #    prunningTrainLogger.plot()
    # #    prunningTestLogger.plot()
    # #    prunedNeuronsCount.plot()
    #     rulesTrainLogger.plot()
    #     rulesTestLogger.plot()
    #     rulesTestDepthLogger.plot()
    #     rulesTestLeafsLogger.plot()


    # ####################################################################################################################
    # # TRAIN TEST
    # trainTestTrainer = EmptyClass()
    # trainTestTrainer.criterion = criterion
    # trainTestTrainer.classNames = classNames
    # trainTestTrainer.trainDataLoader = torch.utils.data.DataLoader(dataset=train_dataset, batch_size=args.batchSize,
    #                                                                shuffle=True)
    # trainTestTrainer.smallTrainDataLoader = torch.utils.data.DataLoader(dataset=small_train_dataset,
    #                                                                     batch_size=args.batchSize, shuffle=True)
    # trainTestTrainer.testDataLoader = torch.utils.data.DataLoader(dataset=test_dataset, batch_size=args.batchSize,
    #                                                               shuffle=True)
    # trainTestTrainer.saveModels = False  # if true all trained models will be saved
    # trainTestTrainer.verbose = False  # will print accuracy if true
    # trainTestTrainer.trainTest = None
    # trainTestTrainer.trainTest = train_test_func
    #
    # runningUtil.runTestTrain(trainTestTrainer, 2, model_builder_func, optimizer_builder_func)
    # ####################################################################################################################

    ####################################################################################################################
    # XVAL
    xValRunner = EmptyClass()
    xValRunner.classNames = classNames
    xValRunner.batchSize = args.batchSize
    xValRunner.saveModels = False  # if true all trained models will be saved
    xValRunner.verbose = False  # will print accuracy if true
    # xValRunner.criterion = criterion
    # xValRunner.trainTest = None
    # xValRunner.trainTest = train_test_func
    xValRunner.trainTest = train_test_func

    # runningUtil.runTestTrain(trainTestTrainer, 2, model_builder_func, optimizer_builder_func)
    # runningUtil.runXVal(xValRunner, 2, model_builder_func, optimizer_builder_func)
    logs = runningUtil.runXVal(X, y, xValRunner, foldsN=10, foldsRepeats=1, model_builder_func=model_builder_func, small_train_fraction=args.small_train_set_fraction)

    print("##########################################################################################################")
    print("##########################################################################################################")
    print("##########################################################################################################")
    print("##########################################################################################################")
    print(logs)
    print("##########################################################################################################")

    # let's save logs
    json.dump(obj=logs, fp=open("AdultCensus_stats.json", 'w'), indent=4)
    ####################################################################################################################

    # trainConfTensor = tensorUtil.buildTensorFromConfusionsTable(xValLogs.trainConfusion, function(confM) return confM.totalValid )
    # testConfTensor = tensorUtil.buildTensorFromConfusionsTable(xValLogs.testConfusion, function(confM) return confM.totalValid )
    # pruningTrainConfTensor = tensorUtil.buildTensorFromConfusionsTable(xValLogs.pruningTrainConfusion, function(confM) return confM.totalValid )
    # pruningTestConfTensor = tensorUtil.buildTensorFromConfusionsTable(xValLogs.pruningTestConfusion, function(confM) return confM.totalValid )
    # treeTrainConfTensor = tensorUtil.buildTensorFromConfusionsTable(xValLogs.rulesTrainConfusion, function(confM) return confM.totalValid )
    # treeTestConfTensor = tensorUtil.buildTensorFromConfusionsTable(xValLogs.rulesTestConfusion, function(confM) return confM.totalValid )


    # function printTensor(t, str)
    #   print(str+' mean='+t.mean()+'; std='+t.std()+';')
    #
    #
    # printTensor(trainConfTensor, 'mlp train')
    # printTensor(testConfTensor, 'mlp test')
    # printTensor(pruningTrainConfTensor, 'pruning train')
    # printTensor(pruningTestConfTensor, 'pruning test' )
    # printTensor(treeTrainConfTensor, 'tree train')
    # printTensor(treeTestConfTensor, 'tree test' )


    # timeExec = datetime.now() - timeExec
    # print("Time to execute = " + (timeExec*1000) + 'ms')


if __name__ == '__main__':
    main()