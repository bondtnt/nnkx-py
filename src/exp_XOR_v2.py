import copy
import os
import csv
from collections import OrderedDict

import torch
import torch.optim as optim
import torch.nn as nn
from datetime import datetime
from nnkx import TableUtil
from nnkx import DataUtil
from nnkx import TensorUtil
from nnkx import ActivationModifierUtil
from nnkx import RunningUtil
from nnkx import MathUtil
from nnkx import ActivationModifier
from nnkx import ActivationRecorder
from nnkx import ActivationsDiscretizer
from nnkx import ModelPingUtil
from nnkx import RulesGenerator
from nnkx import timeit
from nnkx import DecisionTreePrinter
from torch.utils.data import Dataset
from data_utils import XYDataset
import math
import argparse
import sklearn
import numpy as np
from matplotlib.colors import ListedColormap
import matplotlib.pyplot as plt
import random
import time
import datetime

from src.nnkx import SensitivityPruningLogic, NeuronPruning, \
    QuantizationLayer, NeuronPruningModule, MAX_FLOAT_BOUND, MIN_FLOAT_BOUND

tableUtil = TableUtil()
dataUtil = DataUtil(tableUtil)
tensorUtil = TensorUtil(tableUtil)
runningUtil = RunningUtil(tensorUtil, dataUtil, tableUtil)
activationModifierUtil = ActivationModifierUtil(tensorUtil, runningUtil)
mathUtil = MathUtil(False, tensorUtil)
rulesGenerator = RulesGenerator(runningUtil, mathUtil, tensorUtil)

parser = argparse.ArgumentParser(description='PyTorch XOR FC-ANN training / decision tree extraction')
parser.add_argument('--device', default='cpu', help='device assignment ("cpu" or "cuda")')
parser.add_argument('-b', '--batchSize', default='100', help='batch size', type=int)
parser.add_argument('-o', '--optimization', default='SGD', help='optimization ("SGD" | "LBFGS" | etc+.)')
parser.add_argument('-lr', '--learningRate', default='0.001', help='learning rate, for SGD only', type=float)
parser.add_argument('-ld', '--learningRateDecay', default='0.001', help='learning rate, for SGD only', type=float)
parser.add_argument('--epochs', default='1500', help='learning rate, for SGD only', type=int)
parser.add_argument('-m', '--momentum', default='0.9', help='momentum, for SGD only', type=float)
parser.add_argument('-e', default='10', help='maximum nb of epochs per single run (fold)', type=int)
parser.add_argument('-i', default='3', help='maximum nb of iterations per batch, for LBFGS', type=int)
parser.add_argument('-coefL1', default='0', help='L1 penalty on the weights', type=float)
parser.add_argument('-coefL2', default='0', help='L2 penalty on the weights', type=float)
parser.add_argument('-t', '--threads', default='4', help='number of threads', type=int)
parser.add_argument('-tZero', default='1', help='start averaging at t0 (ASGD only), in nb of epochs', type=float)
parser.add_argument('--weightDecay', default='0.0', help='weightDecay', type=float)
args = parser.parse_args()

# fix seed
torch.manual_seed(1)

# threads
torch.set_num_threads(args.threads)
print('<torch> set nb of threads to ' + str(torch.get_num_threads()))

from memory_profiler import profile
import psutil


N_JOBS = 4

# precision = 10
# fp = open('memory_profiler_basic_mean2.log', 'w+')
# @profile(precision=precision, stream=fp)

def findDiff(d1, d2, path=""):
    for k in d1.keys():
        if not k in d2:
            print(path, ":")
            print(k + " as key not in d2", "\n")
        else:
            if type(d1[k]) is dict or isinstance(d1[k], OrderedDict):
                if path == "":
                    path = k
                else:
                    path = path + "->" + k
                findDiff(d1[k], d2[k], path)
            elif type(d1[k]) is list:
                l1 = d1[k]
                l2 = d2[k]
                for l1_item, l2_item in zip(l1, l2):
                    if isinstance(l1_item, torch.Tensor):
                        if not torch.all(torch.eq(l1_item, l2_item)):
                            print(path, ":")
                            print(" - ", k, " : (list Tensors)", l1_item)
                            print(" + ", k, " : (list Tensors)", l2_item)
                    else:
                        diff = [item for item in l1_item if item not in l2_item]
                        if len(diff) > 0:
                            print(path, ":")
                            print(" - ", k, " : (list)", l1_item)
                            print(" + ", k, " : (list)", l2_item)
            elif isinstance(d1[k], torch.Tensor):
                if not torch.all(torch.eq(d1[k], d2[k])):
                    print(path, ":")
                    print(" - ", k, " : (Tensors)", d1[k])
                    print(" + ", k, " : (Tensors)", d2[k])
            else:
                if d1[k] != d2[k]:
                    print(path, ":")
                    print(" - ", k, " : ", d1[k])
                    print(" + ", k, " : ", d2[k])



def main():
    process = psutil.Process(os.getpid())

    # use floats, for SGD
    if args.optimization == 'SGD':
      torch.set_default_tensor_type('torch.FloatTensor')

    # batch size?
    if args.optimization == 'LBFGS' and args.batchSize < 100:
      raise  Exception('LBFGS should not be used with small mini-batches; 1000 is recommed')


    # time = datetime.now()
    # timestamp = time.strftime('%Y-%m-%d_%H:%M:%S')
    dataSetName = 'xor' #used for log files naming
    print(dataSetName)

    classesNumber = 2
    labels2TargetsTe = [torch.FloatTensor([1, -1]), torch.FloatTensor([-1, 1])]
    labels2TargetsTr = [torch.FloatTensor([1, -1]), torch.FloatTensor([-1, 1])]

    datasetTrain={}
    datasetTest={}
    sizeTrain=0
    sizeTest=0
    originalMin = MAX_FLOAT_BOUND
    originalMax = MIN_FLOAT_BOUND
    newMin = -1
    newMax = 1


    class EmptyClass():
      def __init__(self):
        pass

    # originalMin = -1
    # originalMax = 1
    classesNumber = 2

    num_of_points = 200
    # X = [((random.randint(0, 100) * 2 - 100) / 100, ((random.randint(0, 100) * 2) - 100) / 100) for k in range(num_of_points)]
    X = [((random.randint(0, 100)) / 100, (random.randint(0, 100)) / 100) for k in range(num_of_points)]
    Xnp = np.asarray(X)
    X = torch.Tensor(X)

    y = []
    for i in range(X.shape[0]):
        # if (X[i][0] > 0 and X[i][1] < 0) or (X[i][0] < 0 and X[i][1] > 0):
        if (X[i][0] > 0.5 and X[i][1] < 0.5) or (X[i][0] < 0.5 and X[i][1] > 0.5):
            y.append(0)
        else:
            y.append(1)
    ynp = np.asarray(y).reshape(-1, 1)
    y = torch.LongTensor(y)

    ##############################################################################################
    # DECISION TREE CLASSIFIER TEST
    from sklearn.model_selection import cross_val_score
    from sklearn.tree import DecisionTreeClassifier
    from sklearn.metrics import f1_score
    from sklearn.metrics import confusion_matrix
    clf = DecisionTreeClassifier(random_state=0)
    clf.fit(Xnp, ynp)
    y_pred = clf.predict(Xnp)
    f1 = f1_score(ynp, y_pred, average='macro')
    cm = confusion_matrix(ynp, y_pred)
    print(cross_val_score(clf, Xnp, ynp, cv=10))
    print(f1)
    print(cm)
    # END OF DECISION TREE CLASSIFIER TEST
    ##############################################################################################
    # for CrossEntropyLoss
    # y = y.squeeze(1)

    data = XYDataset(X, y)


    classNames = {}
    for i in range(0, classesNumber):
      classNames[i] = ''+str(i)


    # ANN parameters
    inputsSize = data.X.size()[1]   #data[0].size(0)
    outputsSize = classesNumber # For ClassNNLCriterion - there must be classesNumber y_pred neurons.
    hidden1Size = 12
    # hidden2Size = 6

    class Net(nn.Module):

        def __init__(self, num_features, hidden1_units):
            super().__init__()
            self.num_features = num_features
            self.hidden1_units = hidden1_units
            # self.hidden2_units = hidden2_units
            self.model = nn.Sequential(
                # #NeuronPruningLayer('relu', num_features),
                QuantizationLayer(),  # Used to perform activations discretization/clusterization
                # ActivationModifier(num_features),  # Used to perform activations discretization/clusterization
                # ActivationRecorder(False),  # Used to record activation values
                nn.Linear(num_features, hidden1_units),
                nn.ReLU(),
                nn.BatchNorm1d(hidden1_units),

                NeuronPruningModule('relu', hidden1_units),
                # ActivationModifier(hidden1_units),  # Used to perform activations discretization/clusterization
                # ActivationRecorder(False),  # Used to record activation values
                nn.Linear(hidden1_units, outputsSize),
                # nn.ReLU(),
                # nn.BatchNorm1d(outputsSize),

                # # #NeuronPruningLayer('relu', outputsSize),
                # ActivationModifier(outputsSize),  # Used to perform activations discretization/clusterization
                # ActivationRecorder(False),  # Used to record activation values

                nn.LogSoftmax(),
                # ActivationModifier(outputsSize),  # Used to perform activations discretization/clusterization
                # ActivationRecorder(False),  # Used to record activation values
                # # nn.Linear(num_features, hidden1_units),
                # # nn.ReLU(),
                # # nn.BatchNorm1d(hidden1_units),
                # # nn.Linear(hidden1_units, 2),
                # # nn.LogSoftmax()
            )
            for m in self.model:
                if isinstance(m, nn.Linear):
                    nn.init.kaiming_normal_(m.weight)
                    nn.init.constant_(m.bias, 0)

        def forward(self, input_tensor):
            x = self.model(input_tensor)
            return x

        def clone(self):
            model_copy = type(self)(num_features=self.num_features,
                                    hidden1_units=self.hidden1_units)  # get a new instance
            model_copy.load_state_dict(self.state_dict())  # copy weights and stuff

            # # Our custom layers are not using nn.torch.Parameters / Variables / Buffers, thus we need to set their state
            # # manually.
            # layers = []
            # for layer in self.modules():
            #     layers.append(layer)
            #
            # copied_layers = []
            # for layer in model_copy.modules():
            #     copied_layers.append(layer)
            #
            # for layer, copied_layer in zip(layers, copied_layers):
            #     if 'ActivationModifier' in torch.typename(layer):
            #         # copied_layer. = layer
            #         pass
            #
            #     if 'ActivationRecorder' in torch.typename(layer):
            #         copied_layer.dump_patches = layer.dump_patches
            #         copied_layer.inputs_list = layer.inputs_list.copy()
            #         copied_layer.recordFlag = layer.recordFlag
            #         copied_layer.training = layer.training
            #
            #     if 'NeuronPruningLayer' in torch.typename(layer):
            #         copied_layer.dump_patches = layer.dump_patches
            #         copied_layer.mask_flag = layer.mask_flag
            #         copied_layer.pruningMask = layer.pruningMask.clone()
            #         copied_layer.training = layer.training
            #         copied_layer.zeroVal = layer.zeroVal

            return model_copy

        def get_state(self, optimizer):
            state = {
                # '0': {  # ActivationModifier
                #     'clustersTableBackup': self.model._modules['0'].clustersTableBackup,
                #     'layer_size': self.model._modules['0'].layer_size,
                #     'training': self.model._modules['0'].training
                # },
                # '1': {  # ActivationRecorder
                #     'inputs_list': self.model._modules['1'].inputs_list,
                #     'recordFlag': self.model._modules['1'].recordFlag,
                #     'training': self.model._modules['1'].training
                # },
                # '5': {  # NeuronPruningLayer
                #     'pruningMask': self.model._modules['5'].pruningMask,
                #     'zeroVal': self.model._modules['5'].zeroVal,
                #     'training': self.model._modules['5'].training
                # },
                # # '6': {  # ActivationModifier
                # #     'clustersTableBackup': self.model._modules['6'].clustersTableBackup,
                # #     'layer_size': self.model._modules['6'].layer_size,
                # #     'training': self.model._modules['6'].training
                # # },
                # # '7': {  # ActivationRecorder
                # #     'inputs_list': self.model._modules['7'].inputs_list,
                # #     'recordFlag': self.model._modules['7'].recordFlag,
                # #     'training': self.model._modules['7'].training
                # # },
                # # '9': {  # ActivationModifier
                # #     'clustersTableBackup': self.model._modules['9'].clustersTableBackup,
                # #     'layer_size': self.model._modules['9'].layer_size,
                # #     'training': self.model._modules['9'].training
                # # },
                # # '10': {  # ActivationRecorder
                # #     'inputs_list': self.model._modules['10'].inputs_list,
                # #     'recordFlag': self.model._modules['10'].recordFlag,
                # #     'training': self.model._modules['10'].training
                # # },
                'state_dict': self.state_dict(),
                'optimizer': optimizer.state_dict(),
                # 'epoch': epoch,
            }
            return state

        def set_state_dict(self, state_dict):
            # ActivationModifier
            layer_idx = '0'
            self.model._modules[layer_idx].clustersTableBackup = state_dict[layer_idx]['clustersTableBackup']
            self.model._modules[layer_idx].layer_size = state_dict[layer_idx]['layer_size']
            self.model._modules[layer_idx].training = state_dict[layer_idx]['training']

            # ActivationRecorder
            layer_idx = '1'
            self.model._modules[layer_idx].inputs_list = state_dict[layer_idx]['inputs_list']
            self.model._modules[layer_idx].recordFlag = state_dict[layer_idx]['recordFlag']
            self.model._modules[layer_idx].training = state_dict[layer_idx]['training']

            # NeuronPruningLayer
            layer_idx = '5'
            self.model._modules[layer_idx].pruningMask = state_dict[layer_idx]['pruningMask']
            self.model._modules[layer_idx].zeroVal = state_dict[layer_idx]['zeroVal']
            self.model._modules[layer_idx].training = state_dict[layer_idx]['training']

            # # ActivationModifier
            # layer_idx = '6'
            # self.model._modules[layer_idx].clustersTableBackup = state_dict[layer_idx]['clustersTableBackup']
            # self.model._modules[layer_idx].layer_size = state_dict[layer_idx]['layer_size']
            # self.model._modules[layer_idx].training = state_dict[layer_idx]['training']
            #
            # # ActivationRecorder
            # layer_idx = '7'
            # self.model._modules[layer_idx].inputs_list = state_dict[layer_idx]['inputs_list']
            # self.model._modules[layer_idx].recordFlag = state_dict[layer_idx]['recordFlag']
            # self.model._modules[layer_idx].training = state_dict[layer_idx]['training']
            #
            # # ActivationModifier
            # layer_idx = '9'
            # self.model._modules[layer_idx].clustersTableBackup = state_dict[layer_idx]['clustersTableBackup']
            # self.model._modules[layer_idx].layer_size = state_dict[layer_idx]['layer_size']
            # self.model._modules[layer_idx].training = state_dict[layer_idx]['training']
            #
            # # ActivationRecorder
            # layer_idx = '10'
            # self.model._modules[layer_idx].inputs_list = state_dict[layer_idx]['inputs_list']
            # self.model._modules[layer_idx].recordFlag = state_dict[layer_idx]['recordFlag']
            # self.model._modules[layer_idx].training = state_dict[layer_idx]['training']


    def model_builder_func():
        return Net(num_features=inputsSize,
                   hidden1_units=hidden1Size)

    mlp = model_builder_func()
    # verbose
    # print('<> using model.')
    # print(mlp)

    #=====================================================

    # criterion = nn.MSELoss()
    criterion = nn.CrossEntropyLoss()
    #criterion = nn.BCECriterion()
    #criterion = nn.MarginCriterion()
    #criterion = nn.SoftMarginCriterion()
    #criterion = nn.ClassNLLCriterion()

    verbose = True
    saveModels = False
    i = 1
    results = {}

    # # log results to files
    # trainLogger = optim.Logger(paths.concat('logs', dataSetName+'_'+timestamp+'train.log'))
    # testLogger = optim.Logger(paths.concat('logs', dataSetName+'_'+timestamp+'test.log'))
    # #prunningTrainLogger = optim.Logger(paths.concat('logs', dataSetName+'_'+timestamp+'prunedTrain.log'))
    # #prunningTestLogger = optim.Logger(paths.concat('logs', dataSetName+'_'+timestamp+'prunedTest.log'))
    # #prunedNeuronsCount = optim.Logger(paths.concat('logs', dataSetName+'_'+timestamp+'prunedNeuronsCount.log'))
    # rulesTrainLogger = optim.Logger(paths.concat('logs', dataSetName+'_'+timestamp+'rulesTrain.log'))
    # rulesTestLogger = optim.Logger(paths.concat('logs', dataSetName+'_'+timestamp+'rulesTest.log'))
    # rulesTestDepthLogger = optim.Logger(paths.concat('logs', dataSetName+'_'+timestamp+'rulesTestDepth.log'))
    # rulesTestLeafsLogger = optim.Logger(paths.concat('logs', dataSetName+'_'+timestamp+'rulesTestLeafs.log'))

    xValLogs = EmptyClass()
    xValLogs.trainConfusion = {}
    xValLogs.testConfusion = {}
    xValLogs.pruningTrainConfusion = []
    xValLogs.pruningTestConfusion = []
    xValLogs.rulesTrainConfusion = []
    xValLogs.rulesTestConfusion = []
    xValLogs.pruningLogs = []

    pruningConfig = EmptyClass()
    pruningConfig.afterPruningRetrainEpochs = 400
    pruningConfig.maxFallbacks = 1
    pruningConfig.errorWorsenForFallback = 0.05
    pruningConfig.numberOfNeuronsToPrune = 1  # Number of neurons to prune at single step
    pruningConfig.maxNodesToPrune = hidden1Size + inputsSize
    pruningConfig.maxPruningIterations = inputsSize + hidden1Size + pruningConfig.maxFallbacks * 3 #20
    pruningConfig.classNames = classNames
    # pruningConfig.retrainIterations = 10
    pruningConfig.verbose = 1


    @timeit
    def visualize_classification_boundary(_model, _dataLoader, folder_prefix=None, prefix="ann_"):
        _model.eval()

        h = .02  # step size in the visualization mesh
        cmap_light = ListedColormap(['#FFAAAA', '#AAFFAA', '#AAAAFF'])
        cmap_bold = ListedColormap(['#FF0000', '#00FF00', '#0000FF'])

        # Get mesh evaluation
        x_min, x_max = _dataLoader.dataset.X[:, 0].min() - 1, _dataLoader.dataset.X[:, 0].max() + 1
        y_min, y_max = _dataLoader.dataset.X[:, 1].min() - 1, _dataLoader.dataset.X[:, 1].max() + 1
        xx, yy = np.meshgrid(np.arange(x_min, x_max, h), np.arange(y_min, y_max, h))
        XY_mesh = torch.FloatTensor(np.c_[xx.ravel(), yy.ravel()])
        _, Z = torch.max(_model(XY_mesh), 1)

        # get train data evaluation
        train_predict_list = []
        X_train = []
        y_train = []
        for batch_idx, (input, target) in enumerate(_dataLoader):
            outputs = _model(input)  # Do the forward pass
            _, predicted = torch.max(outputs.data, 1)
            train_predict_list.append(predicted)
            X_train.append(input.squeeze(0))
            y_train.append(target.squeeze(0))

        X_train = np.vstack(X_train)
        y_train = np.vstack(y_train).flatten()
        train_predict = torch.cat(train_predict_list, 0).numpy()

        # Put the result into a color plot
        Z = Z.numpy()
        Z = Z.reshape(xx.shape)
        plt.figure()
        plt.pcolormesh(xx, yy, Z, cmap=cmap_light)

        # Plot also the training points
        # XY_mesh = XY_mesh.numpuy()
        plt.scatter(X_train[:, 0], X_train[:, 1], c=y_train, cmap=cmap_bold, edgecolor='k', s=20)
        plt.xlim(xx.min(), xx.max())
        plt.ylim(yy.min(), yy.max())
        plt.title("Classification boundary for trained model")

        if not folder_prefix is None:
            file_name = os.path.join(folder_prefix, prefix + str(time.time())+'.png')
            plt.savefig(file_name, dpi=None, facecolor='w', edgecolor='w', orientation='portrait')
        else:
            plt.show()

    @timeit
    def visualize_tree_classification_boundary(rulegen, treeRoot, _dataLoader, folder_prefix=None):
        print('Visualizing tree decision boundary')

        h = .02  # step size in the visualization mesh
        cmap_light = ListedColormap(['#FFAAAA', '#AAFFAA', '#AAAAFF'])
        cmap_bold = ListedColormap(['#FF0000', '#00FF00', '#0000FF'])

        # Get mesh evaluation over decision tree
        x_min, x_max = _dataLoader.dataset.X[:, 0].min() - 1, _dataLoader.dataset.X[:, 0].max() + 1
        y_min, y_max = _dataLoader.dataset.X[:, 1].min() - 1, _dataLoader.dataset.X[:, 1].max() + 1
        xx, yy = np.meshgrid(np.arange(x_min, x_max, h), np.arange(y_min, y_max, h))
        XY_mesh = torch.FloatTensor(np.c_[xx.ravel(), yy.ravel()])

        y_pred = []
        for row_indx in range(0, XY_mesh.shape[0]):
            input = XY_mesh[row_indx, :]
            actualOutput = rulegen.runTree(treeRoot, input)
            y_pred.append(actualOutput)

        Z = np.vstack(y_pred).flatten()

        # get (X1, X2, y) for input data - to print dots on scatter plot
        X_train = []
        y_train = []
        for batch_idx, (input, target) in enumerate(_dataLoader):
            X_train.append(input.squeeze(0))
            y_train.append(target.squeeze(0))
        X_train = np.vstack(X_train)
        y_train = np.vstack(y_train).flatten()

        # Put the result into a color plot
        Z = Z.reshape(xx.shape)
        print(Z)
        plt.figure()
        plt.pcolormesh(xx, yy, Z, cmap=cmap_light)

        # Plot also the training points
        # XY_mesh = XY_mesh.numpuy()
        plt.scatter(X_train[:, 0], X_train[:, 1], c=y_train, cmap=cmap_bold, edgecolor='k', s=20)
        plt.xlim(xx.min(), xx.max())
        plt.ylim(yy.min(), yy.max())
        plt.title("Classification boundary for trained model")

        if not folder_prefix is None:
            file_name = os.path.join(folder_prefix, "tree_"+str(time.time())+'.png')
            plt.savefig(file_name, dpi=None, facecolor='w', edgecolor='w', orientation='portrait')
        else:
            plt.show()


    time_stamp = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d_%H%M%S')
    xval_iteration = 1

    def optimizer_builder_func(_model):
        # return torch.optim.Adamax(_model.parameters(), betas=(0.9, 0.999), lr=1e-4, weight_decay=1e-4)
        return optim.SGD(_model.parameters(), lr=args.learningRate, momentum=args.momentum)

    def train_test_func(_model, _classNames, _trainDataLoader, _smallTrainDataLoader,_testDataLoader, xval_iteration=1):

        # _trainDataLoader = torch.utils.data.DataLoader(dataset=data, batch_size=args.batchSize, shuffle=True)
        # _testDataLoader = torch.utils.data.DataLoader(dataset=data, batch_size=args.batchSize, shuffle=True)

        runningUtil = RunningUtil(tensorUtil, dataUtil, tableUtil)
        activationModifierUtil = ActivationModifierUtil(tensorUtil, runningUtil)
        mathUtil = MathUtil(False, tensorUtil)
        rulesGenerator = RulesGenerator(runningUtil, mathUtil, tensorUtil)

        folder_prefix = os.path.join(os.path.sep, os.getcwd(), time_stamp, str(xval_iteration))
        if not os.path.exists(folder_prefix):
            os.makedirs(folder_prefix)

        # # calc / save stats
        # # trainConf = runningUtil.train(_model, criterion, optimizer, _trainDataLoader, _testDataLoader, _classNames, args)
        # loss_history_train, accuracy_history_train = runningUtil.train(_model, criterion, optimizer_builder_func, _trainDataLoader, args, epochs_to_print=100)
        # visualize_classification_boundary(_model, _trainDataLoader, folder_prefix)
        # accuracy_test = runningUtil.test(_model, _testDataLoader, criterion, args.batchSize)
        #
        # # # pruning -------------------------------------------------------------------------------------------------------
        # # # enable pruning layers
        # # for layer in _model.modules():
        # #     if 'NeuronPruningLayer' in torch.typename(layer):
        # #         layer.mask_flag = True
        #
        # sensPruningLogic = SensitivityPruningLogic(dataUtil)
        # neuronPruning1 = NeuronPruning(sensPruningLogic, dataUtil, runningUtil)
        # sensPruningLog, prunedModel = neuronPruning1.prune_module_neurons(pruningConfig, _trainDataLoader, copy.deepcopy(_model), criterion, args, optimizer_builder_func)
        # _model = prunedModel
        #
        # ## _model.train(True)
        # ## trainConf.updateValids()
        # ## testConf.updateValids()
        # ## xValLogs.trainConfusion[xval_iteration] = trainConf
        # ## xValLogs.testConfusion[xval_iteration] = testConf
        # ## print('trainConf '+ trainConf.__tostring__())
        # ## print('testConf '+ testConf.__tostring__())
        #
        # PATH = os.path.join(folder_prefix, 'MODEL' + str(time.time()) + '.model')
        # torch.save(_model.state_dict(), PATH)
        #
        # accuracy_test_before = runningUtil.test(_model, _trainDataLoader, criterion, args.batchSize)
        # print('Before saving: ' + str(accuracy_test_before))

        PATH = os.path.join(os.path.sep, os.getcwd(), '2019-05-04_002818', '1', 'MODEL1556919120.2276525.model')
        model = model_builder_func()
        model.load_state_dict(torch.load(PATH))
        model.eval()
        _model = model

        accuracy_test_after = runningUtil.test(model, _trainDataLoader, criterion, args.batchSize)
        print('After saving: ' + str(accuracy_test_after))

        # clusterize neuron outputs ---------------------------, 1st step is recording outputs of all neurons
        # modelPingUtil = ModelPingUtil()
        # discretizer = ActivationsDiscretizer(runningUtil, tableUtil, modelPingUtil, activationModifierUtil)

        def confusionCalcFuncTr(_model):
            def error_score(y_true, y_pred):
                # return 1 - sklearn.metrics.accuracy_score(y_true, y_pred)
                return 1 - sklearn.metrics.f1_score(y_true, y_pred)

            # return runningUtil.calculateMetric(_model, _trainDataLoader, error_score)
            return runningUtil.calculateMetric(_model, _trainDataLoader, sklearn.metrics.f1_score)

        modelPingUtil = ModelPingUtil()
        discretizer = ActivationsDiscretizer(runningUtil, tableUtil, modelPingUtil, activationModifierUtil)
        # confusionBeforeClusterizationTr = confusionCalcFuncTr()

        #state_dict = _model.get_state(optimizer=optimizer_builder_func(_model))
        #state_dict_path = 'state_dict.pth'
        #torch.save(state_dict, state_dict_path)

        # state_dict_restored = torch.load(state_dict_path)
        # _model_restored = model_builder_func()
        # _model_restored.load_state_dict(state_dict_restored['state_dict'])
        # _model_restored.set_state_dict(state_dict_restored)
        # optimizer_restored = optimizer_builder_func(_model)
        # optimizer_restored.load_state_dict(state_dict_restored['optimizer'])
        # state_dict_restored = _model_restored.get_state(optimizer=optimizer_builder_func(_model))
        #
        # #findDiff(state_dict, state_dict_restored)

        accuracy_test1 = runningUtil.test(_model, _testDataLoader, criterion, args.batchSize)
        print('Accuracy before clusterize '+str(accuracy_test1))

        visualize_classification_boundary(_model, _trainDataLoader, folder_prefix, prefix='ann')

        # discretizer.clusterize_new(_model, confusionCalcFuncTr, 1.95, _trainDataLoader, epsilonDecreaseStep=0.99)
        discretizer.clusterize_discretize(_model, confusionCalcFuncTr, _trainDataLoader)

        accuracy_test2 = runningUtil.test(_model, _testDataLoader, criterion, args.batchSize)
        print('Accuracy after clusterize ' + str(accuracy_test2))

        visualize_classification_boundary(_model, _trainDataLoader, folder_prefix, prefix='ann_clusterized')

        # extract rules
        maxDepth = 100
        rulegen = rulesGenerator
        rulegen.initialize(_model, _trainDataLoader)

        # get clustersTable from (input) first QuantizationLayer
        inputClustersWithBoundaries = None
        for key, l in _model.model._modules.items():
            layer_type = torch.typename(l)
            if QuantizationLayer.NAME in layer_type:
                inputClustersWithBoundaries = l.clusters_table.data
                break
        if inputClustersWithBoundaries is None:
            raise Exception('ClustersTable was not collected from QuantizationLayer layers (do you have at least one?)!')

        print('GenerateTree... Memory consumption is ' + str(process.memory_info().rss / 1024 / 1024) + 'Mbs')  # in Mbytes
        rootNode = rulegen.generateTreeFromInput(inputClustersWithBoundaries, maxDepth, folder_prefix, _trainDataLoader)
        print('GenerateTree DONE. Memory consumption is ' + str(process.memory_info().rss / 1024 / 1024) + 'Mbs')  # in Mbytes

        print('rootNode=>\n')
        DecisionTreePrinter().print(rootNode)
        treeStats = rootNode.treeStats()
        print('Tree leafs='+str(treeStats.leafsCount) + ' maxDepth='+str(treeStats.maxDepth))

        scaledTree = rulegen.scaleTree(rootNode, newMin, newMax, originalMin, originalMax)
        print('scaledTree=>\n')
        DecisionTreePrinter().print(scaledTree)

        visualize_tree_classification_boundary(rulegen, rootNode, _trainDataLoader, folder_prefix)

        print('-------------------------------------')
        # save extracted rules stats
        accuracy_score, f1_score, classification_report, conf_mat = runningUtil.calculateConfusion3(rulegen, rootNode,
                                                                                                    _trainDataLoader)
        print('Rules Train Confusion matrix: ')
        print(str(conf_mat))
        print('Rules Train accuracy=%.8f, f1=%.8f' % (accuracy_score, f1_score))
        print('Rules Train Classification report: ')
        print(str(classification_report))
        print('-------------------------------------')

        accuracy_score, f1_score, classification_report, conf_mat = runningUtil.calculateConfusion3(rulegen, rootNode,
                                                                                                    _testDataLoader)
        print('Rules Test Confusion matrix: ')
        print(str(conf_mat))
        print('Rules Test accuracy=%.8f, f1=%.8f' % (accuracy_score, f1_score))
        print('Rules Test Classification report: ')
        print(str(classification_report))
        print('-------------------------------------')

        # xValLogs.rulesTrainConfusion[xval_iteration] = rulesTrainConf
        # xValLogs.rulesTestConfusion[xval_iteration] = rulesTestConf

        print('Iteration '+str(xval_iteration)+' finished')
        xval_iteration = xval_iteration + 1

    #   trainLogger.add{['% mean class accuracy (train set)'] = trainConf.totalValid * 100}
    #   testLogger.add{['% mean class accuracy (test set)'] = testConf.totalValid * 100}
    # #  prunningTrainLogger.add{['% mean class accuracy (pruned train set)'] = pruningTrainConf.totalValid * 100}
    # #  prunningTestLogger.add{['% mean class accuracy (pruned test set)'] = pruningTestConf.totalValid * 100}
    # #  prunedNeuronsCount.add{['% pruning Log (total pruned neurons counts)'] = sensPruningLog.prunedNeurons[#sensPruningLog.prunedNeurons].total}
    #   rulesTrainLogger.add{['% mean class accuracy (rules train set)'] = rulesTrainConf.totalValid * 100}
    #   rulesTestLogger.add{['% mean class accuracy (rules test set)'] = rulesTestConf.totalValid * 100}
    #   rulesTestDepthLogger.add{['% rules maxDepth'] = treeStats.maxDepth}
    #   rulesTestLeafsLogger.add{['% rules count'] = treeStats.maxDepth}

    #   if True
    #     trainLogger.style{['% mean class accuracy (train set)'] = '-'}
    #     testLogger.style{['% mean class accuracy (test set)'] = '+'}
    # #    prunningTrainLogger.style{['% mean class accuracy (pruned train set)'] = '-'}
    # #    prunningTestLogger.style{['% mean class accuracy (pruned test set)'] = '+'}
    # #    prunedNeuronsCount.style{['% pruning Log (pruned neurons counts)'] = '-' }
    #     rulesTrainLogger.style{['% mean class accuracy (rules train set)'] = '-'}
    #     rulesTestLogger.style{['% mean class accuracy (rules test set)'] = '+'}
    #     rulesTestDepthLogger.style{['% mean class accuracy (rules test set)'] = '-'}
    #     rulesTestLeafsLogger.style{['% mean class accuracy (rules test set)'] = '-'}
    #
    #     trainLogger.plot()
    #     testLogger.plot()
    # #    prunningTrainLogger.plot()
    # #    prunningTestLogger.plot()
    # #    prunedNeuronsCount.plot()
    #     rulesTrainLogger.plot()
    #     rulesTestLogger.plot()
    #     rulesTestDepthLogger.plot()
    #     rulesTestLeafsLogger.plot()



    #profiler.start('expiris-profile.out')

    trainTestTrainer = EmptyClass()
    trainTestTrainer.criterion = criterion
    trainTestTrainer.classNames = classNames
    trainTestTrainer.trainDataLoader = torch.utils.data.DataLoader(dataset=data, batch_size=args.batchSize, shuffle=True)
    trainTestTrainer.smallTrainDataLoader = torch.utils.data.DataLoader(dataset=data, batch_size=args.batchSize, shuffle=True)
    trainTestTrainer.testDataLoader = torch.utils.data.DataLoader(dataset=data, batch_size=args.batchSize, shuffle=True)
    trainTestTrainer.saveModels = False  # if true all trained models will be saved
    trainTestTrainer.verbose = False     # will print accuracy if true
    trainTestTrainer.trainTest = None
    trainTestTrainer.trainTest = train_test_func

    runningUtil.runTestTrain(trainTestTrainer, 2, model_builder_func, optimizer_builder_func)

    # trainConfTensor = tensorUtil.buildTensorFromConfusionsTable(xValLogs.trainConfusion, function(confM) return confM.totalValid )
    # testConfTensor = tensorUtil.buildTensorFromConfusionsTable(xValLogs.testConfusion, function(confM) return confM.totalValid )
    # pruningTrainConfTensor = tensorUtil.buildTensorFromConfusionsTable(xValLogs.pruningTrainConfusion, function(confM) return confM.totalValid )
    # pruningTestConfTensor = tensorUtil.buildTensorFromConfusionsTable(xValLogs.pruningTestConfusion, function(confM) return confM.totalValid )
    # treeTrainConfTensor = tensorUtil.buildTensorFromConfusionsTable(xValLogs.rulesTrainConfusion, function(confM) return confM.totalValid )
    # treeTestConfTensor = tensorUtil.buildTensorFromConfusionsTable(xValLogs.rulesTestConfusion, function(confM) return confM.totalValid )


    # function printTensor(t, str)
    #   print(str+' mean='+t.mean()+'; std='+t.std()+';')
    #
    #
    # printTensor(trainConfTensor, 'mlp train')
    # printTensor(testConfTensor, 'mlp test')
    # printTensor(pruningTrainConfTensor, 'pruning train')
    # printTensor(pruningTestConfTensor, 'pruning test' )
    # printTensor(treeTrainConfTensor, 'tree train')
    # printTensor(treeTestConfTensor, 'tree test' )


    # timeExec = datetime.now() - timeExec
    # print("Time to execute = " + (timeExec*1000) + 'ms')


    #profiler.stop()
    #outfile = io.open( "expirisBatch-profile.txt", "w+" )
    #profiler.report( outfile )
    #outfile.close()

if __name__ == '__main__':
    main()