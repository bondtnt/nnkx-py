import torch
from unittest import TestCase

from src.nnkx import NeuronPruningFunction


class TestNeuronPruningFunction(TestCase):

    def test_NeuronPruningFunction_2d_batch4(self):

        # GIVEN
        # 2 neurons, batch size - 4
        input = torch.Tensor([[0.0825, 0.165], [0.2825, 0.4], [0.5525, 0.705], [0.8525, 1]])
        pruning_mask = torch.ByteTensor([0, 1])
        pruning_empty_value = 0.0

        # WHEN
        y = NeuronPruningFunction.apply(input, pruning_mask, pruning_empty_value)

        # THEN
        # second column should be nullified
        self.assertEqual(pruning_empty_value, y[:, 1].sum().item(), 'Pruning failed')

    def test_NeuronPruningFunction_1d_batch1(self):

        # GIVEN
        # 1 neuron, batch size - 1
        input = torch.Tensor([[0.0825]])
        pruning_mask = torch.ByteTensor([1])
        pruning_empty_value = 0.5

        # WHEN
        y = NeuronPruningFunction.apply(input, pruning_mask, pruning_empty_value)

        # THEN
        # second column should be nullified
        self.assertEqual(pruning_empty_value, y[0, 0].item(), 'Pruning single neuron failed')


