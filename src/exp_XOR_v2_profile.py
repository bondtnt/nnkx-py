import cProfile


if __name__ == '__main__':
    pr = cProfile.Profile()
    pr.enable()

    import exp_XOR_v2

    pr.disable()
    pr.print_stats()
    pr.dump_stats("profile.pstats")
